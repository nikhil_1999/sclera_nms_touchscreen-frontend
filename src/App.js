import React from 'react';
import MainContainer from './components/maincontainer/MainContainer';
import './App.css';
import { Component } from 'react';
import io from 'socket.io-client';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from './components/modal/Modal';
import * as NavbarActionType from './actions/NavbarActions';
import * as DashboardActionType from './actions/DashboardActions';
import * as NotificationActionType from './actions/NotificationActions';

import { connect } from 'react-redux';
import axios from 'axios';
import { link, dashboard_state_enum, AlarmTypes } from './config';
import Sound from 'react-sound';

const socketURL = 'http://localhost:2000';

const Msg = (data) => {
    let display_name, model, alarm;
    if (data.data.user_data_name === null) { display_name = data.data.display_name } else { display_name = data.data.user_data_name };
    if (data.data.user_data_model === null) { model = data.data.model } else { model = data.data.user_data_model };
    alarm = data.data.alarm;
    console.log('ALARM', alarm);
    return (
        <>
            <div className="notification-toast">
                <img src="/img/notification/device_type/generic.png" className="notification-toast-img" alt={data.data.type} />
                <div className="notification-toast-msg">
                    <p className="notification-toast-alarm">
                        {display_name}
                        <span className="notifcation-toast-highlight">{model}</span>
                        {AlarmTypes[`${alarm}`]}
                    </p>
                    <div className="notification-toast-location">
                        <img src="/img/notification/icon/location.png" className="notification-toast-locationimg" alt="Location" />
                        <label className="notification-toast-lastseen notification-toast-locationtxt">{data.data.location}</label>
                    </div>

                </div>
            </div>
            <div className="notification-toast-statusblock">
                <span className="notification-toast-status childcontainer-status childcontainer-online"></span>
                <p className="notification-toast-lastseen">2 minute(s) ago</p>
            </div>
        </>
    )
}


class App extends Component {

    state = {
        socket: null,
        popup_one: false,
        popup_multiple: false,
        current_device: null,
        offlineSoundStatus: "STOPPED",
        onlineSoundStatus: "STOPPED",
        onlineAudioURL: " ",
        offlineAudioURL: " ",
        offlinedevices: []
    }

    showToast = (data) => {
        this.setState({
            onlineAudioURL: "",
            onlineSoundStatus: "STOPPED"
        });
        return (toast(<Msg data={data} />,
            {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            }))
    }

    componentDidMount() {
        const socket = io(socketURL);
        socket.on("connect", () => {
            console.log("Socket connection established");
        });

        socket.on("hello", data => {
            console.log(data);
        });

        socket.on("offline", data => {
            console.log('offline', data);

            axios.get(`${link}/devices/dashboard`)
                .then(result => {
                    this.props.set_dashboard_list(result.data);
                })
                .catch(err => console.log(err));


            this.setState({
                offlinedevices: [...this.state.offlinedevices, data]
            }, () => {
                if (this.state.offlinedevices.length === 1) {
                    this.setState({
                        popup_one: true,
                        popup_multiple: false,
                        current_device: data,
                        offlineAudioURL: "/assets/offline.mp3",
                        offlineSoundStatus: "PLAYING"
                    });
                } else if (this.state.offlinedevices.length > 1) {
                    this.setState({
                        popup_one: false,
                        popup_multiple: true,
                        offlineAudioURL: "/assets/offline.mp3",
                        offlineSoundStatus: "PLAYING"
                    })
                }
            });
        });

        socket.on("online", data => {
            console.log('online', data);
            this.showToast(data);
            let filteredList = this.state.offlinedevices.filter(device => device.id !== data.id);
            this.setState({
                offlinedevices: filteredList,
                popup_multiple: false,
                popup_one: false,
                onlineAudioURL: "/assets/online.mp3",
                onlineSoundStatus: "PLAYING"
            })
            axios.get(`${link}/devices/dashboard`)
                .then(result => {
                    console.log('DASHBOARD', result);
                    this.props.set_dashboard_list(result.data);
                    this.props.reset_selected_dashboard()
                })
                .catch(err => console.log(err));
        });

        socket.on("refresh", () => {
            console.log("Notifications Refreshed")
            axios.get(`${link}/notification/all`)
                .then(res => {
                    this.props.set_notification_list(JSON.parse(res.data))
                })
                .catch(err => console.log(err));
        });

        this.setState({ socket: socket });
    }

    showDashboard = () => {
        this.props.selected('dashboard')
        axios.get(`${link}/devices/dashboard`)
            .then(result => {
                this.props.set_dashboard_list(result.data);
            })
            .catch(err => console.log(err));
        this.setState({
            popup_multiple: false,
            offlinedevices: []
        })
    }

    handleSnooze = (device_id) => {
        axios.put(`${link}/devices/updatedashboard/${device_id}/${dashboard_state_enum[1]}`, { "ticket": null })
            .then(res => {
                this.state.socket.emit("dashboard");
                axios.get(`${link}/devices/dashboard`)
                    .then(result => {
                        this.props.set_dashboard_list(result.data);
                        let filteredList = this.state.offlinedevices.filter(device => device.id !== device_id);
                        this.setState({
                            popup_one: false,
                            offlinedevices: filteredList
                        })
                    })
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
        this.setState({
            popup_one: false
        })
    }
    handleAcknowledge = (device_id) => {
        //generate ticket
        var timeStamp = Math.floor(Date.now() / 1000)
        var ticket = device_id + timeStamp;
        //update the device dashboard status & ticket
        this.state.socket.emit("dashboard");
        axios.put(`${link}/devices/updatedashboard/${device_id}/${dashboard_state_enum[2]}`, { "ticket": ticket })
            .then(res => {
                axios.get(`${link}/devices/dashboard`)
                    .then(result => {
                        this.props.set_dashboard_list(result.data);
                        let filteredList = this.state.offlinedevices.filter(device => device.id !== device_id);
                        this.setState({
                            popup_one: false,
                            offlinedevices: filteredList
                        })
                    })
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));

        this.setState({
            popup_one: false
        })

    }
    render() {
        return (
            <>
                <ToastContainer />

                <Sound
                    url={this.state.offlineAudioURL}
                    loop={true}
                    playStatus={this.state.offlineSoundStatus} />

                <Sound
                    url={this.state.onlineAudioURL}
                    loop={false}
                    playStatus={this.state.onlineSoundStatus}
                />

                <>
                    <MainContainer />
                    {this.state.popup_multiple ? <Modal>
                        <div className="modal-container">
                            <div className="modal-content">
                                <p className="modal-dashboard-msg">Some devices are offline. Please acknowledge.</p>
                                <div className="modal-btn-container">
                                    <button className="modal-btn" onClick={this.showDashboard}>OK</button>
                                </div>
                            </div>
                        </div></Modal> : null}

                    {this.state.popup_one ? <Modal>
                        <div className="modal-container">
                            <div className="modal-content">
                                <p className="modal-dashboard-msg">Device <span className="modal-dashboard-devicehighlight">{this.state.current_device.user_data_name !== null ? this.state.current_device.user_data_name : (this.state.current_device.display_name !== null ? this.state.current_device.display_name : this.state.current_device.hw_address)} </span>
                                    at location <span className="modal-dashboard-devicehighlight"> {this.state.current_device.location} </span> has been detected offline</p>
                                <div className="modal-dashboard-device">
                                    <img src="/img/notification/device_type/generic.png" className="modal-dashboard-icon" alt={`${this.state.current_device.type}`} />
                                    <div className="modal-dashboard-deviceinfo">
                                        <div className="modal-dashboard-deviceblock">
                                            <label className="modal-dashboard-label">IP Address</label>
                                            <label className="modal-dashboard-label modal-dashboard-colon">:</label>
                                            <p className="childcontainer-value modal-dashboard-value">{this.state.current_device.ip_addresses}</p>
                                        </div>
                                        <div className="modal-dashboard-deviceblock">
                                            <label className="modal-dashboard-label">MAC Address</label>
                                            <label className="modal-dashboard-label modal-dashboard-colon">:</label>
                                            <p className="childcontainer-value modal-dashboard-value">{this.state.current_device.hw_address}</p>
                                        </div>
                                        <div className="modal-dashboard-deviceblock">
                                            <label className="modal-dashboard-label">Floor</label>
                                            <label className="modal-dashboard-label modal-dashboard-colon">:</label>
                                            <p className="childcontainer-value modal-dashboard-value">{this.state.current_device.floor}</p>
                                        </div>
                                        <div className="modal-dashboard-deviceblock">
                                            <label className="modal-dashboard-label">Building</label>
                                            <label className="modal-dashboard-label modal-dashboard-colon">:</label>
                                            <p className="childcontainer-value modal-dashboard-value">{this.state.current_device.buidling}</p>
                                        </div>
                                    </div>
                                </div>
                                {this.state.current_device.local_dealer_account_number !== null ?
                                    <div className="modal-dashboard-vendor">
                                        <p className="modal-dashboard-vendor childcontainer-value">Please Contact the Vendor:</p>
                                        <div className="modal-dashboard-vendorblock">
                                            <label className="modal-dashboard-label">Vendor:</label>
                                            <p className="childcontainer-value">{this.state.current_device.local_dealer_account_number.dealer_name !== null ? this.state.current_device.local_dealer_account_number.dealer_name : null}
                                            </p>
                                        </div>
                                        <div className="modal-dashboard-vendorblock">
                                            <label className="modal-dashboard-label">Acc No:</label>
                                            <p className="childcontainer-value">{this.state.current_device.local_dealer_account_number.account_number !== null ? this.state.current_device.local_dealer_account_number.account_number : null}
                                            </p>
                                        </div>
                                        <div className="modal-dashboard-vendorblock">
                                            <label className="modal-dashboard-label">Phone:</label>
                                            <p className="childcontainer-value">{this.state.current_device.local_dealer_account_number.phone !== null ? this.state.current_device.local_dealer_account_number.phone : null}
                                            </p>
                                        </div>
                                        <div className="modal-dashboard-vendorblock">
                                            <label className="modal-dashboard-label">Email:</label>
                                            <p className="childcontainer-value">{this.state.current_device.local_dealer_account_number.email !== null ? this.state.current_device.local_dealer_account_number.email : null}
                                            </p>
                                        </div>
                                    </div>
                                    : null}
                                <div className="modal-btn-container">
                                    <button className="modal-btn modal-alert" onClick={() => this.handleSnooze(this.state.current_device.id)}>Snooze</button>
                                    <button className="modal-btn" onClick={() => this.handleAcknowledge(this.state.current_device.id)}>Send to Vendor</button>
                                </div>
                            </div>
                        </div></Modal> : null}
                </>
            </>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        selected: option =>
            dispatch({
                type: NavbarActionType.SELECTED_CHOICE,
                payload: option
            }),
        set_dashboard_list: option =>
            dispatch({
                type: DashboardActionType.LIST_DASHBOARD,
                payload: option
            }),
        set_notification_list: option =>
            dispatch({
                type: NotificationActionType.LIST_NOTIFICATIONS,
                payload: option
            }),
        reset_selected_dashboard: () =>
            dispatch({
                type: DashboardActionType.RESET_SELECTED_DASHBOARD
            })
    }
}

export default connect(null, mapDispatchToProps)(App);

