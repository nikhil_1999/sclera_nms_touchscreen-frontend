import React, { Component } from 'react'
import * as NavbarActionType from '../../../actions/NavbarActions';
import * as homeActionTypes from '../../../actions/HomeActions';

import { connect } from 'react-redux';
import './navbar.css';
import Clock from 'react-live-clock'
import axios from 'axios';
import { link } from '../../../config';
import './navbar.css'

// Image imports
// import home from '/img/header/home.png'
// import home_active from '../../../assets/img/icon/home.png'
// import contact from '../../../assets/img/icon/contact.png'
// import contact_active from '../../../assets/img/icon/contact.png'
// import notification from '../../../assets/img/icon/notification.png'
// import notification_active from '../../../assets/img/icon/notification.png'
// import report from '../../../assets/img/icon/report.png'
// import report_active from '../../../assets/img/icon/report.png'
// import setting from '../../../assets/img/icon/setting.png'
// import setting_active from '../../../assets/img/icon/setting.png'

class Navbar extends Component {

    state = {
        selected_choice: 'dashboard',
        property_name: '',
        agent_name: ''
    }
    componentDidMount = () => {
        axios.get(`${link}/home/propertydetails`)
            .then(res => {
                this.setState({
                    property_name: res.data.property_name.property_name,
                    agent_name: res.data.display_name.display_name
                })
            })
            .catch(err => console.log(err));
    }

    componentWillReceiveProps(nextProps){
        if(this.props!==nextProps){
            this.setState({selected_choice:nextProps.selected_choice});
        }
    }

    handleClick = (e) => {
        this.setState({
            selected_choice: e.target.name
        });
        this.props.selected(e.target.name);
        this.props.set_notification(false)
    }
    handleClickNotification = (e) => {
        this.props.reset_device()
        this.props.set_notification(!this.props.show_notification)
    }

    render() {
        return (
            <>
                <div className='navbar'>
                    <div className='navbar-left'>
                        <p className='navbar-heading'>{this.state.property_name}</p>
                        <p className='navbar-sideheading'>{this.state.agent_name}</p>
                    </div>
                    <div className='navbar-right'>
                        <img src={this.state.selected_choice === 'dashboard' ? '/img/header/dashboard_active.png' : '/img/header/dashboard.png'} className="navbar-icon" onClick={this.handleClick} alt="dashboard" name="dashboard" />
                        <img src={this.state.selected_choice === 'home' ? '/img/header/home_active.png' : '/img/header/home.png'} className="navbar-icon" onClick={this.handleClick} alt="home" name="home" />
                        <img src={this.state.selected_choice === 'contact' ? '/img/header/contact_active.png' : '/img/header/contact.png'} className="navbar-icon" onClick={this.handleClick} alt="contact" name="contact" />
                        <img src={this.state.selected_choice === 'report' ? '/img/header/report_active.png' : '/img/header/report.png'} className="navbar-icon" onClick={this.handleClick} alt="report" name="report" />
                        <img src={this.state.selected_choice === 'remote_access' ? '/img/header/remote_active.png' : '/img/header/remote.png'} className="navbar-icon" onClick={this.handleClick} alt="remote_access" name="remote_access" />
                        <img src={this.state.selected_choice === 'setting' ? '/img/header/setting_active.png' : '/img/header/setting.png'} className="navbar-icon" onClick={this.handleClick} alt="setting" name="setting" />
                        <img src={this.state.selected_choice === 'notification' ? '/img/header/notification_active.png' : '/img/header/notification.png'} className="navbar-icon" onClick={this.handleClickNotification} alt="notification" name="notification" />
                    </div>
                </div>
            </>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        selected: option =>
            dispatch({
                type: NavbarActionType.SELECTED_CHOICE,
                payload: option
            }),
        not_selected: () =>
            dispatch({
                type: NavbarActionType.NOT_SELECTED
            }),
        set_notification: option =>
            dispatch({
                type: NavbarActionType.SHOW_NOTIFICATION,
                payload: option
            }),
        reset_device: () =>
            dispatch({
                type: homeActionTypes.RESET_SELECTED_DEVICE,

            })

    }
}
const mapStateToProps = (state) => {
    return {
        show_notification: state.navbar.show_notification,
        selected_choice:state.navbar.selected_choice
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);