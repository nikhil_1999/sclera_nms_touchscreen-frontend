import React, { Component } from 'react'
import ContactList from './contact_list/ContactList'
import {connect} from 'react-redux';
import * as contactActionTypes from '../../../../actions/ContactActions';

class ContactContainer extends Component{

    componentDidMount(){
        this.props.reset_contact();
    }

    render(){
        return (
            <>
                <ContactList/>
            </>
        )
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        reset_contact:()=>
        dispatch({
            type:contactActionTypes.RESET_SELECTED_CONTACT
        })
    }
}

export default connect(null,mapDispatchToProps)(ContactContainer);