import React, { Component } from 'react'
import axios from 'axios';
import {link} from '../../../../../config';
import {connect} from 'react-redux';
import Contact from './contact/Contact';
import ContactInfo from './contact_info/ContactInfo';

class ContactList extends Component {

    state = {
        contactlist:[],
        searchTerm:''
    }

    temp_contactlist = []
    letter_array = [];
    backup_contactlist = [];  

    componentDidMount(){
        axios.get(`${link}/dealers/all`)
            .then(res=>{
                this.temp_contactlist = this.deleteNullDealers(res.data)
                this.backup_contactlist = [...this.temp_contactlist];
                this.setFinalState(res.data);
            })
            .catch(err=>console.log(err));
    }

    deleteNullDealers(contactlist) {
        for (let index = 0; index < contactlist.length; index++) {
          if (contactlist[index]['dealer_name'] === null) {
             contactlist.splice(index, 1);
          }
        }
        return contactlist
    }

    setFinalState = (temp_state) => {
        let temp = temp_state.filter(this.checkAlphabet);
        this.saveCurentState(temp);
        this.contactlistCreation(temp);
    }

    contactlistCreation(old_contactlist) {
        this.letter_array = [];
        let no_null_contactlist = this.deleteNullDealers(old_contactlist);
        no_null_contactlist.sort(this.GetSortOrder("dealer_name"))
        no_null_contactlist.map((contact) => {
          this.letter_array.push({
            key: 'heading' + contact.dealer_name.charAt(0).toUpperCase(),
            dealer_name: contact.dealer_name.charAt(0).toUpperCase()
          })
        })
    
        this.letter_array = this.letter_array.filter((obj, pos, arr) => {
          return arr.map(mapObj =>
            mapObj['key']).indexOf(obj['key']) === pos;
        });
    
        this.setState({
          contactlist: this.letter_array.concat(old_contactlist).sort(this.GetSortOrder("dealer_name")),
        })
    }


  checkAlphabet(alpha) {
    if (alpha.dealer_name !== null) {
      if (alpha.dealer_name.charCodeAt(0) >= 65 && alpha.dealer_name.charCodeAt(0) <= 90 && alpha.dealer_name.length === 1) {
        return false;
      }
      else {
        return true;
      }
    }
  }

  saveCurentState = (temp) => {
    this.temp_contactlist = [...temp];
  }

  searchContact = (e) => {
    let result_contactlist = [];

    for (let index = 0; index < this.temp_contactlist.length; index++) {
      let match_str = new RegExp(e.target.value, 'i');
      if (this.temp_contactlist[index].dealer_name.match(match_str)) {
        result_contactlist.push(this.temp_contactlist[index]);
        result_contactlist = result_contactlist.sort(this.GetSortOrder("dealer_name"))
      }
    }
    this.setFinalState(result_contactlist);
  }

  GetSortOrder = (val) => {
    return function (a, b) {
      if (a[val] !== undefined || b[val] !== undefined) {
        var a1 = a[val].toUpperCase();
        var b1 = b[val].toUpperCase();
      }
      if (a1 < b1) {
        return -1;
      } else if (a1 > b1) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  searchHandler = (e) => {
    this.setState({
      searchTerm: e.target.value
    });
  }

    render() {
        const { searchTerm } = this.state;
        const filteredContacts = this.state.contactlist.filter(contact => {
            return contact.dealer_name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
        });
        return (
            <div className="contactlist childcontainer-inner">
                <div className="contactlist-body childcontainer-left">
                    <div className="contactlist-contacts">
                      {filteredContacts.map(contact => (
                      <Contact
                        key={contact.account_number}
                        contact={contact}
                      />
                ))}
                    </div>
                </div>
                {this.props.selected_contact!==null?(
                    <div className="contact-info childcontainer-right">
                        <div className="childcontainer-card maininfo-card">
                          <div className="childcontainer-card-details">
                            <ContactInfo/>
                          </div>
                        </div>
                    </div>
                ):(null)}
            </div>
        )
    }
}

const mapStateToProps = (state)=>{
    return {
        selected_contact:state.contact.selected_contact
    }
}

export default connect(mapStateToProps,null)(ContactList);