import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as contactActionTypes from '../../../../../../actions/ContactActions';
import './../../contact.css';

class Contact extends Component {

    state = {
        dealer_name: null,
        type: null,
        phone: null,
        extension: null,
        option: null,
        email: null,
        address: null,
        notes: null,
        account_number: null,
        activeContact: false
    }

    componentDidMount() {
        if (this.props.contact !== null) {
            let clickable;
            let length = this.props.contact.dealer_name.length;
            if (length > 1) {
                clickable = true;
            } else {
                clickable = false;
            }
            this.setState({
                dealer_name: this.props.contact.dealer_name,
                type: this.props.contact.type,
                phone: this.props.contact.phone,
                extension: this.props.contact.extension,
                option: this.props.contact.option,
                email: this.props.contact.email,
                address: this.props.contact.address,
                notes: this.props.contact.notes,
                account_number: this.props.contact.account_number,
                clickable: clickable
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            if (nextProps.contact !== null) {
                let clickable;
                let length = this.props.contact.dealer_name.length;
                if (length > 1) {
                    clickable = true;
                } else {
                    clickable = false;
                }
                this.setState({
                    dealer_name: nextProps.contact.dealer_name,
                    type: nextProps.contact.type,
                    phone: nextProps.contact.phone,
                    extension: nextProps.contact.extension,
                    option: nextProps.contact.option,
                    email: nextProps.contact.email,
                    address: nextProps.contact.address,
                    notes: nextProps.contact.notes,
                    account_number: nextProps.contact.account_number,
                    clickable: clickable
                })
            }

            if (nextProps.selected_contact !== null) {
                if (nextProps.selected_contact.account_number === this.state.account_number && this.state.clickable) {
                    this.setState({ activeContact: true })
                } else {
                    this.setState({ activeContact: false })
                }
            }
        }
    }

    handleClickedContact = (e) => {
        e.stopPropagation();
        if (e.target.id === 'contact') {
            this.props.select_contact(this.state);
        }
    }

    render() {

        return (
            this.state.clickable ? 
            <>
                <p className={this.state.activeContact ? 'contact-text contact-activetext' : 'contact-text'} id="contact" onClick={this.handleClickedContact}>
                    {this.state.dealer_name}
                </p>
            </> : 
            <>
                    <div className='contact-label'>
                        <p id="contact">{this.state.dealer_name}</p>
                    </div>
            </>
        )


    }
}
const mapStateToProps = (state) => {
    return {
        selected_contact: state.contact.selected_contact
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        select_contact: contact =>
            dispatch({
                type: contactActionTypes.SELECTED_CONTACT,
                payload: contact
            })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);