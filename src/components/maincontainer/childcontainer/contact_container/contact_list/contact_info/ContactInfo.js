import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as contactActionTypes from '../../../../../../actions/ContactActions';
import './contactinfo.css';

class ContactInfo extends Component {

    state = {
        dealer_name: null,
        type: null,
        phone: null,
        extension: null,
        option: null,
        email: null,
        address: null,
        notes: null,
        account_number: null,
        selected_phone_option: null
    }

    componentDidMount() {
        if (this.props.selected_contact !== null) {
            this.setState({
                dealer_name: this.props.selected_contact.dealer_name,
                type: this.props.selected_contact.type,
                phone: this.props.selected_contact.phone,
                extension: this.props.selected_contact.extension,
                option: this.props.selected_contact.option,
                email: this.props.selected_contact.email,
                address: this.props.selected_contact.address,
                notes: this.props.selected_contact.notes,
                account_number: this.props.selected_contact.account_number
            })

            if (this.props.selected_contact.extension === null) {
                this.setState({ selected_phone_option: 'Option' })
            } else {
                this.setState({ selected_phone_option: 'Extension' })
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            if (nextProps.selected_contact !== null) {
                this.setState({
                    dealer_name: nextProps.selected_contact.dealer_name,
                    type: nextProps.selected_contact.type,
                    phone: nextProps.selected_contact.phone,
                    extension: nextProps.selected_contact.extension,
                    option: nextProps.selected_contact.option,
                    email: nextProps.selected_contact.email,
                    address: nextProps.selected_contact.address,
                    notes: nextProps.selected_contact.notes,
                    account_number: nextProps.selected_contact.account_number
                })
            }
            if (nextProps.selected_contact !== null) {
                if (nextProps.selected_contact.extension === null) {
                    this.setState({ selected_phone_option: 'Option' })
                } else {
                    this.setState({ selected_phone_option: 'Extension' })
                }
            }
        }
    }

    closeContactInfo = (e) => {
        this.props.reset_contact();
    }

    onChange = (e) => {
        this.state({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div className="maininfo-card-info">
                <label className="childcontainer-label" htmlFor="Account Number">Account Number</label>
                <input className="childcontainer-input" type="text" name="account_number" value={this.state.account_number === null ? ('') : (this.state.account_number)} />
                <label className="childcontainer-label" htmlFor="Dealer Name">Dealer Name</label>
                <input className="childcontainer-input" type="text" name="dealer_name" value={this.state.dealer_name === null ? ('') : (this.state.dealer_name)} />
                <label className="childcontainer-label" htmlFor="Type">Type</label>
                <input className="childcontainer-input" type="text" name="type" value={this.state.type === null ? ('') : (this.state.type)} />
                <label className="childcontainer-label" htmlFor="Phone">Phone</label>
                <input className="childcontainer-input" type="text" name="phone" value={this.state.phone === null ? ('') : (this.state.phone)} />
                <label className="childcontainer-label">{this.state.selected_phone_option}</label>
                <input className="childcontainer-input" name={this.state.selected_phone_option === 'Extension' ? ('Extension') : ('Option')} value={this.state.selected_phone_option === 'Extension' ? (this.state.extension) : (this.state.option)} id="account-extension-input" readOnly />
                <label className="childcontainer-label" htmlFor="Address">Address</label>
                <input className="childcontainer-input" type="text" name="address" value={this.state.address === null ? ('') : (this.state.address)} />
                <label className="childcontainer-label" htmlFor="Email">Email</label>
                <input className="childcontainer-input" type="text" name="email" value={this.state.email === null ? ('') : (this.state.email)} />
                <label className="childcontainer-label" htmlFor="Notes">Notes</label>
                <input className="childcontainer-input" type="text" name="notes" value={this.state.notes === null ? ('') : (this.state.notes)} />

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selected_contact: state.contact.selected_contact
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reset_contact: () =>
            dispatch({
                type: contactActionTypes.RESET_SELECTED_CONTACT
            })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactInfo);