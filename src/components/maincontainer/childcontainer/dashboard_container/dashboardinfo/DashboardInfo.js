import React, { Component } from 'react';
import './dashboardinfo.css'
import DeviceInfo from '../deviceinfo/DeviceInfo';
import { connect } from 'react-redux';
import {AlarmTypes} from '../../../../../config';

class DashboardInfo extends Component {
    state = {
        show_more: false
    }
    componentWillReceiveProps = (nextProps) => {
        this.setState({
            device: nextProps.selected_dashboard,
            show_more: false
        })
    }

    showMoreDetails = () => {
        this.setState({
            show_more: true
        })
    }

    showLessDetails = () => {
        this.setState({
            show_more: false
        })

    }
    
    styleHeading = () => {

        if (this.props.selected_dashboard) {
            if (this.props.selected_dashboard.dashboard_status === 'snooze') {
                return 'dashboard-info-type dashboard-notify'
            }
            else if (this.props.selected_dashboard.dashboard_status === 'reported to vendor') {
                return 'dashboard-info-type dashboard-awaiting'
            }
            else if (this.props.selected_dashboard.dashboard_status === 'fixed by vendor') {
                return 'dashboard-info-type dashboard-resolve'
            }

        }

    }

    textHeading = () => {

        if (this.props.selected_dashboard) {
            if (this.props.selected_dashboard.dashboard_status === 'snooze') {
                return 'Not Notified to Vendor'
            }
            else if (this.props.selected_dashboard.dashboard_status === 'reported to vendor') {
                return 'Waiting for Acknowledgment'
            }
            else if (this.props.selected_dashboard.dashboard_status === 'fixed by vendor') {
                return 'Resolved Issue'
            }

        }

    }
    
    render() {
        console.log('Dashboard Info', this.props)
        if (this.state.show_more === false) {
            return (
                <>
                    {/* <p className="dashboard-info-type dashboard-awaiting">Waiting for Acknowledgment</p> */}
                    <p className={this.styleHeading()}>{this.textHeading()}</p>
                    <div className="dashboard-info-container maininfo-card-info">
                        {this.props.selected_dashboard.ticket ? (this.props.selected_dashboard.ticket !== null ? <div><label className="dashboard-card-label chilcontainer-label">Ticket No.:</label>
                            <input className="childcontainer-input" value={this.props.selected_dashboard.ticket}/></div> : null) : null}
                        <label className="dashboard-card-label chilcontainer-label">Vendor:</label>
                        <input className="childcontainer-input" value={this.props.selected_dashboard.user_data_name !== null ? this.props.selected_dashboard.user_data_name : (this.props.selected_dashboard.display_name !== null ? this.props.selected_dashboard.display_name : this.props.selected_dashboard.hw_address)}/>
                        <label className="dashboard-card-label chilcontainer-label">Building:</label>
                        <input className="childcontainer-input" value={this.props.selected_dashboard.building}/>
                        <label className="dashboard-card-label chilcontainer-label">Floor:</label>
                        <input className="childcontainer-input" value={this.props.selected_dashboard.floor}/>
                        <label className="dashboard-card-label chilcontainer-label">Location:</label>
                        <input className="childcontainer-input" value={this.props.selected_dashboard.location}/>
                        <label className="dashboard-card-label chilcontainer-label">MAC Address:</label>
                        <input className="childcontainer-input" value={this.props.selected_dashboard.hw_address}/>
                        <label className="dashboard-card-label chilcontainer-label">IP Address:</label>
                        <input className="childcontainer-input" value={this.props.selected_dashboard.ip_addresses}/>
                        <label className="dashboard-card-label chilcontainer-label">Alarm:</label>
                        <input className="childcontainer-input" value={AlarmTypes[`${this.props.selected_dashboard.alarm}`]}/>
                        {/* <div className="childcontainer-btn-container maininfo-btn-container"> */}
                        <button className="dashboard-info-more" onClick={this.showMoreDetails}>more...</button>
                        {/* </div> */}
                    </div>
                </>
            )

        }
        else {
            return (<DeviceInfo selected_device={this.props.selected_dashboard} showLessDetails={this.showLessDetails} />)
        }
    }
}
const mapStateToProps = (state) => {
    return {
        selected_dashboard: state.dashboard.selected_dashboard
    }
}



export default connect(mapStateToProps, null)(DashboardInfo);