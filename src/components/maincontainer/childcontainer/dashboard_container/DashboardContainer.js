import React, { Component } from 'react';
import './dashboardcontainer.css';
import axios from 'axios';
import { link, dashboard_state_enum } from '../../../../config';
import { connect } from 'react-redux';
import DashboardInfo from './dashboardinfo/DashboardInfo'
import * as DashboardActionType from '../../../../actions/DashboardActions';

const epochs = [
    ['year(s)', 31536000],
    ['month(s)', 2592000],
    ['day(s)', 86400],
    ['hour(s)', 3600],
    ['minute(s)', 60],
    ['second(s)', 1]
];

class DashboardContainer extends Component {
    state = {
        dashboard_list: [],
        selected_dashboard: null,
        show_device_info: false
    }

    componentDidMount = () => {
        axios.get(`${link}/devices/dashboard`)
            .then(result => {
                this.props.set_dashboard_list(result.data)
            })
            .catch(err => console.log(err));
    }

    componentWillReceiveProps = (nextProps) => {

        this.setState({
            dashboard_list: nextProps.dashboard_list,
            selected_dashboard: nextProps.selected_dashboard,
            // show_device_info: false
        })
    }

    getDuration = (timeAgoInSeconds) => {
        for (let [name, seconds] of epochs) {
            const interval = Math.floor((timeAgoInSeconds + 1) / seconds);

            if (interval >= 1) {
                return {
                    interval: interval,
                    epoch: name
                };
            }
        }
    };


    calculateLastSeenTime = (timestamp) => {
        let last_seen_timestamp = parseInt(timestamp, 10);
        const timeAgoInSeconds = Math.floor((new Date() - new Date(last_seen_timestamp)) / 1000);
        const { interval, epoch } = this.getDuration(timeAgoInSeconds);
        return `${interval} ${epoch} ago`
    }

    handleAcknowledge = (device_id) => {
        //generate ticket
        var timeStamp = Math.floor(Date.now() / 1000)
        var ticket = device_id + timeStamp
        //update device with dashboard status & ticket on reporting to vendor
        axios.put(`${link}/devices/updatedashboard/${device_id}/${dashboard_state_enum[2]}`, { "ticket": ticket })
            .then(res => {
                axios.get(`${link}/devices/dashboard`)
                    .then(result => {
                        this.props.set_dashboard_list(result.data)
                        this.setState({
                            selected_dashboard: null,
                            show_device_info: false
                        })
                    })
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    }

    renderDeviceIcon = (device) => {
        if (device.user_data_type !== null) {
            return (<img src={`/img/notification/device_type/${this.convertImageName(device.user_data_type)}.png`} className="dashboard-card-devicetype" alt={device.user_data_name === null ? (device.display_name) : (device.user_data_name)} />)
        } else if (device.type !== null) {
            return (<img src={`/img/notification/device_type/${this.convertImageName(device.type)}.png`} className="dashboard-card-devicetype" alt={device.user_data_name === null ? (device.display_name) : (device.user_data_name)} />)
        } else {
            return (<img src={`/img/notification/device_type/generic.png`} className="dashboard-card-devicetype" alt={device.user_data_name === null ? (device.display_name) : (device.user_data_name)} />)
        }
    }
    convertImageName = (type) => {
        return type.replace(/[\s\/]/g, "_").toLowerCase()
    }

    styleOuter = (device) => {
        var outer_class_name = '';
        switch (device.dashboard_status) {
            case dashboard_state_enum[1]: outer_class_name = "dashboard-card-blocks dashboard-card-notify"
                break;
            case dashboard_state_enum[2]: outer_class_name = "dashboard-card-blocks dashboard-card-awaiting"
                break;
            case dashboard_state_enum[3]: outer_class_name = "dashboard-card-blocks dashboard-card-resolved"
                break;

            default: outer_class_name = "dashboard-card-blocks dashboard-card-notify"
        }

        if (this.props.selected_dashboard) {
            if (this.props.selected_dashboard.id === device.id && this.state.show_device_info === true) {
                outer_class_name += ' dashboard-activegrid'
            }
            else {
                outer_class_name += ''
            }
        }
        else {
            outer_class_name += ''
        }
        return outer_class_name;
    }

    styleButton = (device) => {
        switch (device.dashboard_status) {
            case dashboard_state_enum[1]: return (<div className="dashboard-card-btnblock" onClick={() => this.handleAcknowledge(device.id)}><p className="dashboard-card-btn childcontainer-btn">Notify Vendor</p><img src="/img/childcontainer/dashboard/red.png" className="dashboard-bell" /></div>)
            case dashboard_state_enum[2]: return (<div className="dashboard-card-btnblock"><p className="dashboard-card-btn childcontainer-btn">Awaiting Vendor</p><img src="/img/childcontainer/dashboard/yellow.png" className="dashboard-bell" /></div>)
            case dashboard_state_enum[3]: return (<div className="dashboard-card-btnblock"><p className="dashboard-card-btn childcontainer-btn">Vendor Acknowledged</p><img src="/img/childcontainer/dashboard/green.png" className="dashboard-bell" /></div>)
            default: return (<div className="dashboard-card-btnblock" onClick={() => this.handleAcknowledge(device.id)}><p className="dashboard-card-btn childcontainer-btn">Notify Vendor</p><img src="/img/childcontainer/dashboard/red.png" className="dashboard-bell" /></div>)
        }
    }

    showDeviceInfo = (device) => {
        this.props.set_selected_dashboard(device)
        if (this.props.selected_dashboard) {
            if (this.props.selected_dashboard.id === device.id) {
                this.props.reset_selected_dashboard()   //deselect selected dashboard
                this.setState({
                    show_device_info: false,
                })
            }
            else {
                this.setState({
                    show_device_info: true,
                    selected_dashboard: device
                })
            }
        }
        else {
            this.setState({
                show_device_info: true,
                selected_dashboard: device
            })
        }
    }

    render() {

        let list = this.state.dashboard_list.map(device => {
            return (
                <div className={this.styleOuter(device)} key={device.id} >
                    <div className="dashboard-card" onClick={() => this.showDeviceInfo(device)}>
                        <div className="dashboard-card-device">
                            {this.renderDeviceIcon(device)}
                            <div className="dashboard-card-deviceinfo">
                                <p className="childcontainer-value dashboard-card-devicename">{device.user_data_name !== null ? device.user_data_name : (device.display_name !== null ? device.display_name : device.hw_address)}</p>
                                <p className="childcontainer-value dashboard-card-value dashboard-card-location">{device.user_data_model !== null ? device.user_data_model : device.model}</p>
                                <p className="childcontainer-value dashboard-card-value dashboard-card-location">{device.location !== null ? device.location : null}</p>
                            </div>
                        </div>

                        <div className="dashboard-card-vendordetails">
                            <div className="dashboard-card-vendordetail">
                                <label className="dashboard-card-label">Vendor</label>
                                <label className="dashboard-card-label dashboard-card-colon">:</label>
                                {device.local_dealer_account_number !== null ? (
                                <p className="childcontainer-value dashboard-card-vendorvalue">{device.local_dealer_account_number.dealer_name !== null ? device.local_dealer_account_number.dealer_name : 'No Name'}</p>) : null}     
                                </div>
                            <div className="dashboard-card-vendordetail">
                                <label className="dashboard-card-label">Acc No</label>
                                <label className="dashboard-card-label dashboard-card-colon">:</label>
                                {device.local_dealer_account_number !== null ? (
                                <p className="childcontainer-value dashboard-card-vendorvalue">{device.local_dealer_account_number.account_number !== null ? device.local_dealer_account_number.account_number : 'No Account Number'}</p>
                                ):null}    </div>
                            <div className="dashboard-card-vendordetail">
                                <label className="dashboard-card-label">Phone</label>
                                <label className="dashboard-card-label dashboard-card-colon">:</label>
                                {device.local_dealer_account_number !== null ? (
                                <p className="childcontainer-value dashboard-card-vendorvalue">{device.local_dealer_account_number.phone !== null ? device.local_dealer_account_number.phone : null}</p>
                                ):null}
                                </div>
                            <div className="dashboard-card-vendordetail">
                                <label className="dashboard-card-label">Email</label>
                                <label className="dashboard-card-label dashboard-card-colon">:</label>
                                {device.local_dealer_account_number !== null ? (
                                <p className="childcontainer-value dashboard-card-vendorvalue">{device.local_dealer_account_number.email !== null ? device.local_dealer_account_number.email : null}</p>
                                ):null}</div>
                        </div>
                       

                        <div className="dashboard-statusblock">
                            <p className="childcontainer-value dashboard-card-value dashboard-card-location dashboard-card-time">{this.calculateLastSeenTime(device.last_seen_on * 1000)}</p>
                            <span className={device.status === 1 ? ("dashboard-status childcontainer-status childcontainer-online") : ("dashboard-status childcontainer-status childcontainer-offline")}></span>
                        </div>

                    </div>
                    {this.styleButton(device)}
                </div >
            )
        })

        return (

            <div className="dashboard childcontainer-inner" >
                <div className="childcontainer-left">
                    <div className="homecontainer-block">
                        <p className="dashboard-heading">Dashboard</p>
                        <div className="dashboard-card-container">
                            {list}
                        </div>
                    </div>
                </div>
                <div className="childcontainer-right">
                    <div className="homecontainer-block">
                        {(this.state.show_device_info && this.props.selected_dashboard) ?
                            <div className="maininfo-card childcontainer-card">
                                <div className="childcontainer-card-details">
                                    <DashboardInfo />
                                </div>
                            </div> : null}
                    </div>
                </div>
            </div>
        )



    }
}
const mapStateToProps = (state) => {
    return {
        dashboard_list: state.dashboard.dashboard_list,
        selected_dashboard: state.dashboard.selected_dashboard
    }
}
const mapDispatchToProps = (dispatch) => {
    return {

        set_dashboard_list: option =>
            dispatch({
                type: DashboardActionType.LIST_DASHBOARD,
                payload: option
            }),
        set_selected_dashboard: option =>
            dispatch({
                type: DashboardActionType.SELECTED_DASHBOARD,
                payload: option
            }),

        reset_selected_dashboard: () =>
            dispatch({
                type: DashboardActionType.RESET_SELECTED_DASHBOARD
            }),

    }
}



export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);
