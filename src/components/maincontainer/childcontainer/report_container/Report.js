import React, { Component } from 'react'
import { Bar } from 'react-chartjs-2';
import './report.css';
import html2canvas from 'html2canvas';
import { saveAs } from 'file-saver';
import axios from 'axios';
import { link } from '../../../../config';


class Report extends Component {

    state = {
        data: {
            labels: ["Toshiba", "Dell", "Nvidia", "Sandisk", "AMD", "WD", "Sony", "HP", "Samsung"],
            datasets: [{
                label: "Toshiba",
                data: [10, 20, 30, 40, 10, 60, 70, 10, 90, 10, 0],
                backgroundColor: "red"
            },
            {
                label: "Dell",
                data: [100, 20, 30, 40, 50, 60, 70, 80, 90, 40, 0],
                backgroundColor: "blue"
            },
            {
                label: "Nvidia",
                data: [10, 50, 30, 40, 50, 60, 70, 80, 10, 100, 0],
                backgroundColor: "green"
            },
            {
                label: "Sandisk",
                data: [10, 90, 30, 40, 50, 10, 70, 80, 30, 50, 0],
                backgroundColor: "yellow"
            },
            {
                label: "AMD",
                data: [10, 20, 30, 40, 10, 10, 44, 22, 90, 100, 0],
                backgroundColor: "pink"
            },
            {
                label: "WD",
                data: [40, 50, 30, 40, 50, 60, 70, 80, 90, 30, 0],
                backgroundColor: "grey"
            },
            ]
        },
        showModal: false,
        items: [],
        value: "",
        error: null,
        visible: false,
        selected_range: 'Last 24 hours',
        fromDate: '',
        toDate: '',
        vendor: 'All',
        dealer_list: []
    }

    flag = 0;
    componentDidMount() {
        axios.get(`${link}/dealers/all`)
            .then(res => {
                this.setState({ dealer_list: res.data })
            })
            .catch(err => console.log(err));
    }

    componentWillReceiveProps(nextProps) {
        axios.get(`${link}/dealers/all`)
            .then(res => {
                this.setState({ dealer_list: res.data })
            })
            .catch(err => console.log(err));
    }



    toggleModal = () => {
        this.setState({
            showModal: !this.state.showModal
        })
    };

    changeData = () => {
        this.setState({
            data: {
                labels: ["A", "B", "C", "D", "E", "F", "G", "H", "I"],
                datasets: [{
                    label: "Dummy",
                    data: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 0, 120],
                    backgroundColor: '#222222'
                }],
            }
        })
    }


    calculateAvg = () => {
        let sum = this.state.data.datasets[0].data.reduce((a, b) => a + b, 0);
        let avg = (sum / this.state.data.datasets[0].data.length) || 0;
        return avg;

    }
    generateURL = () => {

        let sum = this.state.data.datasets[0].data.reduce((a, b) => a + b, 0);
        let avg = (sum / this.state.data.datasets[0].data.length) || 0;
        let final_obj = {}
        final_obj['selected_range'] = this.state.selected_range
        final_obj['vendor'] = this.state.vendor
        final_obj['data'] = document.getElementById('ctx').toDataURL()
        final_obj['average'] = avg
        final_obj['email'] = this.state.items
        console.log('ffff', final_obj);
        this.setState({
            items: []
        })
        this.toggleModal();
    }

    // Email handling function
    exportData = () => {
        this.toggleModal();
        html2canvas(document.querySelector("#ctx")).then(canvas => {
            var dataURL = canvas.toDataURL("image/png");
            let data = {
                image: dataURL,
                emailList: ['nikhil@accessonline.io', 'nikhiladigaz@gmail.com', 'avinash@accessonline.io']
            }
            axios.post('http://localhost:5000/api/pdf/createPDF', data)
                .then(() => axios.get('http://localhost:5000/api/pdf/fetchPDF', { responseType: 'blob' }))
                .then((res) => {
                    const pdfBlob = new Blob([res.data], { type: 'application/pdf' })
                    saveAs(pdfBlob, "report.pdf");
                })
                .catch(err => console.log(err))
                .catch(err => console.log(err))
        });
    }


    handleKeyDown = evt => {
        if (["Enter", "Tab", ","].includes(evt.key)) {
            evt.preventDefault();

            var value = this.state.value.trim();

            if (value && this.isValid(value)) {
                this.setState({
                    items: [...this.state.items, this.state.value],
                    value: ""
                });
            }
        }
    };

    handleChange = evt => {
        this.setState({
            value: evt.target.value,
            error: null
        });
    };

    handleDelete = item => {
        this.setState({
            items: this.state.items.filter(i => i !== item)
        });
    };

    // handlePaste = evt => {
    //     evt.preventDefault();
    //     var paste = evt.clipboardData.getData("text");
    //     var emails = paste.match(`/[\w\d\.-]+@[\w\d\.-]+\.[\w\d\.-]+/g`);
    //     if (emails) {
    //         var toBeAdded = emails.filter(email => !this.isInList(email));

    //         this.setState({
    //             items: [...this.state.items, ...toBeAdded]
    //         });
    //     }
    // };

    // isValid(email) {
    //     let error = null;

    //     if (this.isInList(email)) {
    //         error = `${email} has already been added.`;
    //     }

    //     if (!this.isEmail(email)) {
    //         error = `${email} is not a valid email address.`;
    //     }

    //     if (error) {
    //         this.setState({ error });

    //         return false;
    //     }

    //     return true;
    // }

    // isInList(email) {
    //     return this.state.items.includes(email);
    // }

    // isEmail(email) {
    //     return /[\w\d\.-]+@[\w\d\.-]+\.[\w\d\.-]+/.test(email);
    // }

    handleClick = (e) => {
        if (this.flag === 0) {
            if (!this.state.visible) {
                document.addEventListener('click', this.handleOutsideClick, false);
            } else {
                document.removeEventListener('click', this.handleOutsideClick, false);
            }
            this.setState(prevState => ({
                visible: !prevState.visible
            }));
        }
        else {
            this.flag = 0;
        }
    }

    handleOutsideClick = (e) => {
        //Nikhil see this
        // if (this.node.contains(e.target)) {
        //     return;
        // }
        // this.handleClick();
    }

    setData = (e) => {
        console.log("Set data func clicked")
        this.flag = 1;
        this.setState({
            selected_range: e.target.innerHTML,
            visible: false
        });
    }

    setDate = ({ target: { value, name } }) => {
        this.setState({
            [name]: value
        },
            () =>
                this.setState({
                    selected_range: `${this.state.fromDate} - ${this.state.toDate}`
                })
        )
    }


    selectedOption = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }




    render() {

        return (
            <>
                <div className="report-chart childcontainer-inner">
                    <div className="report-range-header">
                        <div className="report-header-left">
                            <select name="vendor" className="report-range-vendor-dropdown filter child-info-dropdowns" onChange={this.selectedOption}>
                                <option>Select Vendors</option>
                                {this.state.dealer_list.map(dealer =>
                                    <option key={dealer.account_number} value={dealer.dealer_name}>{dealer.dealer_name}</option>
                                )};

                            </select>
                            <div className="report-custom-range-container" ref={node => { this.node = node; }}>
                                <div className="report-calendar report-btn">
                                    <img className="report-calendar-icon" onClick={this.handleClick} src='/img/childcontainer/report/calendar.png' alt="Time Frame" title="Time Frame" />
                                    <span className="report-calendar-label childcontainer-value">{this.state.selected_range}</span>
                                </div>
                                {this.state.visible ? (<div className="report-range-custom-wrapper">
                                    <div className="report-range-days">
                                        <ul className="report-range-list">
                                            <li className="report-range-li" onClick={this.setData}>Last 24 hours</li>
                                            <li className="report-range-li" onClick={this.setData}>Last 7 days</li>
                                            <li className="report-range-li" onClick={this.setData}>Last 30 days</li>
                                            <li className="report-range-li" onClick={this.setData}>Last 90 days</li>
                                        </ul>
                                    </div>
                                    <div className="report-range-date">
                                        <p className="report-range-date-heading">Specify a custom date range</p>
                                        <div id="start-date" className="report-range-dates">
                                            <p className="">Start Date</p>
                                            <input type="date" name="fromDate" onChange={this.setDate} className="report-range-date-picker" />
                                        </div>
                                        <div id="end-date" className="report-range-dates">
                                            <p className="">End Date</p>
                                            <input type="date" name="toDate" onChange={this.setDate} className="report-range-date-picker" />
                                        </div>
                                    </div>
                                </div>) : ('')}
                            </div>
                            <div className="report-btn" onClick={this.changeData}>
                                <img className="report-icons" src='/img/childcontainer/report/report.png' alt="Generate Report" title="Generate Report" />
                                <span className="childcontainer-value">Report</span>
                            </div>

                        </div>
                        <div className="report-chart-buttons">
                            <div className="report-btn" onClick={this.exportData}>
                                <img className="report-icons" src='/img/childcontainer/report/export.png' alt="Export" title="Export" />
                                <span className="childcontainer-value">Export</span>
                            </div>
                        </div>
                    </div>

                    <Bar id="ctx" data={this.state.data} getElementAtEvent={(elems, event) => { console.log(elems) }} />
                    <div className="report-stats">
                        <p className="report-stats-heading child-value">Overall Analytics</p>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Total Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">123ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Maximum Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">222ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Average Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">876ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Minimum Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">456ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Total Downtime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">45ms</p>
                        </div>
                    </div>

                    <div className="report-stats">
                        <p className="report-stats-heading child-value">Vendor Name: Vendor Name</p>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Total Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">123ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Maximum Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">222ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Average Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">876ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Minimum Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">456ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Total Downtime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">45ms</p>
                        </div>
                    </div>
                    <div className="report-stats">
                        <p className="report-stats-heading child-value">Vendor Name: Vendor Name</p>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Total Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">123ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Maximum Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">222ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Average Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">876ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Minimum Uptime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">456ms</p>
                        </div>
                        <div className="report-stats-block">
                            <label className="report-stats-label child-label">Total Downtime</label>
                            <p className="report-stats-dots">:</p>
                            <p className="report-stats-value child-value">45ms</p>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}


// const mapStateToProps = (state) => {
//     return {
//         property: state.topbar.property,
//         username: state.topbar.username,

//     }
// }

export default Report;
