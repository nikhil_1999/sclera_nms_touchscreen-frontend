import React, { Component } from 'react'
import { connect } from 'react-redux';
import Clock from 'react-live-clock'
import './childcontainer.css'

// Import components
import Report from './report_container/Report';
import Setting from './settings_container/Setting';
import HomeContainer from './home_container/HomeContainer';
import ContactContainer from './contact_container/ContactContainer';
import DashboardContainer from './dashboard_container/DashboardContainer';
import RemoteAccessContainer from './remote_access_container/RemoteAccessContainer';
import * as NavbarActionType from '../../../actions/NavbarActions';



class ChildContainer extends Component {

    displayPage = () => {
        switch (this.props.selected_choice) {
            case 'home': return <HomeContainer />;
            case 'contact': return (<ContactContainer />)
            case 'report': return (<Report />)
            case 'setting': return (<Setting />)
            case 'dashboard': return (<DashboardContainer />)
            case 'remote_access': return (<RemoteAccessContainer />)
            default: return (<DashboardContainer />)
        }
    }

    render() {
        return (
            <div className="childcontainer">
                <div className="childcontainer-box">
                    {this.displayPage()}
                    <div className="widgets">
                        <div className="date-time">
                            <p className="time"><Clock format={'hh:mm A'} ticking={true} /></p>
                            <p className="date"><Clock format={'dddd, MMMM, YYYY'} ticking={true} /></p>
                        </div>
                        <div className="weather-widget">
                            <a className="weatherwidget-io" href="https://forecast7.com/en/34d05n111d09/arizona/" data-label_1="ARIZONA" data-label_2="WEATHER" data-theme="dark" >ARIZONA WEATHER</a>
                            {!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = 'https://weatherwidget.io/js/widget.min.js'; fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'weatherwidget-io-js')}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        selected: option =>
            dispatch({
                type: NavbarActionType.SELECTED_CHOICE,
                payload: option
            })
    }
}

const mapStateToProps = (state) => {
    return {
        selected_choice: state.navbar.selected_choice
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChildContainer);