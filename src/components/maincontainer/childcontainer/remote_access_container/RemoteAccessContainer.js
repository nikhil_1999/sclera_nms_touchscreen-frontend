import React, { Component } from 'react';
import './remoteAccessContainer.css'

class RemoteAccess extends Component {
    state = {  }
    render() { 
        return ( 
        <div className="childcontainer-inner">
            <div className="remote-container">
                <p className="childcontainer-heading">Remote Access</p>
                <div className="childcontainer-left remote-left">
                    <p className="remote-text">There are no sessions live at the moment!</p>
                </div>
                <div className="childcontainer-right">

                </div>
            </div>
        </div>
 );
    }
}
 
export default RemoteAccess;