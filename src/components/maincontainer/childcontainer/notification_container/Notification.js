import React, { Component } from 'react'
import './notification.css';
import axios from 'axios';
import * as homeActionTypes from '../../../../actions/HomeActions';
import { link, AlarmTypes } from '../../../../config'
import { connect } from 'react-redux';
import DeviceInfo from '../home_container/maininfo/device_list/device_info/DeviceInfo';
import * as NavbarActionType from '../../../../actions/NavbarActions';
import './notification.css';

const epochs = [
    ['year(s)', 31536000],
    ['month(s)', 2592000],
    ['day(s)', 86400],
    ['hour(s)', 3600],
    ['minute(s)', 60],
    ['second(s)', 1]
];

class Notification extends Component {
    state = {
        notificationlist: [],
        loading: true,
        show_card: false,
        interval:null,
        epoch:null
    }

    componentDidMount() {
        axios.get(`${link}/notification/all`)
            .then(res => {
                console.log('notificationnn', res.data);
                this.setState({ notificationlist: JSON.parse(res.data), loading: false })
            })
            .catch(err => console.log(err));

    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            axios.get(`${link}/notification/all`)
                .then(res => {
                    console.log(res.data);
                    this.setState({ notificationlist: JSON.parse(res.data), loading: false })
                })
                .catch(err => console.log(err));
        }
    }
    
    showDeviceInfo = () => {
        return (<DeviceInfo />)
    }

    handleClick = (device) => {
        this.props.set_device(device) // for card 
    }
    closeNotifications = () => {
        this.props.reset_device()
        this.props.set_notification(false)
    }

    summaryCard = (device) => {
        return (<div>
            <p>{device.ip_addresses}</p>

        </div>)

    }
    render() {
        if (this.state.loading) {
            return (
                <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
            )
        } else {

            let list = this.state.notificationlist.map(device => {
                console.log(device)
                return (
                    <div className="device-container devicelist-card" key={device._id} onClick={() => this.handleClick(device)}>
                        <p className="devicelist-name childcontainer-value">{device.user_data_name !== null ? device.user_data_name : (device.display_name !== null ? device.display_name : device.hw_address)}</p>
                        {/* <p className="devicelist-location childcontainer-value">{AlarmTypes[`${device.alarm}`]}</p> */}
                        <p className="devicelist-location childcontainer-value">{device.location}</p>
                        <div className={device.status === 0 ? "devicelist-status childcontainer-status childcontainer-offline" : "devicelist-status childcontainer-status childcontainer-online"}></div>
                    </div >
                )
            })
            return (
                <div className="notification slide" id="notification">
                    <div className="maininfo-btn-container notification-header">
                        <p className="notification-heading">Notifications</p>
                        <img className="notification-close" src="/img/childcontainer/notification/close.png" onClick={this.closeNotifications} />
                    </div>

                    <div className="maininfo-card-info">
                        {this.props.selected_device !== null ? this.showDeviceInfo() : list}
                    </div>
                    {/* <div className="childcontainer-btn-container maininfo-btn-container">
                    <button className="childcontainer-btn maininfo-btn" onClick={this.closeNotifications}>Close</button>
                </div> */}
                </div>
            )

        }
    }
}

const mapStateToProps = (state) => {
    return {
        systemtype: state.home.systemtype,
        building: state.home.building,
        floor: state.home.floor,
        location: state.home.location,
        selected_device: state.home.selected_device,
        show_notification: state.navbar.show_notification,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        set_device: device =>
            dispatch({
                type: homeActionTypes.SELECTED_DEVICE,
                payload: device
            }),
        set_notification: option =>
            dispatch({
                type: NavbarActionType.SHOW_NOTIFICATION,
                payload: option
            }),
        reset_device: () =>
            dispatch({
                type: homeActionTypes.RESET_SELECTED_DEVICE,

            })
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(Notification);


