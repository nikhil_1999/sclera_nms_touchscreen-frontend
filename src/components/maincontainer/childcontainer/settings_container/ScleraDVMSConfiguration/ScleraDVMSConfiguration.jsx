import React, { Component } from 'react';
import {link, axiosRequest, scleradmslink, dockerserverlink, mainserver} from '../../../../../config';
import  axios from 'axios';
import './scleradmvsconfiguration.css';
import Loader from '../Loader/Loader';
import AlertModal from '../AlertModal/Alertmodal';
import Notifications from '../Notifications/Notifications'

class ScleraDVMSConfiguration extends Component {
    
    constructor(props){
        super(props);
        this.props_copy = JSON.parse(JSON.stringify(props));
        this.state = {
            interface:null,
            mac_address: null,
            all_ports: [],
            static: false,
            dhcp: false,
            vlan_id : false,
            ip: false,
            gateway: false,
            prefix: false,
            primary_dns: false,
            secondary_dns: false,
            data: [],
            edit:true,
            editstatic:false,
            showloader:false,
            configured: true,
            shownotify:false
        }

    }

    componentDidUpdate(prevProps){

    }

    getConfiguration = () =>{

        this.setLoader();

        axiosRequest(`${link}/api/settings/get/systemdetails`, null, "GET", { 'Content-Type': 'application/JSON' } )
        .then((res) =>{
           
            if(res.status === 200){
                if(res.data){
                    console.log(res.data)
                    this.setState({
                        interface: res.data.interface,
                        mac_address: res.data.mac_address,
                        vlan_id: res.data.vlan_id,
                        tagged: res.data.tagged,
                        untagged: res.data.untagged,
                        dhcp: res.data.dhcp,
                        static: res.data.static,
                        ip: res.data.ip,
                        gateway: res.data.gateway,
                        prefix: res.data.prefix,
                        primary_dns: res.data.primary_dns,
                        secondary_dns: res.data.secondary_dns,
                        configured: res.data.configured,
                        showloader : false,
                        edit:false
                    })
                }
                else{

                    this.setState({
                        configured: false,
                    },
                    () =>{

                        this.getDMVSPortList()
                    })
                }
            }
        })
        .catch((error) =>{

            this.setState({
                notificationtext:"Unable to connect to the Sclera DVMS",
                shownotify:true,
                notificationcolor:false
            })
        })
    }

    componentDidMount(){

        this.getConfiguration();
    }

    getDMVSPortList = () =>{

        this.setLoader();
        
        axiosRequest(scleradmslink.concat('/api/getInterfaces/all'), null, 'GET', { 'Content-Type': 'application/JSON' })
        .then((res) =>{

            if(res.status === 200){

                let data = res.data;
                if(data && Array.isArray(data.interfaces)){
                    // this.data = data.interfaces;
                    // this.populateData();
                    this.setState({
                        data: data.interfaces,
                        interface:null,
                        mac_address: null,
                        all_ports: [],
                        static: false,
                        dhcp: false,
                        vlan_id : false,
                        ip: false,
                        gateway: false,
                        prefix: false,
                        primary_dns: false,
                        secondary_dns: false,
                        edit: true,
                        editstatic:false,
                        showloader:false,
                    })
                }
                //modal --> unable to get list of ports from router
            }
            //catcha the error here and show a modal
        })
        .catch((error) =>{

            this.setState({
                notificationtext:"Unable to get the List of Interfaces",
                shownotify:true,
                
            })
        })
    }

    showLoader = () =>{

        return(
            <Loader></Loader>
        )
    }

    showPopupModal = () => { 

        return (
            <AlertModal modalcolor={this.state.modalcolor} modaltext={this.state.modaltext} onCancel={this.onCancel}></AlertModal>
        )  
    }
    
    showNotification = () => {
        setTimeout(
            this.resetNotification(),5000
        )
        return(
            <Notifications notificationcolor={this.state.notificationcolor} notificationtext={this.state.notificationtext}></Notifications>
        )
    }
    
    resetNotification =() => {
        this.setState({
            shownotify:false
        })
    }

    onCancel = () =>{
        this.setState({
            showpopup:false,
            modaltext: false,
        })

    }

    setLoader = () =>{

        this.setState({
            showloader:true
        })
    }

    generateScleraDVMSPortData = () =>{

        var network_object = [];

        if(this.state.vlan_id && this.state.interface){
           
            var interface_object = {};
            interface_object["interface"] = this.state.interface;
            var vlan_object = {};
            vlan_object["interface"] = this.state.interface;
            vlan_object['vlanid'] = parseInt(this.state.vlan_id);
            vlan_object['ip_address'] = this.state.ip;
            vlan_object['prefix'] = parseInt(this.state.prefix);
            
            if(this.state.dhp){
                vlan_object['dhcp']= true
            }
            if(this.state.tagged){
                vlan_object['tagged'] = true
            }

            vlan_object['gateway'] = this.state.gateway;
            vlan_object['nameserver'] = [this.state.primary_dns || "", this.state.secondary_dns || ""]
            
            network_object.push(interface_object)
            network_object.push(vlan_object);

            if(network_object.length === 2){

                this.configureScleraDVMSPort(network_object);
            }

        }
        else if(this.state.interface && !this.state.vlan_id){

            var interface_object = {};
            interface_object.interface = this.state.interface;
            interface_object.ip_address = this.state.ip;
            interface_object.prefix = parseInt(this.state.prefix);
            interface_object.gateway = this.state.gateway;

            if(this.state.dhp){
                interface_object.dhcp= true
            }

            interface_object.nameserver = [this.state.primary_dns || "", this.state.secondary_dns || ""]
            
            network_object.push(interface_object);

            if(network_object.length === 1){

                 this.configureScleraDVMSPort(network_object);
            }
        }
    }

    configureScleraDVMSPort = (network_object) =>{

        this.setLoader();

        axios.post(`${scleradmslink}/api/setIPaddresses`,network_object,{timeout: 5000})
            .then((res) => {
                this.setState({
                    notificationtext:"IP Setup Successfull",
                    shownotify:true
                })
                this.saveDatainDB();
                
            })
            .catch(error =>{

                this.setState({
                    notificationtext:"IP Setup Unsuccessfull",
                    shownotify:true
                })
                this.saveDatainDB();
            });

    }

    saveDatainDB = () =>{

        var vlan_object = {};
        vlan_object["interface"] = this.state.interface;
        vlan_object['vlan_id'] = parseInt(this.state.vlan_id);
        vlan_object['ip'] = this.state.ip;
        vlan_object['prefix'] = parseInt(this.state.prefix);

        if(this.state.dhp){
            vlan_object['dhcp']= true
        }
        else{
            vlan_object['static']= true
        }
        if(this.state.tagged){
            vlan_object['tagged'] = true
        }
        else{
            vlan_object['untagged'] = true
        }

        vlan_object['gateway'] = this.state.gateway
        vlan_object['primary_dns'] = this.state.primary_dns;
        vlan_object['secondary_dns'] = this.state.secondary_dns;
        vlan_object['configured'] = true;
        vlan_object['mac_address'] = this.state.mac_address

        axiosRequest(`${link}/api/settings/insert/systemdetails`,vlan_object, "POST",{ 'Content-Type': 'application/JSON' })
            .then((res) => {
    
                this.getConfiguration();
            })
            .catch(error =>{

                this.setState({
                    notificationtext:"Unable to save data in database",
                    shownotify:true
                })
                this.getConfiguration();
                //show warning -> unable to save data in database
            });
    }

    netmask2CIDR = (netmask) =>{

        if(netmask ){

            return (netmask.split('.').map(Number)
            .map(part => (part >>> 0).toString(2))
            .join('')).split('1').length -1;
        }
        else{

            return null
        }
        
    }

    handlePortChange = (e) =>{

        let selected_interface = {};
        for(let i=0; i< this.state.data.length; i++){

            if(e.target.value === this.state.data[i].name){

                selected_interface = this.state.data[i]
            }
        }

        if(e.target.value) {

            this.setState({
                interface:e.target.value,
                mac_address: selected_interface.mac_address || "",
                static: selected_interface.static || false,
                dhcp: selected_interface.dhcp || false,
                vlan_id : selected_interface.vlan_id || "",
                ip: selected_interface.ip || "",
                gateway: selected_interface.gateway_ip || "",
                prefix: this.netmask2CIDR(selected_interface.netmask || ""),
                primary_dns: selected_interface.primary_dns || "",
                secondary_dns: selected_interface.secondary_dns || "",
            })
        }
        else{

            this.setState({
                mac_address: null,
                static: false,
                dhcp: false,
                vlan_id : false,
                ip: false,
                gateway: false,
                prefix: false,
                primary_dns: false,
                secondary_dns: false,
                edit: false,
                editstatic:false
            })
        }
    }

    handleisp =(value) => {

        if(this.state.edit){

            if(value==="static") {

                this.setState({
                    static:true,
                    dhcp:false,
                    editstatic:true
                })
            }
            else if(value==="dhcp") {

                this.setState({
                    static:false,
                    dhcp:true,
                    editstatic:false
                })
            }
        }
    }

    handleChange=(e) => {
        
        this.setState({
            [e.target.name]: e.target.value
        })

    }

    editData = () =>{

        if(this.state.static){

            this.setState({
                edit: true,
                editstatic:true
            })
        }
        else if(this.state.dhcp){

            this.setState({
                edit: true,
            })
        }
        else{

            this.setState({
                edit: true,
                editstatic:true
            })
        }
    }

    renderData = () =>{

        return(
            <div className="sclera-data-main-section">
                <div className="sclera-radio-form-row">
                    <div className="settings-radio-form" >
                        <div className="radio-button-row">
                            <div className={this.state.static?"radiobutton radiobutton-selected":"radiobutton"} 
                            onClick={(e)=>{this.handleisp("static")}} >
                            </div>
                            <p className="settings-label">Static</p>
                        </div>
                        <div className="radio-button-row">  
                            <div className={this.state.dhcp?"radiobutton radiobutton-selected":"radiobutton"} 
                            onClick={(e)=>{this.handleisp("dhcp")}} >
                            </div>
                            <p className="settings-label">DHCP</p>
                        </div>
                    </div>
                </div>
                <div className="isp-form-row">
                    <div className="settings-form-data">
                        <p className="settings-label"> VLAN ID</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            readOnly={!this.state.edit}
                            name="vlan_id"
                            onChange={(e)=>this.handleChange(e)}
                            value= {this.state.vlan_id || ""}
                        /> 
                    </div>
                </div>
                <div className="isp-form-row">
                    <div className="settings-form-data">
                        <p className="settings-label">IP Address</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            readOnly={!this.state.editstatic}
                            name="ip"
                            onChange={(e) =>this.handleChange(e)}
                            value=  {this.state.ip || ""}
                        /> 
                    </div>   
                    <span className="vendor-slash">/</span>
                    <div className="settings-form-data-prefix">
                        <p className="settings-label"> Prefix</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            readOnly={!this.state.editstatic}
                            name="prefix"
                            onChange={(e) =>this.handleChange(e)}
                            value= {this.state.prefix || ""}
                        /> 
                    </div>
                    <div className="settings-form-data">
                        <p className="settings-label"> Gateway</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            readOnly={!this.state.editstatic}
                            name="gateway"
                            onChange={(e) =>this.handleChange(e)}
                            value= {this.state.gateway || ""}
                        /> 
                    </div>
                </div>
                <div className="isp-form-row">
                    <div className="settings-form-data">
                        <p className="settings-label">Primary DNS</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            readOnly={!this.state.editstatic}
                            name="primary_dns"
                            onChange={(e) =>this.handleChange(e)}
                            value= {this.state.primary_dns || ""}
                        /> 
                    </div>
                    <div className="settings-form-data">
                        <p className="settings-label">Secondary DNS</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            readOnly={!this.state.editstatic}
                            name="secondary_dns"
                            onChange={(e) =>this.handleChange(e)}
                            value= {this.state.secondary_dns || ""}
                        /> 
                    </div>     
                </div>    
            </div>
        )
    }

    renderAllPorts = () =>{

        return(
            <div className="isp-form-row">
                <div className="settings-form-data">
                    <p className="settings-label">DMVS Port</p>
                    {this.state.configured
                        ?<input 
                            type="text" 
                            className="settings-input" 
                            readOnly={true}
                            value={this.state.interface || ""} 
                        />
                        :<select className="settings-input" onChange={this.handlePortChange} >
                            <option defaultValue value= ""></option> 
                            {this.state.data.map((data)=>(
                                <option value={data.name} key={`${data.name}`}>{data.name}</option>
                            ))}
                        </select>
                    }
                </div>
                <div className="settings-form-data">
                    <p className="settings-label">MAC Address</p>
                    <input 
                        type="text" 
                        className="settings-input" 
                        readOnly={true}
                        value={this.state.mac_address || ""} 
                    />
                </div>
                {!this.state.configured 
                    ?<div className="sclera-config-button-form">
                        <div className="router-buttons">
                            
                                <button className="save-button" onClick = {this.generateScleraDVMSPortData}>
                                <img src={require('../../../../../img/settings/iconawesome-save.png')}  alt="save logo"/>
                                    
                                </button>
                           
                        </div>
                    </div>
                    :null
                }  
            </div>
        )
    }

    render() { 
        return ( 
            <div className="sclera-config-container">
                {this.state.shownotify
                    ?this.showNotification()
                    :null}
                {this.state.showpopup
                    ?this.showPopupModal()
                    :null}
                {this.state.showloader
                    ?<div className="sclera-loader-section">
                        {this.showLoader()}
                    </div>
                    :<div className="Sclera-main-section">
                        <div className="sclera-port-main-section">
                            {this.renderAllPorts()}
                        </div>
                        <div className="sclera-data-main-section">
                            {this.renderData()}
                        </div>
                    </div>
                }
            </div>
        );
    }
}
 
export default ScleraDVMSConfiguration;