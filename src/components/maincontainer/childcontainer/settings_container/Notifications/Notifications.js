import React, { Component } from 'react';
import { toast, Slide,Zoom, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import './notification.css';

toast.configure({
            position: "top-center",
            autoClose: 5000,
            pauseOnHover: true,
            draggable: true,
            closeOnClick: true,
            transition:  Slide,
            hideProgressBar: true,
            newestOnTop: true,
            className: {

                border: '1px solid red ',
            }
        })

class Notfications extends Component {
    constructor(props){
        super(props)
        this.state={
            showtext:props.notificationtext,
            showcolor:props.notificationcolor,
        }
        console.log(props.notificationcolor)
    }

    notify = (text,color) => {
        
        toast.dark(<div  className={color?"toast-section-red":"toast-section"}>
        <p 
                        className="notification-header-text"
                    >
                        {text}
                    </p></div>
                    );
    }
   
    render() { 
        return(
            <div>
                      {this.notify(this.state.showtext,this.state.showcolor)}
                    
                    
            </div>
            
        )
       
    }
}
 
export default Notfications;