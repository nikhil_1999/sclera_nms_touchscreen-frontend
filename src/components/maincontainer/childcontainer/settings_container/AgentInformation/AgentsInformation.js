import React, { Component } from 'react';
import AuthenticationModal from './../AuthenticationModal/AuthenticationModal_AgentAuth';
import {mainserver, axiosRequest, link,dockerserverlink} from './../../../../../config';
import Loader from '../Loader/Loader'
import './agentInformation.css';
import AlertModal from '../AlertModal/Alertmodal';
import Notifications from '../Notifications/Notifications';

class AgentsInformation extends Component {
    
    constructor(props) {
        super(props)
        this.state= {
            active: this.props.information.active,
            property_code: "",
            agent_id: "",
            activated: false,
            save: false,
            auth: false,
            auth_modal: false,
            showloader:true,
            showpopup:false,
            shownotify:false,
            api: mainserver.concat("/agents/activateagent"),
            edit:false,
        }
    }

    componentDidMount(){

        this.getAgentInformation()
    }

    UNSAFE_componentWillReceiveProps = (props) => {
        this.setState(
            {
                active: props.information.active,
                property_code: "",
                agent_id: "",
                activated: false,
                save: false,
                auth_modal: false,
                auth: false
            }
        )

        this.getAgentInformation();
    }

    getAgentInformation = () => {

        axiosRequest(link.concat("/api/property/getDetails"), null, "GET", { 'Content-Type': 'application/JSON' })
        .then((res) =>{
            console.log(res.data)
            this.setState({
                agent_id: res.data.dvms_id,
                property_code: res.data.property_code,
                activated: res.data.activate_agent,
                showloader:false,
                showpopup:false,
                shownotify:false
                }    
            )
        })
        .catch((e) => {
            this.setState({
                showloader:true,
                notificationtext:"Unable to connect to Sclera DVMS",
                shownotify:true
            })
        })
    }
    
    showLoader = () =>{

        return(
            <Loader></Loader>
        )
    }

    showPopupModal = () => { 

        return (
            <AlertModal modalcolor={this.state.modalcolor} modaltext={this.state.modaltext} onCancel={this.onCancel}></AlertModal>
        )  
    }

    showNotification = () => {

        setTimeout(
            this.resetNotification(),5000
        )
        return(
            <Notifications notificationcolor={this.state.notificationcolor} notificationtext={this.state.notificationtext}></Notifications>
        )
    }

    resetNotification =() => {
        this.setState({
            shownotify:false
        })
    }

    onCancel = () => {

        this.setState({
            showpopup:false
        })

        this.getAgentInformation();
    }

    changePropertyCode = (e) => {

        this.property_code = e.currentTarget.value;
        this.setState({
            property_code: this.property_code
        })
    }

    changeAgentId = (e) => {

        this.agent_id = e.currentTarget.value;
        this.setState({
            agent_id: this.agent_id
        })
    }


    saveAgentInfomration = () => {

        this.setState({
            auth_modal: true,
        })

    }

    feedback = () => {

        this.props.information.onExplore("agents_information");

    }

    generateAuthenticationProps = () => {

        const information = {

            onCancel: this.cancelAuth,
            onDone: this.authenticated,
            data: {
                agent_id: this.state.agent_id,
                property_code: this.state.property_code,
                type: "agent_activation"
            },
            api: this.state.api
        }

        return information;
    }

    authenticated = (username) =>{

        var data  = {
            dvms_id: this.state.agent_id,
            username: username,
            property_code : this.state.property_code,
            activate_agent : true
        }

        axiosRequest(link.concat("/api/property/activate_agent/"), data, "PUT", { 'Content-Type': 'application/JSON' })
        .then((res) =>{
            this.setState({
                auth_modal: false,
                showpopup:false
            })
        })
        .catch((e) => {
            this.setState({
                notificationtext:"Unable to Activate the Agent",
                shownotify:true
            })
        })

        this.getAgentInformation();
    }

    cancelAuth = () => {

        this.setState({
            auth_modal: false,
            save: false
        })
    }

    render() {
            return (
               
                <div className="row mtp-10">
                    {this.state.shownotify
                        ?this.showNotification()
                        :null}
                     {this.state.showpopup
                        ?this.showPopupModal()
                        :null}
                    {!this.state.showloader?
                        <div className="agent-showloader">
                            <div className="isp-form-row">
                            <div className="settings-form-data">
                                <label className="settings-label">Property Code</label>
                                <input className="settings-input" onChange={this.changePropertyCode} value={this.state.property_code || ""} disabled={this.state.property_code && true} />
                            </div>
                            <div className="settings-form-data">
                                <label className="settings-label">Agent Id</label>
                                <input className="settings-input" onChange={this.changeAgentId} value={this.state.agent_id || ""} disabled={this.state.property_code && true}/>
                            </div>
                            {!this.state.activated
                                ?<div className="settings-form-buttons">
                                    <button className="settings-button" onClick={this.saveAgentInfomration}>Save</button>
                                    <button className="settings-button" onClick={this.feedback}>Cancel</button>
                                </div>
                                :null
                            }
                            
                            </div>
                            {this.state.auth_modal?
                                <AuthenticationModal information={this.generateAuthenticationProps()}  />
                            :null
                            }
                        </div>
                     :<div className="agentinfo-loader-section">
                        {this.showLoader()}
                    </div>
                     }
                </div>
               
                   
                
            );

    }
}

export default AgentsInformation;