import React, { Component } from 'react';
import Vendor from '../Vendor/Vendor';
import {mainserver, link, axiosRequest,dockerserverlink} from '../../../../../config'
import AddVendor from '../AddVendor/AddVendor';
import Loader from '../Loader/Loader';
import './systeminformation.css';
import AlertModal from '../AlertModal/Alertmodal';
import Notifications from '../Notifications/Notifications'

class SystemInformation extends Component {

    constructor(props){
        super(props);
        this.state = {  
            active: props.information.active,
            showloader: 3,
            agent_id: null,
            property_code: null,
            activated: false,
            active_vendor_id : "",
            vendor_type_array : [],
            vendors_information: [],
            showpopup:false,
            shownotify:false,
         }

    }

    componentDidMount = () =>{

        this.getAgentInformation();
    }

    getAgentInformation = () =>{

        axiosRequest(link.concat("/api/property/getDetails"), null, "GET", { 'Content-Type': 'application/JSON' })
        .then((res) =>{
            this.setState({
                agent_id: res.data.dvms_id,
                property_code: res.data.property_code,
                activated: res.data.activate_agent,
                showloader: this.state.loader-1,
                errorportal:false,
                },
                () =>{
                    this.checkActivation()
                } 
            )
        })
        .catch((e) => {
            this.setState({
                notificationtext:"Unable to connect to Local Server",
                shownotify:true,
            })
            //show popup "Unable to connect to local server"
        })
    }

    showLoader = () =>{

        return(
            <Loader></Loader>
        )
    }

    showPopupModal = () => { 

        return (
            <AlertModal modalcolor={this.state.modalcolor} modaltext={this.state.modaltext} onCancel={this.onCancel}></AlertModal>
        )  
    }

    showNotification = () => {

        return(
            <Notifications notificationcolor={this.state.notificationcolor} notificationtext={this.state.notificationtext}></Notifications>
        )
    }

    onCancel = () => {

        this.setState({
            showpopup:false,
            modaltext:false
        })
        this.feedback();
    }

    checkActivation = () =>{

        if(this.state.activated){

            this.getSystemTypes();
            this.getVendorInformation();

        }
        else{
            this.setState({
                modaltext:"Activate the agent",
                shownotify:true
            })
            //show popup with text "Activate the agent"
        }
    }

    getSystemTypes = () =>{

        axiosRequest(mainserver.concat("/systemtypes"), null, "GET", { 'Content-Type': 'application/JSON' })
            .then((res) =>{
                
                if(res.status === 200 && Array.isArray(res.data)){

                    this.setState({
                        vendor_type_array: res.data,
                        showloader: this.state.loader-1,
                        showpopup:false
                    })
                }
                else{
                    this.setState({
                        notificationtext:"Unable to get system types",
                        shownotify:true
                    })
                    // show popup unable to get system types
                }
            })
            .catch((error) =>{
                this.setState({
                    notificationtext:"Unable to connect to Server",
                    shownotify:true
                })
                //show popup "Unable to connect to server"
            })
    }

    getVendorInformation = () =>{

        axiosRequest(link.concat("/api/settings/getdockers"), null, "GET", { 'Content-Type': 'application/JSON' })
        .then((res) =>{
            if(res.status === 200 && Array.isArray(res.data)){
                
                this.setState({
                    vendors_information: res.data,
                    showloader: this.state.loader-1,
                    shownotify:false
                })

            }
            
        })
        .catch((error) =>{
            this.setState({
                notificationtext:"Unable to connect to Server",
                shownotify:true
            })
            //show popup "Unable to connect to server"
        })
    }   

    showVendorsList = () =>{

        this.setState({
            active : !this.state.active,
            active_vendor_id : ""
        })
      
    }

    showVendorInformation = (key) =>{

        console.log(key);

        if(key === this.state.active_vendor_id){
            this.setState({
                active_vendor_id : ""
            })
        }
        else{
            this.setState({
                active_vendor_id : key
            })
        }
    }

    reloadData = () =>{

        this.setState({
            showloader: 3,
            agent_id: null,
            property_code: null,
            activated: false,
            active_vendor_id : "",
            vendor_type_array : [],
            vendors_information: [],
            showpopup:false,
            shownotify:false,
        },
        ()=>{
            this.getAgentInformation();
        })
        
    }

    generateVendorInformation = (vendorInfo) =>{

        console.log(vendorInfo);
        console.log(this.state);

        let information = {
            ...vendorInfo,
            vendor_type_array: this.state.vendor_type_array,
            onExplore :this.showVendorInformation,
            onSave: this.reloadData,
            active : this.state.active_vendor_id === vendorInfo._id ? true: false,
            print: this.printState
        }
        return information;
    }

    printState = () =>{

        console.log("state is", this.state);
    }

    generateNewVendorInfromation = () =>{

        let information = {
            onExplore : this.showVendorInformation,
            active : this.state.active_vendor_id === "__New" ? true: false,
            vendor_type_array: this.state.vendor_type_array,
            agent_id: this.state.agent_id
        }
        return information;
    }

    feedback = () =>{

        this.props.information.onExplore("system_information");
    }

    showLoader = () =>{

        return(
            <Loader></Loader>
        )
    }

    

    handleSystemChange = (e) =>{

        for( var i =0; i< this.state.vendors_information; i++){
            if(this.state.vendors_information[i].system_type === e.target.value){
                this.setState({
                    selected_vendor: {...this.state.vendors_information[i]}
                })
                break
            }
        }
    }

    renderDetails = () =>{
        
        return (
            <React.Fragment>
                <div >
                    <ul>
                        {this.state.vendors_information.map(vendorInfo =>
                                (<Vendor key={vendorInfo._id} information = {this.generateVendorInformation(vendorInfo)} />)
                        )}
                    </ul>
                    <AddVendor information ={this.generateNewVendorInfromation()}/>
                </div>
            </React.Fragment>
        )
    }
    

    render() { 
        if(this.state.active){
            return (
                <div className = "vendor-information mtp-10">
                     {this.state.shownotify
                        ?this.showNotification()
                        :null}
                     {this.state.showpopup
                        ?this.showPopupModal()
                        :null}
                    {this.state.showloader?
                        this.showLoader():
                        this.renderDetails()}
                </div>
             );
        }
        else{
            return (
               null
             );
        }
        
    }
}
 
export default SystemInformation;