import React, { Component } from 'react';
import './routerconfiguration.css';
import {routerapilinks, axiosRequest} from './../../../../../config';
// import {SELECTED_MODE,SCLERA_PORT} from './../../../../../actions/RouterConfigurationAction';
import {connect} from "react-redux";
import ISPConfig from './ISPConfig/ISPConfig'
import CreateScleraNetwork from './CreateScleraNetwork/CreateScleraNetwork'
import AlertModal from '../AlertModal/Alertmodal';
import Notifications from '../Notifications/Notifications'

class RouterConfiguration extends Component {

    constructor(props){

        super(props);
        this.props_copy = JSON.parse(JSON.stringify(props));
        this.state = {
            selected_mode : "no-router",
            selected_port : null,
            sclera_port: null,
            available_ports : [],
            isp_panel: false,
            create_sclera_network: false,
            shownotify:false
        }
    }

    componentDidUpdate(prevProps) {

        if(prevProps !== this.props){
            if(!this.props.information.active && (this.state.isp_panel || this.state.create_sclera_network)){
                this.setState({
                    isp_panel: false,
                    create_sclera_network: false
                })
            }
        }
        
    }

    sendFeedback = () =>{

        if(!this.state.isp_panel && !this.state.create_sclera_network) {

            this.props.information.onExplore("router_information");
        }
        
    }

    getListofScleraPorts = () =>{

        axiosRequest(routerapilinks.concat('/api/getScleraPort'), null, 'GET', { 'Content-Type': 'application/JSON' })
        .then((res) =>{

            if(res.status === 200){

                let data = res.data;

                if(data && Array.isArray(data.all_ports)){

                    this.setState({
                        available_ports : data.all_ports,
                        sclera_port: data.sclera_port,
                        selected_port: data.sclera_port,
                    },
                    this.updateSelectedPortToStore
                    )
                }
                //modal --> unable to get list of ports from router
            }
            //catcha the error here and show a modal
        })
        .catch((error) =>{
            this.setState({
                notificationtext:"Unable to connect to local server",
                shownotify:true
            })
        })

    }

    saveScleraPort = () =>{

        axiosRequest(routerapilinks.concat('/api/saveScleraPort'), {sclera_port: this.state.selected_port}, 'POST', { 'Content-Type': 'application/JSON' })
        .then((res) =>{
            if(res.status === 200){

                let data = res.data;

                if(data['success']){

                    this.setState({
                        sclera_port: this.state.selected_port,
                    },
                    this.updateSelectedPortToStore
                    )
                    //show a modal ->sclera port selected
                }
                else{
                    this.setState({
                        notificationtext:"Sclera port failed to update",
                        shownotify:true
                    })
                    //show a modal ->sclera port failed to update
                }
                this.setState({
                    notificationtext:"Unable to get list of ports from router",
                    shownotify:true
                })
                //modal --> unable to get list of ports from router
            }

            //catcha the error here and show a modal
            
        })
        .catch((error) => {
            this.setState({
                notificationtext:"Unable to connect to local server",
                shownotify:true
            })
        })

    }

    showPopupModal = () => { 

        return (
            <AlertModal modalcolor={this.state.modalcolor} modaltext={this.state.modaltext} onCancel={this.onCancel}></AlertModal>
        )  
    }
    
    showNotification = () => {

        return(
            <Notifications notificationcolor={this.state.notificationcolor} notificationtext={this.state.notificationtext}></Notifications>
        )
    }

    onCancel = () =>{
        this.setState({
            showpopup:false,
            modaltext: false,
        })
    }

    updateSelectedPortToStore = () =>{

        this.props.selectedPort(this.state.sclera_port);
    }
   
    switchTabs = (key) =>{

            if(key === "isp_panel"){

                this.setState({
                    isp_panel : !this.state.isp_panel,
                    create_sclera_network: false,
                },
                this.sendFeedback())
            }
            else if(key === "create_sclera_network"){

                this.setState({
                    isp_panel : false,
                    create_sclera_network: !this.state.create_sclera_network,
                },
                this.sendFeedback())
            }
        }

    ispPanel = () =>{

        if(this.state.isp_panel){

            return(
                <div className="router-container">
                    <div className="settings-type" onClick = {() =>{
                        this.switchTabs("isp_panel");
                    }}>
                        <p className="settings-type-text">ISP Configuration</p>
                        <img src={require(`../../../../../img/settings/dropdown_close.png`)} alt="drop down "/>
                    </div>
                    <div className="router-section">
                        <ISPConfig></ISPConfig>
                    </div>
                </div>
            )
        }
        else{

            return(
                <div className="router-container">
                    <div className="settings-type" onClick = {() =>{
                        this.switchTabs("isp_panel");
                    }}>
                        <p className="settings-type-text">ISP Configuration</p>
                        <img src={require(`../../../../../img/settings/dropdown_open.png`)} alt="drop down "/>
                    </div>                    
                </div>
            )
        }
    }

    createScleraNetworkPanel = () =>{

        if(this.state.create_sclera_network){

            return(
                <div className="router-container">
                    <div className="settings-type" onClick = {() =>{
                        this.switchTabs("create_sclera_network");
                    }}>
                        <p className="settings-type-text">Create Sclera Network</p>
                        <img src={require(`../../../../../img/settings/dropdown_close.png`)} alt="drop down "/>
                    </div>
                    <div className="router-section">
                        <CreateScleraNetwork information = {this.generateCreateScleraNetworkInformation()}></CreateScleraNetwork>
                    </div>
                   
                </div>
            )
        }
        else{

            return(
                <div className="router-container">
                    <div className="settings-type" onClick = {() =>{
                        this.switchTabs("create_sclera_network");
                    }}>
                        <p className="settings-type-text">Create Sclera Network</p>
                        <img src={require(`../../../../../img/settings/dropdown_open.png`)} alt="drop down "/>
                    </div>               
                </div>
            )
        }

    }

    generateCreateScleraNetworkInformation = () =>{

        const information = {
            sclera_port: this.state.sclera_port
        }
        return information
    }

    routerModeCallback = () =>{
        
        this.props.selectedMode("router");
        this.props.selectedPort(null);
        this.getListofScleraPorts()
        
    }

    noRouterModeCallBack = () =>{

        this.props.selectedMode("no-router");
        this.props.selectedPort(null);
     }

    handleSelection = (selection) =>{

        if(selection === "router" && this.state.selected_mode !== "router"){

            this.setState({
                selected_mode : "router",
                available_ports : []
            },
            this.routerModeCallback()
            );

        }
        else if(selection === "no-router" && this.state.selected_mode !== "no-router"){

            this.setState({
                selected_mode : "no-router",
                available_ports : []
            },
            this.noRouterModeCallBack())

        }
    }

    handlePortChange = (e)=> {

        this.setState({
            selected_port:e.target.value
        })
    }

    renderConfigurationMode = () =>{

        return(
            <div className = "configuration-mode">
                <div className = "settings-mode">
                    <div className="settings-radio-button">
                        <div
                            className={
                            this.state.selected_mode === "router"
                                ? "mode-radio mode-radio-selected"
                                : "mode-radio"
                            }
                            // onClick = {() =>{this.handleSelection("router")}}
                        >
                        <div className="mode-inner-radio"></div>
                        </div>
                    </div>
                   
                    <div className = "mode-label">
                        Router
                    </div>
                </div>
                <div className = "settings-mode">
                    <div className="settings-radio-button">
                        <div
                            className={
                            this.state.selected_mode === "no-router"
                                ? "mode-radio mode-radio-selected"
                                : "mode-radio"
                            }
                            onClick = {() =>{this.handleSelection("no-router")}}
                        >
                            <div className="mode-inner-radio"></div>
                        </div>
                    </div>
                    <div className = "mode-label">
                        No Router
                    </div>
                </div>
            </div>
        )
    }



    renderPorts = () =>{

        if(this.state.selected_mode === "router"){
            
            return(
                <div className="settings-sclera-form">
                    <div className="settings-sclera-form-data">
                        <p className="settings-label"> Sclera Port</p>
                        {this.state.sclera_port?
                            <div className="settings-input" >
                                {this.state.sclera_port}
                            </div>
                            :
                            <select className="settings-input" onChange={this.handlePortChange} >
                                <option defaultValue value={this.state.selected_port}  disabled = {this.state.sclera_port}>{this.state.selected_port}</option> 
                                {this.state.available_ports.map((data)=>(
                                    <option value={data} key={`${data}`}>{data}</option>
                                ))}
                            </select>
                        }
                    </div>
                    <div className="sclera-port-button">
                        {this.state.sclera_port?
                            null:  
                            <button className="save-button" onClick = {this.saveScleraPort}>
                                <img src={require(`../../../../../img/settings/iconawesome-save.png`)} alt="drop down "/>
                            </button>
                        }
                    </div>
                </div>
            )
        }
        else{

            return null
        }
    }

    render() { 

        return ( 
            <div className="settings-main-container">
                {this.state.shownotify
                    ?this.showNotification()
                    :null}
                {this.state.showpopup
                    ?this.showPopupModal()
                    :null}
                <div className="settings-mode-form">
                    {this.renderConfigurationMode()}
                    {this.renderPorts()}
                </div>
                <div className="settings-main-section">
                    {this.state.selected_mode === "router"?this.ispPanel():null}
                </div>
                <div className="settings-main-section">
                    {this.state.selected_mode === "router"?this.createScleraNetworkPanel():null}
                </div>
            </div>
        );

        
    }
}

 
// const mapStateToProps = (state /*, ownProps*/) => {

//     return {
//       mode: state,
//     }
//   }
  
// const mapDispatchToProps = dispatch => {

//     return {
//       selectedMode: (mode) => dispatch({ type: SELECTED_MODE, payload: mode }),
//       selectedPort: (port) => dispatch({ type: SCLERA_PORT, payload: port }),
//     }
// }
export default connect(

    null,
    null
  ) (RouterConfiguration)