import React, { Component } from 'react';
import './ispconfig.css'
import {routerapilinks, axiosRequest} from './../../../../../../config';
import Loader from '../../Loader/Loader';
import AlertModal from '../../AlertModal/Alertmodal';
import Notifications from '../../Notifications/Notifications'

class ISPConfig extends Component {
    constructor(props){
        super(props);
        this.props_copy = JSON.parse(JSON.stringify(props));
        this.state = {
            dhcp: false,
            static: false,
            tagged: false,
            untagged: false,
            ip_address: null,
            port: null,
            vlan_id: null,
            gateway: null,
            mask: null,
            primary_dns: null,
            secondary_dns: null,
            available_ports: [],
            selected_port: null,
            edit: false,
            editstatic:false,
            showloader:false,
            shownotify:false
        }
        
    }

    componentDidUpdate(prevProps) {
        // console.log(this.props.mode);
    }

    componentDidMount(){
        this.getAvailablePorts();
        this.getPortData();
    }

    getAvailablePorts = () =>{

        axiosRequest(routerapilinks.concat('/api/AllPorts'), null, 'GET', { 'Content-Type': 'application/JSON' }).then((res) =>{
            console.log(res.data)
            if(res.status === 200){
                let data = res.data;
                if(data && Array.isArray(data.ports)){
                    this.setState({
                        available_ports: data.ports,
                        selected_port: null   
                    })

                    // catcha error -> unable to get ports
                }
            }

            //catcha the error here and show a modal
            
        })
        .catch((error) => {
            this.setState({
                notificationtext:"Unable to get Ports",
                shownotify:true
            })
        })
    }

    getPortData = () =>{
        
        if(this.state.selected_port){
            axiosRequest(routerapilinks.concat('/api/getNetworkDetail'), {port: this.state.selected_port}, 'POST', { 'Content-Type': 'application/JSON' })
            .then((res) =>{
                console.log(res.data)
                if(res.status === 200){
                    let data = res.data;
                    if(data){
                        this.setState({
                            static: data.static,
                            dhcp:data.dhcp,
                            tagged: data.tagged,
                            untagged: !data.tagged,
                            ip_address: data.ip_address,
                            vlan_id: data.vlan_id,
                            gateway: data.gateway,
                            mask: data.subnet,
                            primary_dns: data.primary_dns,
                            secondary_dns: data.secondary_dns,
                            edit: false
    
                        })
                    }
                    else{
                        this.setState({
                            edit: true,
                            static: true,
                            tagged: true,
                            notificationtext:"Unable to get list of ports from router",
                            shownotify:true
                        })
                    }
                    
    
                    //modal --> unable to get list of ports from router
                }
    
                //catcha the error here and show a modal
                
            })
            .catch((error) =>{
                this.setState({
                    notificationtext:"Unable to Connect to the local Router",
                    shownotify:true
                })
            })
        }
    }

    handlePortChange = (e) =>{

        this.setState({
            selected_port : e.target.value,
            dhcp: false,
            static: false,
            tagged: false,
            untagged: false,
            ip_address: null,
            port: null,
            vlan_id: null,
            gateway: null,
            mask: null,
            primary_dns: null,
            secondary_dns: null,
            edit: false,
            editstatic:false
        },() =>{
            this.getPortData()
        })

    }
    

    handleisp =(value) => {
        if(this.state.edit){
            if(value==="static") {
                this.setState({
                    static:true,
                    dhcp:false,
                    editstatic:true
                })
            }
            else if(value==="dhcp") {
                this.setState({
                    static:false,
                    dhcp:true,
                    editstatic:false
                })
            }
        }
       
    }
    handleip =(e) =>{
        this.setState({
            ip_address:e.target.value
        });
    }
    handlevlanid =(e) =>{
        
        this.setState({
            vlan_id:e.target.value
        });
    }
    handlegateway=(e) =>{
        this.setState({
            gateway:e.target.value
        });
    }
    handlemask =(e) =>{
        this.setState({
            mask:e.target.value
        });
    }
    handleprimarydns =(e) =>{
        this.setState({
            primary_dns:e.target.value
        });
    }    
    handlesecondarydns =(e) =>{
        this.setState({
            secondary_dns:e.target.value
        });
    }  

    handletag =(value) => {
        if(this.state.edit){
            if(value==="tagged"){
                this.setState({
                    tagged:true,
                    untagged:false
                })
            }
            else if(value=== "untagged") {
                this.setState({
                    tagged:false,
                    untagged:true
                })
            }
        }   
    }
    setLoader = () =>{
        this.setState({
            showloader:true
        })
    }
    
    showLoader = () =>{

        return(
            <Loader></Loader>
        )
    }

    showPopupModal = () => { 

        return (
            <AlertModal modalcolor={this.state.modalcolor} modaltext={this.state.modaltext} onCancel={this.onCancel}></AlertModal>
        )  
    }
    
    showNotification = () => {

        return(
            <Notifications notificationcolor={this.state.notificationcolor} notificationtext={this.state.notificationtext}></Notifications>
        )
    }

    onCancel = () =>{
        this.setState({
            showpopup:false,
            modaltext: false,
        })
    }

    saveData = () => {
        this.setLoader()

        if(this.state.edit){
            const data = {
                port: this.state.selected_port,
                static: this.state.static,
                dhcp: this.state.dhcp,
                tagged: this.state.tagged,
                vlan_id: this.state.vlan_id,
                ip_address: this.state.ip_address,
                gateway: this.state.gateway,
                subnet: this.state.mask,
                primary_dns: this.state.primary_dns,
                secondary_dns: this.state.secondary_dns
            
            }

            axiosRequest(routerapilinks.concat('/api/saveVlandb'), data, 'POST', { 'Content-Type': 'application/JSON' })
            .then((res) =>{
                console.log(res.data)
                if(res.status === 200){
                    let data = res.data;
                    if(data["success"]){
                        //give success modal,
                        this.setState({
                            dhcp: false,
                            static: false,
                            tagged: false,
                            untagged: false,
                            ip_address: null,
                            port: null,
                            vlan_id: null,
                            gateway: null,
                            mask: null,
                            primary_dns: null,
                            secondary_dns: null,
                            available_ports: [],
                            edit: false,
                            editstatic:false,
                            showloader:false
                        },
                        this.getPortData(),
                        this.getAvailablePorts())

                    }
                    
    
                    //modal --> error msge
                }
    
                //catcha the error here and show a modal
                
            })
            .catch((error) => {
                this.setLoader()
                this.setState({
                    notificationtext:"Unable to Connect to the local Router",
                    shownotify:true
                })
            })
        }

        

    }
    editMode =() => {
        if(this.state.static){
            this.setState({
                edit: true,
                editstatic:true
            })
        }
        else{
            this.setState({
                edit: true,
               
            })
        }
            
      
    }

    renderStaticFields = () =>{

        return(
            <div className="isp-form-section">
                <div className="isp-form-row">
                    <div className="settings-form-data">
                        <p className="settings-label">Port</p>
                        <select className="settings-input" onChange={(e)=>this.handlePortChange(e)} >
                            <option defaultValue value=""></option>
                            {this.state.available_ports.map((data)=>(
                                <option value={data} key={`${data}`}>{data}</option>
                            ))}
                        </select>
                    </div>
                    <div className="settings-form-data">
                        <p className="settings-label">VLAN ID</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            value={this.state.vlan_id || ""} 
                            readOnly={!this.state.edit} 
                            onChange={(e)=>this.handlevlanid(e)}
                        />
                    </div>
                </div>
                <div className="isp-form-row">
                    <div className="settings-form-data">
                        <p className="settings-label">IP Address</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            value= {this.state.ip_address || ""} 
                            readOnly={!this.state.editstatic} 
                            onChange={(e)=>this.handleip(e)}
                        />
                    </div>
                    <div className="settings-form-data">
                        <p className="settings-label"> Gateway</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            value={this.state.gateway || ""} 
                            readOnly={!this.state.editstatic} 
                            onChange={(e)=>this.handlegateway(e)}
                        />
                    
                    </div>
                    <div className="settings-form-data">
                        <p className="settings-label">Mask</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            value= {this.state.mask || ""} 
                            readOnly={!this.state.editstatic} 
                            onChange={(e)=>this.handlemask(e)}
                        /> 
                    </div>
                </div>
                <div className="isp-form-row">
                    <div className="settings-form-data">
                        <p className="settings-label">Primary DNS</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            value={this.state.primary_dns || ""} 
                            readOnly={!this.state.editstatic} 
                            onChange={(e)=>this.handleprimarydns(e)}
                        />
                    
                    </div>
                    <div className="settings-form-data">
                        <p className="settings-label"> Seconday DNS</p>
                        <input 
                            type="text" 
                            className="settings-input" 
                            value={this.state.secondary_dns || ""} 
                            readOnly={!this.state.editstatic} 
                            onChange={(e)=>this.handlesecondarydns(e)}
                        />
                    </div>
               </div>
            </div>
        )
    }

    renderDHCPFields = () =>{

        return null;

    }

    renderBasicFields = () =>{

        return(
            <div className="basic-checkbox">
               <div className="settings-radio-form" >
                    <div className="radio-button-row">
                        <div className={this.state.static?"radiobutton radiobutton-selected":"radiobutton"} 
                        onClick={(e)=>{this.handleisp("static")}} >
                        </div>
                        <p className="settings-label">Static</p>
                    </div>
                    <div className="radio-button-row">
                        <div className={this.state.dhcp?"radiobutton radiobutton-selected":"radiobutton"} 
                        onClick={(e)=>{this.handleisp("dhcp")}} >
                        </div>
                        <p className="settings-label">DHCP</p>
                    </div>
                </div>
                <div className="settings-radio-form">
                    <div className="radio-button-row">
                        <div className={this.state.tagged?"radiobutton radiobutton-selected":"radiobutton"} 
                        onClick={(e)=>{this.handletag("tagged")}} >
                        </div>
                        <p className="settings-label">Tagged</p>
                    </div>
                    <div className="radio-button-row">
                        <div className={this.state.untagged?"radiobutton radiobutton-selected":"radiobutton"} 
                        onClick={(e)=>{this.handletag("untagged")}} >
                        </div>
                        <p className="settings-label"> Untagged</p>
                    </div>
                </div>
                <div className="router-buttons">
                    {this.state.edit?
                        <button  className="save-button" onClick = {this.saveData}>
                            <img src={require('../../../../../../img/settings/iconawesome-save.png')}  alt="save logo"/>
                        </button>
                    :null}
                    {!this.state.edit?
                        <button  className="edit-button" onClick = {this.editMode}>
                            <img src={require('../../../../../../img/settings/iconawesome-edit.png')} alt="edit logo"/>
                        </button>
                    :null}
                </div>
            </div>
        )
    }

    render() { 

        return(
                 <div className="isp-container">
                     {this.state.shownotify
                        ?this.showNotification()
                        :null}
                    {this.state.showpopup
                        ?this.showPopupModal()
                        :null}
                    {this.state.showloader?
                        <div className="isp-loader-section">
                            {this.showLoader()}
                        </div>
                    :
                    <div className="isp-main-container">
                        {this.renderBasicFields()}
                        {this.renderStaticFields()}
                        {this.renderDHCPFields()}
                    </div>
                }
                    
                </div>
            
        )
        
    }
}
 
export default ISPConfig;