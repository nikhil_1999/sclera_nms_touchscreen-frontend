import React, { Component } from 'react';
import './createscleranetwork.css';
import {routerapilinks, axiosRequest} from './../../../../../../config';
import Loader from '../../Loader/Loader';
import AlertModal from '../../AlertModal/Alertmodal';
import Notifications from '../../Notifications/Notifications'

class CreateScleraNetwork extends Component {
    constructor(props){
        super(props);
        this.props_copy = JSON.parse(JSON.stringify(props))
        this.state = {
            ip_address: null,
            vlan_id: null,
            gateway: null,
            mask: null,
            primary_dns: null,
            secondary_dns: null,
            dhcp_server: false,
            dhcp_gateway: null,
            dhcp_network_address: null,
            dhcp_pool_range: null,
            edit: false,
            port: this.props.information.sclera_por,
            showloader:false,
            shownotify:false
        }
    }

    componentDidMount(){

        this.getScleraNetowrkInfomration()
    }

    componentDidUpdate(prevProps){

        if(prevProps !== this.props){
            this.setState({
                port: this.props.information.sclera_port
            },
            this.getScleraNetowrkInfomration())
        }

    }

    componentWillUnmount(){

    }

    getScleraNetowrkInfomration = () =>{

        if(this.state.port){
            axiosRequest(routerapilinks.concat('/api/getNetworkDetail'), {port: this.state.port}, 'POST', { 'Content-Type': 'application/JSON' })
            .then((res) =>{
                if(res.status === 200){
                    
                    let data = res.data;
                    console.log(res)
                    if(data){
                        this.setState({
                            static: data.static,
                            dhcp:data.dhcp,
                            tagged: data.tagged,
                            untagged: !data.tagged,
                            ip_address: data.ip_address,
                            vlan_id: data.vlan_id,
                            gateway: data.gateway,
                            mask: data.subnet,
                            primary_dns: data.primary_dns,
                            secondary_dns: data.secondary_dns,
                            dhcp_server: data.dhcp_server,
                            dhcp_gateway: data.dhcp_gateway,
                            dhcp_network_address: data.dhcp_network_address,
                            dhcp_pool_range: data.dhcp_pool_range,
                            edit: false
    
                        })
                    }
                    else{
                        this.setState({
                            edit: true,
                            static: true,
                            tagged: true,
                            notificationtext:"Unable to get list of ports from router",
                            shownotify:true
                        })
                    }
                    
    
                    //modal --> unable to get list of ports from router
                }
    
                //catcha the error here and show a modal
                
            })
            .catch((error) => {
                this.setState({
                    notificationtext:"Unable to Connect to the local Router",
                    shownotify:true
                })
            })

         }
    }

    showLoader = () =>{

        return(
            <Loader></Loader>
        )
    }

    setLoader = () =>{

        this.setState({
            showloader:true
        })
    }

    showPopupModal = () => { 

        return (
            <AlertModal modalcolor={this.state.modalcolor} modaltext={this.state.modaltext} onCancel={this.onCancel}></AlertModal>
        )  
    }
    
    showNotification = () => {

        return(
            <Notifications notificationcolor={this.state.notificationcolor} notificationtext={this.state.notificationtext}></Notifications>
        )
    }

    onCancel = () =>{

        this.setState({
            showpopup:false,
            modaltext: false,
        })
    }

    saveScleraNetowkInformation = () =>{

        this.setLoader()

        if(this.state.edit){

            const data = {
                port: this.state.port,
                static: this.state.static,
                dhcp: this.state.dhcp,
                tagged: this.state.tagged,
                vlan_id: this.state.vlan_id,
                ip_address: this.state.ip_address,
                gateway: this.state.gateway,
                subnet: this.state.mask,
                primary_dns: this.state.primary_dns,
                secondary_dns: this.state.secondary_dns,
                dhcp_server: this.state.dhcp_server,
                dhcp_gateway: this.state.dhcp_gateway,
                dhcp_network_address: this.state.dhcp_network_address,
                dhcp_pool_range: this.state.dhcp_pool_range,
                internet: true
        }

        axiosRequest(routerapilinks.concat('/api/saveVlandb'), data, 'POST', { 'Content-Type': 'application/JSON' })
        .then((res) =>{
            if(res.status === 200){
                console.log(res);
                let data = res.data;
                if(data["success"]){

                    //show success modal
                    this.setState({
                        ip_address: null,
                        vlan_id: null,
                        gateway: null,
                        mask: null,
                        primary_dns: null,
                        secondary_dns: null,
                        dhcp_server: false,
                        dhcp_gateway: null,
                        dhcp_network_address: null,
                        dhcp_pool_range: null,
                        port: this.props.information.sclera_port,
                        showloader:false
                    },
                    this.getScleraNetowrkInfomration())

                }
                else{
                    this.setState({
                        notificationtext:"Unable to Save VLAN to the Router",
                        shownotify:true
                    })
                    //modal -->  error msge
                }
                
            }

            //catcha the error here and show a modal
            
        })
        .catch((error) =>(
            this.setState({
                showloader:false,
                notificationtext:"Unable to Connect to the local Router",
                shownotify:true
            })
        ))

        }

        

    }

    handleChange = (e) => {

        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleDHCPServerToggle = () =>{
        
            this.setState({
                dhcp_server :!this.state.dhcp_server
            })
    }

    editMode = () =>{

        this.setState({
            edit: true
        })
    }
   
    dhcpServerCheckbox = () => {

        return(
            this.state.showloader?
                null
            :
                <div className="dhcp-checkbox">
                    <div className={this.state.dhcp_server?"dhcp-radiobutton radiobutton-selected":"dhcp-radiobutton"} 
                        onClick = {this.state.edit?this.handleDHCPServerToggle:null}>
                    </div>
                    <p className="settings-label">Enable DHCP Server</p>
                </div>
        )
    }

    renderDHCPServerData = () =>{

        if(this.state.dhcp_server){
            return(
                <div className="dhcp-section">       
                    {this.dhcpServerCheckbox()}
                    <div className="dhcp-main-container">
                        {this.state.showloader?
                            <div className="dhcp-loader-section">
                                <Loader></Loader>
                            </div>
                        :<div className="dhcp-container">
                            <div className="isp-form-dhcp-row">
                                <p className="settings-label">Enter The Gateway</p>
                                <div className="dhcp-input">
                                    <input 
                                        className="settings-input" 
                                        type="text" 
                                        value={this.state.dhcp_gateway || ""} 
                                        name="dhcp_gateway" 
                                        onChange={(e)=>this.handleChange(e)} 
                                        readOnly={!this.state.edit}
                                    />
                                </div>
                            </div>
                            <div className="isp-form-dhcp-row">
                                <p className="settings-label">Network Address /n</p>
                                <div className="dhcp-input">
                                    <input 
                                        className="settings-input" 
                                        type="text" 
                                        value={this.state.dhcp_network_address || ""}  
                                        name="dhcp_network_address" 
                                        onChange={(e)=>this.handleChange(e)} 
                                        readOnly={!this.state.edit}
                                    />
                                </div>
                            </div>
                            <div className="isp-form-dhcp-row">
                                <p className="settings-label"> Pool range</p>
                                <div className="dhcp-input">
                                    <input 
                                        className="settings-input" 
                                        type="text" 
                                        value={this.state.dhcp_pool_range || ""} 
                                        placeholder="xxx.xxx.xxx.xxx-xxx.xxx.xxx.xxx" 
                                        name="dhcp_pool_range"  
                                        onChange={(e)=>this.handleChange(e)} 
                                        readOnly={!this.state.edit}
                                    />
                                </div>
                            </div>
                        </div>
                        }
                    </div>
                </div>
            )
        }
        else{
            return(
                <div className="dhcp-section">
                     {this.dhcpServerCheckbox()}
                </div>
            )
        }

    }

    renderNetworkData = () =>{

        return(
            <div className="isp-form-section">
                <div className="setting-buttons">
                    <div className="router-buttons">
                        {this.state.edit?
                            <button className="save-button" onClick = {this.saveScleraNetowkInformation}>
                                <img src={require(`../../../../../../img/settings/iconawesome-save.png`)} alt="edit button" />
                            </button>
                        :null}
                        {!this.state.edit?
                            <button className="edit-button" onClick = {this.editMode}>
                                <img src={require(`../../../../../../img/settings/iconawesome-edit.png`)} alt="edit button"/>
                            </button>
                        :null}
                    </div>
                </div>
                <div className="isp-form-row">
                    <div className="settings-form-data">
                        <p className="settings-label">VLAN ID</p>
                        <input 
                            className="settings-input" 
                            type="text" 
                            value={this.state.vlan_id || ""} 
                            name="vlan_id" 
                            onChange={(e)=>this.handleChange(e)} 
                            readOnly={!this.state.edit}
                        />
                    </div>
                </div>
                <div className="isp-form-row">
                    <div className="settings-form-data">
                        <p className="settings-label">IP Address</p>
                        <input 
                            className="settings-input" 
                            type="text" 
                            value= {this.state.ip_address || ""} 
                            name="ip_address" 
                            onChange={(e)=>this.handleChange(e)} 
                            readOnly={!this.state.edit}
                        />
                    </div>
                    <div className="settings-form-data">
                        <p className="settings-label">Mask</p>
                        <input 
                            className="settings-input" 
                            type="text" 
                            value= {this.state.mask || ""} 
                            name="mask" 
                            onChange={(e)=>this.handleChange(e)} 
                            readOnly={!this.state.edit}
                        />
                    </div>
                    <div className="settings-form-data">
                        <p className="settings-label">Gateway</p>
                        <input 
                            className="settings-input" 
                            type="text" 
                            value= {this.state.gateway || ""} 
                            name="gateway" 
                            onChange={(e)=>this.handleChange(e)} 
                            readOnly={!this.state.edit}
                        />
                    </div>
                </div>
                <div className="isp-form-row">
                    <div className="settings-form-data">
                        <p className="settings-label">Primary DNS</p>
                        <input 
                            className="settings-input" 
                            type="text" 
                            value={this.state.primary_dns || ""} 
                            name="primary_dns" 
                            onChange={(e)=>this.handleChange(e)} 
                            readOnly={!this.state.edit}
                        />
                    </div>
                    <div className="settings-form-data">
                        <p className="settings-label">Secondary DNS</p>
                        <input 
                            className="settings-input" 
                            type="text" 
                            value={this.state.secondary_dns || ""} 
                            name="secondary_dns" 
                            onChange={(e)=>this.handleChange(e)} 
                            readOnly={!this.state.edit}
                        />
                    </div>
                </div>
            </div>
        )
    }

    render() { 
        return ( 
            <div className="sclera-container">
                <div className="static-container">
                {this.state.shownotify
                        ?this.showNotification()
                        :null}
                    {this.state.showpopup
                        ?this.showPopupModal()
                        :null}
                    {this.state.showloader?
                    <div className="sclera-isp-loader-section">
                        <Loader></Loader>    
                    </div>
                    :this.renderNetworkData()}
                </div>
                <div className="dhcp-container">
                    {this.renderDHCPServerData()}
                </div>
            </div>

        );
    }
}
 
export default CreateScleraNetwork;