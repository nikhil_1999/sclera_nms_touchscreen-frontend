import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import cancel from './../../../../../img/settings/popup/close.png';
import './alertmodal.css'

class Alertmodal extends Component {
    constructor(props) {
        super(props)
        this.modalElement = document.createElement('div');
        this.modalRoot = document.getElementById('root');
        this.state={
            showtext:props.modaltext,
            showcolor:props.modalcolor
        }
    }

    componentDidMount() {
        this.modalRoot.appendChild(this.modalElement);
    }

    componentWillUnmount() {
        this.modalRoot.removeChild(this.modalElement);
    }

    cancel = () => {
        this.props.onCancel()
    }
    

    render() { 
      
        return ReactDOM.createPortal(
                    <div id="alert-modal" className="alert-modal">
                        <div className="alert-modal-card">
                        <img src={cancel} className="authenticationModal-cancel" onClick={this.cancel} alt="Cancel" />
                        <p style={{color: this.state.showcolor}} className={this.state.showcolor?"alertmodal-header-text":"alertmodal-header-text alert-modaltext-red"}>{this.state.showtext}</p>
                       
                        </div>
                    </div>,
                this.modalElement
         );
    }
}
 
export default Alertmodal;