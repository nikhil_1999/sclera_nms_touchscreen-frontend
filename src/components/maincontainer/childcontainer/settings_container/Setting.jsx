import React from 'react';
import AgentsInformation from './AgentInformation/AgentsInformation';
import SystemInformation from './SystemInformation/SystemInformation';
import {axiosRequest,routerapilinks} from './../../../../config';
import RouterConfiguration from './RouterConfiguration/RouterConfiguration';
import ScleraDVMSConfiguration from './ScleraDVMSConfiguration/ScleraDVMSConfiguration';
import './Setting.css';


class Setting extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            agents_information: false,
            system_information: false,
            router_information: false,
            configure_sclera_device :false,
            create_network: false,
        };
    }

    componentWillMount() {
       
    }    
     
    

    componentWillUnmount() {

    }

    selectedOption = () => {

    }

    generateAgentsInformationInformation = () => {


        const information = {
            active: this.state.agents_information,
            onExplore: this.onExplore
        }

        console.log(this.state);

        return information;
    }

    generateVendorsInformationInformation = () => {


        const information = {
            active: this.state.system_information,
            onExplore: this.onExplore
        }

        return information;
    }

    generateRouterInfomration = () =>{
        const information = {
            active: this.state.router_information,
            onExplore: this.onExplore
        }

        return information;
    }

    generateScleraDMVSInformationInformation = () =>{

    }

    onExplore = (key) => {

        switch(key){
            case "agents_information":
                this.setState({
                    agents_information: !this.state.agents_information,
                    system_information: false,
                    router_information: false,
                    configure_sclera_device :false,
                    create_network: false,
                })
                break;
            case "system_information":
                this.setState({
                    agents_information: false,
                    system_information: !this.state.system_information,
                    router_information: false,
                    configure_sclera_device :false,
                    create_network: false,
                })
                break;
            case "router_information":
                this.setState({
                    agents_information: false,
                    system_information: false,
                    router_information: !this.state.router_information,
                    configure_sclera_device :false,
                    create_network: false,
                })
                break;
            case "configure_sclera_device":
                this.setState({
                    agents_information: false,
                    system_information: false,
                    router_information: false,
                    configure_sclera_device :!this.state.configure_sclera_device,
                    create_network: false,
                })
                break;
            case "create_network":
                this.setState({
                    agents_information: false,
                    system_information: false,
                    router_information: false,
                    configure_sclera_device :false,
                    create_network: !this.state.create_network,
                })
                break;
            default:
                break
        }

    }

    getAgentInformation = () => {
    
        if(this.state.agents_information === true){
            return(
                <div className="router-container">
                    <div className="settings-type" onClick = {() =>{
                            this.onExplore("agents_information");
                    }}>
                        <p className="settings-type-text">Activate Sclera</p>
                        <img src={require(`../../../../img/settings/dropdown_close.png`)} alt="drop down "/>
                    </div>
                    <div className="router-section">
                        <AgentsInformation information={this.generateAgentsInformationInformation()} />
                    </div>
                </div>
                )
            }
            else {
                return(
                    <div className="router-container">
                        <div className="settings-type" onClick = {() =>{
                            this.onExplore("agents_information");
                        }}>
                            <p className="settings-type-text">Activate Sclera</p>
                            <img src={require(`../../../../img/settings/dropdown_open.png`)} alt="drop down "/>
                        </div>
                    </div> 
                    )
            }
    } 

    getSystemInfomration = () => {
        if(this.state.system_information === true){
            return(
                <div className="router-container">
                    <div className="settings-type" onClick = {() =>{
                            this.onExplore("system_information");
                    }}>
                        <p className="settings-type-text">System Information</p>
                        <img src={require(`../../../../img/settings/dropdown_close.png`)} alt="drop down "/>
                    </div>
                    <div className="router-section">
                        <SystemInformation information={this.generateVendorsInformationInformation()} />
                    </div>
                </div>
                )
            }
            else {
                return(
                    <div className="router-container">
                        <div className="settings-type" onClick = {() =>{
                            this.onExplore("system_information");
                        }}>
                            <p className="settings-type-text">System Information</p>
                            <img src={require(`../../../../img/settings/dropdown_open.png`)} alt="drop down "/>
                        </div>
                    </div> 
                    )
            }
    }

    getScleraDMVSConfigurationInformation = () =>{
        if(this.state.configure_sclera_device === true){
            return(
                <div className="router-container">
                    <div className="settings-type" onClick = {() =>{
                            this.onExplore("configure_sclera_device");
                    }}>
                        <p className="settings-type-text">Configure Sclera Device</p>
                        <img src={require(`../../../../img/settings/dropdown_close.png`)} alt="drop down "/>
                    </div>
                    <div className="router-section">
                        <ScleraDVMSConfiguration information={this.generateScleraDMVSInformationInformation()} />
                    </div>
                </div>
                )
            }
            else {
                return(
                    <div className="router-container">
                        <div className="settings-type" onClick = {() =>{
                            this.onExplore("configure_sclera_device");
                        }}>
                            <p className="settings-type-text">Configure Sclera Device</p>
                            <img src={require(`../../../../img/settings/dropdown_open.png`)} alt="drop down "/>
                        </div>
                    </div> 
                    )
            }
    }

    render() {
        return (
            <React.Fragment>
                <p className="childcontainer-heading">Configuration</p>
                <div className="settings-parent">
                    <div className="settings-child">
                       
                        <div className="settings-scroll-section">
                            <RouterConfiguration information = {this.generateRouterInfomration()}></RouterConfiguration>
                            {this.getScleraDMVSConfigurationInformation()}
                            {this.getAgentInformation()}
                            {this.getSystemInfomration()}
                            
                    </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Setting