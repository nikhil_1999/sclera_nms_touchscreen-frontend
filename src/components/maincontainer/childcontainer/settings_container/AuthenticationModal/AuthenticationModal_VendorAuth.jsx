import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {mainserver, axiosRequest} from './../../../../../config';
import './authenticationModal.css';
import modalLockIcon from './../../../../../img/settings/popup/popup_icon.png';
import cancel from './../../../../../img/settings/popup/close.png';
import Loader from '../Loader/Loader'

class AuhtenticationModal_VendorAuth extends Component {
    

    constructor(props) {
        super(props);
        this.modalElement = document.createElement('div');
        this.modalRoot = document.getElementById('root');
        this.state = {
            active: props.information.active,
            api: props.information.api,
            vendor_id: props.information.vendor_id,
            agent_id: props.information.agent_id,
            system_type: props.information.system_type,
            type: props.information.type,
            username: null,
            password: null,
            wrong: null,
            showloader:false,
        }

    }

    componentWillReceiveProps = (props) => {

        this.setState({
            system_type: props.information.system_type,
            vendor_id: props.information.vendor_id,
        })

       

    }

    componentDidMount() {

        this.modalRoot.appendChild(this.modalElement);
    }

    componentWillUnmount() {

        this.modalRoot.removeChild(this.modalElement);
    }

    changeUsername = (e) => {

        this.setState({
            username : e.currentTarget.value
        })
    }

    changePassword = (e) => {

        this.setState({
            password : e.currentTarget.value
        })
    }

    showLoader = () =>{

        return(
            <Loader></Loader>
        )
    }

    save = () => {

        this.setState({
            showloader:true,
            wrong: null
        })

        if(this.state.type === "vendor_activation"){
            const auth_data = {
                username: this.state.username,
                password: this.state.password,
                vendor_id: this.state.vendor_id,
                system_type: this.state.system_type,
                agent_id: this.state.agent_id,
            }

            axiosRequest(this.state.api, auth_data, 'POST', { 'Content-Type': 'application/JSON' })
            .then(res => {
            
                if (res.status === 200 && !res.data.message) {
                   
                    this.setState({
                        showloader:false,
                    }, () =>{
                        this.props.information.onDone(this.state.username)
                    })
                }
                else{

                    this.setState({
                        wrong: res.data.message,
                        showloader:false
                    })
                }
            })
            .catch(err => {

                this.setState({
                    wrong: "Unable to connect to the server !!!",
                    showloader:false
                })
                return false
            })
        }
        


    }

    cancel = () => {

        this.props.information.onCancel();
    }

    render() {

            return ReactDOM.createPortal(
                <div id="authenticationModal" className="authenticationModal">
                    <div className="authenticationModal-card">
                        {this.state.showloader
                            ?<div className="auth-loader-section">
                                {this.showLoader()}
                            </div>
                            :<div className="authentication-card-section">
                                <img src={cancel} className="authenticationModal-cancel" onClick={this.cancel} alt="Cancel" />
                                <div className="authenticationModal-header">
                                    <img src={modalLockIcon} className="authenticationModal-header-icon" alt="" />
                                    <p className=" authenticationModal-header-text">Please enter your Sign In credentials to continue.</p>
                                </div>
                                <div className="row authenticationModal-block">
                                    <input className="settings-input authenticationModal-input" type="text" onChange={this.changeUsername} value = {this.state.username || ""} placeholder="Username" />
                                    <input className="settings-input authenticationModal-input" type="text" onChange={this.changePassword} value = {this.state.password || ""} placeholder="Password" />
                                    <button className="settings-button authenticationModal-confirm" onClick={this.save}>Confirm</button>
                                       
                                    {
                                        this.state.wrong
                                            ?<p className="settings-name authenticationModal-error">{this.state.wrong}</p>
                                            :null
                                    }
                                
                            </div>
                        </div>
                    }
                </div>
            </div>,
            this.modalElement
        )
    }
}

export default AuhtenticationModal_VendorAuth;