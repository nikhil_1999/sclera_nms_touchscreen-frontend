import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {mainserver, axiosRequest} from './../../../../../config';
import './authenticationModal.css';
import modalLockIcon from './../../../../../img/settings/popup/popup_icon.png';
import cancel from './../../../../../img/settings/popup/close.png';
import Loader from '../Loader/Loader'

class AuhtenticationModal_AgentAuth extends Component {
    

    constructor(props) {
        super(props);
        this.modalElement = document.createElement('div');
        this.modalRoot = document.getElementById('root');
        this.state = {
            active: props.information.active,
            api: props.information.api,
            property_code: props.information.data.property_code,
            agent_id: props.information.data.agent_id,
            type: props.information.data.type,
            username: null,
            password: null,
            wrong: null,
            showloader:false
        }

    }

    componentWillReceiveProps = (props) => {

       

    }

    componentDidMount() {

        this.modalRoot.appendChild(this.modalElement);
    }

    componentWillUnmount() {

        this.modalRoot.removeChild(this.modalElement);
    }

    changeUsername = (e) => {

        this.setState({
            username : e.currentTarget.value
        })
    }

    changePassword = (e) => {

        this.setState({
            password : e.currentTarget.value
        })
    }


    makeTheAPIcall = (auth_data) =>{

                //make the api call and based on the status, return true or false


    }


    save = () => {

        this.setState({
            showloader:true
        })
        if(this.state.type === "agent_activation"){
            const auth_data = {
                username: this.state.username,
                password: this.state.password,
                property_code: this.state.property_code,
                agent_id: this.state.agent_id,
                
            }

            axiosRequest(this.state.api, auth_data, 'POST', { 'Content-Type': 'application/JSON' })
            .then(res => {

                if (res.status === 204) {

                    this.props.information.onDone(this.state.username)
                    this.setState({
                        showloader:false
                    })
                }
                else{

                    this.setState({
                        wrong: "Please enter the valid credentials !!!",
                        showloader:false
                    })
                }
            })
            .catch(err => {

                this.setState({
                    wrong: "Unable to connect to the server",
                    showloader:false
                })
                return false
            })
        }
    }

    showLoader = () =>{

        return(
            <Loader></Loader>
        )
    }

    cancel = () => {

        this.props.information.onCancel();
    }

    render() {

            return ReactDOM.createPortal(
                <div id="authenticationModal" className="authenticationModal">
                    <div className="authenticationModal-card">
                        {this.state.showloader?
                            <div className="auth-loader-section">
                                {this.showLoader()}
                            </div>
                                :<div className="authentication-card-section">
                                    <img src={cancel} className="authenticationModal-cancel" onClick={this.cancel} alt="Cancel" />
                                    <div className="authenticationModal-header">
                                        <img src={modalLockIcon} className="authenticationModal-header-icon" alt="" />
                                        <p className=" authenticationModal-header-text">Please enter your Sign In credentials to continue.</p>
                                    </div>
                                    <div className="row authenticationModal-block">
                                        <input className="settings-input authenticationModal-input" type="text" onChange={this.changeUsername} value = {this.state.username || ""} placeholder="Username" />
                                        <input className="settings-input authenticationModal-input" type="text" onChange={this.changePassword} value = {this.state.password || ""} placeholder="Password" />
                                        <button className="settings-button authenticationModal-confirm" onClick={this.save}>Confirm</button>
                                        
                                        {
                                            this.state.wrong?
                                            <p className="settings-name authenticationModal-error">{this.state.wrong}</p>:
                                            null
                                        }
                                    
                                </div>
                            </div>
                        }
                    </div>
                </div>,
                this.modalElement
            )


    }
}

export default AuhtenticationModal_AgentAuth;