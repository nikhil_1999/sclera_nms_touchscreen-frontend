import React, { Component } from 'react';
import AuthenticationModal from './../AuthenticationModal/AuthenticationModal_VendorAuth';
import {link, axiosRequest, scleradmslink} from '../../../../../config';
import  axios from 'axios';
import './vendor.css';
import save from '../../../../../img/settings/iconawesome-save.png';
import edit from '../../../../../img/settings/iconawesome-edit.png';
import vendor_dropdown_open from './../../../../../img/settings/dropdown_open.png';
import vendor_dropdown_close from './../../../../../img/settings/dropdown_close.png';
import AlertModal from '../AlertModal/Alertmodal';
import Notifications from '../Notifications/Notifications'
import Loader from '../Loader/Loader';

class Vendor extends Component {


    constructor(props){

        super(props);
        this.props_copy = JSON.parse(JSON.stringify(props))
        this.state = {
            id: this.props_copy.information._id,
            ip: this.props_copy.information.ip,
            system_type: this.props_copy.information.system_type,
            vendor_id: this.props_copy.information.vendor_id,
            prefix: this.props_copy.information.prefix,
            interface:this.props_copy.information.interface,
            static: this.props_copy.information.static,
            dhcp :this.props_copy.information.dhcp,
            tagged: this.props_copy.information.tagged,
            untagged: this.props_copy.information.untagged,
            vlan_id: this.props_copy.information.vlan_id,
            gateway: this.props_copy.information.gateway,
            primary_dns: this.props_copy.information.primary_dns,
            secondary_dns: this.props_copy.information.secondary_dns,
            active: this.props_copy.information.active,
            docker_id: this.props_copy.information.docker_id,
            readOnly: true,
            button_name: edit,
            auth_modal: false,
            vendor_type_array: this.props_copy.information.vendor_type_array,
            dvms_interfaces: null,
            shownotify:false,
            showloader:false
        }   
    }

    componentDidUpdate(prevProps){
        if(prevProps.information.active !== this.props.information.active){
            this.setState({
                id: this.props.information._id,
                ip: this.props.information.ip,
                system_type: this.props.information.system_type,
                vendor_id: this.props.information.vendor_id,
                prefix: this.props.information.prefix,
                interface:this.props.information.interface,
                static: this.props.information.static,
                dhcp :this.props.information.dhcp,
                tagged: this.props.information.tagged,
                untagged: this.props.information.untagged,
                vlan_id: this.props.information.vlan_id,
                gateway: this.props.information.gateway,
                primary_dns: this.props.information.primary_dns,
                secondary_dns: this.props.information.secondary_dns,
                active: this.props.information.active,
                docker_id: this.props.information.docker_id,
                active: this.props.information.active,
                vendor_type_array: this.props.information.vendor_type_array,
                shownotify:false,
                showloader:false

            }, () =>{
                console.log("updated");
                this.validateData();
            })
        }
    }

    validateData = () =>{

        if(!this.state.docker_id && this.state.active){
            this.setState({
                button_name: save,
                readOnly: false
            },
            () =>{
                this.getDVMSInterfacesList()
            })
        }

    }
    showLoader = () =>{
        return(
            <Loader></Loader>
        )
    }

    showPopupModal = () => { 

        return (
            <AlertModal modalcolor={this.state.modalcolor} modaltext={this.state.modaltext} onCancel={this.onCancel}></AlertModal>
        )  
    }

    showNotification = () => {

        setTimeout(
            this.resetNotification(),5000
        )

        return(
            <Notifications notificationcolor={this.state.notificationcolor} notificationtext={this.state.notificationtext}></Notifications>          
        )
        
    }
    resetNotification =() => {
        this.setState({
            shownotify:false
        })
    }

    onCancel = () => {

        this.setState({
            showpopup:false,
            modaltext:false,
            
        })
    }

    getDVMSInterfacesList = () =>{
        
        axiosRequest(scleradmslink.concat('/api/getInterfaces/all'), null, 'GET', { 'Content-Type': 'application/JSON' })
        .then((res) =>{
            if(res.status === 200){
                let data = res.data;
                console.log(data);
                if(data && Array.isArray(data.interfaces)){
                    this.setState({
                        dvms_interfaces: data.interfaces
                    });
                }
                //modal --> unable to get list of ports from router
            }
            //catcha the error here and show a modal   
        })
        .catch((error) =>(
            this.setState({
                notificationtext:"unable to get list of ports from router",
                shownotify:true
            })
        )
        )


    }

    handlePortChange = (e) =>{

        var selected_interface = null

        for(var i =0; i< this.state.dvms_interfaces.length; i++){
            if(e.target.value === this.state.dvms_interfaces[i].name){
                selected_interface = this.state.dvms_interfaces[i];
                break;
            }
        }

        this.setState({
            interface: e.target.value,
            ip: selected_interface && selected_interface.ip_address,
            vlan_id: selected_interface && selected_interface.vlan_id,
            prefix: selected_interface && this.netmask2CIDR(selected_interface.netmask || ""),
            gateway: selected_interface && selected_interface.gateway_ip
        })

    }

    generateScleraDVMSPortData = () =>{

        this.setState({
            showloader:true
        })

        var network_object = [];

        if(this.state.vlan_id && this.state.interface){
           
            var interface_object = {};
            interface_object["interface"] = this.state.interface;
            var vlan_object = {};
            vlan_object["interface"] = this.state.interface;
            vlan_object['vlanid'] = parseInt(this.state.vlan_id);
            vlan_object['ip_address'] = this.state.ip;
            vlan_object['prefix'] = parseInt(this.state.prefix);

            if(this.state.dhp){
                vlan_object['dhcp']= true
            }
            if(this.state.tagged){
                vlan_object['tagged'] = true
            }
            vlan_object['gateway'] = this.state.gateway;
            vlan_object['nameserver'] = [this.state.primary_dns || "", this.state.secondary_dns || ""]
            
            network_object.push(interface_object)
            network_object.push(vlan_object);

            if(network_object.length === 2){
                this.configureScleraDVMSPort(network_object);
            }

        }
        else if(this.state.interface && !this.state.vlan_id){

            var interface_object = {};
            interface_object.interface = this.state.interface;
            interface_object.ip_address = this.state.ip;
            interface_object.prefix = parseInt(this.state.prefix);
            interface_object.gateway = this.state.gateway;

            if(this.state.dhp){
                interface_object.dhcp= true
            }

            interface_object.nameserver = [this.state.primary_dns || "", this.state.secondary_dns || ""]
            
            network_object.push(interface_object);

            if(network_object.length === 1){
                this.configureScleraDVMSPort(network_object);
            }
        }
    }

    configureScleraDVMSPort = (network_object) =>{

        axios.post(`${scleradmslink}/api/setIPaddresses`,network_object,{timeout: 5000})
            .then((res) => {
                
                this.saveDatainDB();  
                
            })
            .catch(error =>{
                
                this.saveDatainDB();
                 
               
            });

    }

    saveDatainDB = () =>{

        console.log(this.state);


        var vlan_object = {};
        vlan_object["interface"] = this.state.interface;
        vlan_object['vlan_id'] = parseInt(this.state.vlan_id);
        vlan_object['ip'] = this.state.ip;
        vlan_object['prefix'] = parseInt(this.state.prefix);

        if(this.state.dhp){
            vlan_object['dhcp']= true
        }
        else{
            vlan_object['static']= true
        }
        if(this.state.tagged){
            vlan_object['tagged'] = true
        }
        else{
            vlan_object['untagged'] = true
        }

        vlan_object['gateway'] = this.state.gateway
        vlan_object['primary_dns'] = this.state.primary_dns;
        vlan_object['secondary_dns'] = this.state.secondary_dns;

        axiosRequest(`${link}/api/settings/updatedocker/vendor/${this.state.vendor_id}/systemtype/${this.state.system_type}/${this.state.id}`,vlan_object, "POST",{ 'Content-Type': 'application/JSON' })
            .then((res) => {
                console.log(res);
                this.props.onSave();
                this.setState({
                    showloader:false,
                    notificationtext:"Vendor Setup Successfull",
                    shownotify:true,
                })
                
            })
            .catch((error) =>{
                
                this.setState({
                    notificationtext:"Vendor Setup Unsuccessfull",
                    shownotify:true,
                    showloader:false,
                })
            });
    }

    handleRadio = (key) =>{
        if(key === "tagged"){
            this.setState({
                tagged: true,
                untagged: false
            })
        }
        else if(key === "untagged"){
            this.setState({
                tagged: false,
                untagged: true
            })
        }
        else if(key === "dhcp"){
            this.setState({
                dhcp: true,
                static: false
            })
        }
        else if(key === "static"){
            this.setState({
                dhcp: false,
                static: true
            })
        }
    }

    
    editAndSubmit = (e) =>{

        if(this.state.readOnly === true && this.state.button_name === edit){
            this.setState({
                readOnly : false,
                button_name : save
            })
        }
        else{
 
            this.generateScleraDVMSPortData();            
        }
    }

    handleChnage = (e, key) =>{

        this.setState({
            [key]: e.target.value
        })
    }

    updateGateway = (e) => {

        this.gateway = e.currentTarget.value;
        const vendor_information = this.state.vendor_information;
        vendor_information.network_information.gateway = this.gateway;
        this.setState({
            vendor_information: vendor_information
        })

    }
    updateMask = (e) => {

        this.mask = e.currentTarget.value;
        const vendor_information = this.state.vendor_information;
        vendor_information.network_information.mask = this.mask;
        this.setState({
            vendor_information: vendor_information
        })

    }

    updateVendorType = (e) =>{

        this.vendor_type = e.currentTarget.value;
        const vendor_information = this.state.vendor_information;
        vendor_information.vendor_type = this.vendor_type;
        this.setState({
            vendor_information: vendor_information
        })
    }


    generateAuthenticationProps = () => {

        const information = {
            active : this.state.auth_modal,
            onSave : this.collapsVendorInfo,
            onCancel : this.cancelAuth,
            data:  this.state.vendor_information,
            api: this.state.api
        }

        return information;
    }

    collapsVendorInfo = () => {

        this.setState({
            active: false
        })
        this.props.information.onExplore(this.state.vendor_information._id);
        
    }

    cancelAuth = () =>{

        this.setState({
            auth_modal: false,
        })

    }

    createVendorTypeList() {

        let items = [];         
        for (let i = 0; i < this.state.vendor_type_array.length; i++) {             
             items.push(<option key={i} value={this.state.vendor_type_array[i]}>{this.state.vendor_type_array[i]}</option>);
        }
        return items;
    }


    renderAuthModal = () =>{

        return(
            <AuthenticationModal information = {this.generateAuthenticationProps()}></AuthenticationModal>
        )
    }

    netmask2CIDR = (netmask) =>{

        if(netmask ){
            return (netmask.split('.').map(Number)
            .map(part => (part >>> 0).toString(2))
            .join('')).split('1').length -1;
        }
        else{
            return null
        }
    }

    
    render() { 

        if(this.state.auth_modal){

            this.renderAuthModal()

        }
        else if(this.state.active){
            // if(this.state.authenticated){
                return(
                    <div id="vendor" className="vendor-main-container row " >
                        {this.state.shownotify
                            ?this.showNotification()
                            :null
                        }
                        {this.state.showpopup
                            ?this.showPopupModal()
                            :null
                        } 
                        {
                            this.state.showloader?
                            <div className="vendor-loader-section">
                                {this.showLoader()}
                                </div>
                        :
                        <div className="vendor-main-section">
                        <span className="    vendor-names" 
                            onClick = {(e) => {this.props.information.onExplore(this.state.id)}}
                        ><div className="vendor-text">
                            <p className ="settings-label"> Vendor ID:</p><p> {this.state.vendor_id}</p>
                            </div>
                            <div className="vendor-text">
                        <p className ="settings-label">System Type:</p><p> {this.state.system_type}</p>
                        </div>
                        <img className="vendor-dropdown" src={vendor_dropdown_close} onClick = {(e) => {this.props.information.onExplore(this.state.id)}} alt="Open"/>
                   
                            </span>
                             {/* <div className="isp-form-row-vendor">
                            <div className="settings-form-data">
                                <label className="settings-label">Vendor ID</label>
                                <input 
                                    className="settings-input" 
                                    type="text" 
                                    value={this.state.vendor_id} 
                                    disabled = {true}
                                />
                            </div>
                            <div className = "settings-form-data">
                                <label className="settings-label">System Type </label>
                                <input 
                                    className="settings-input" 
                                    type="text" 
                                    value={this.state.system_type} 
                                    disabled = {true}
                                />
                    </div> }*/}
                    <div className="isp-form-row-vendor">
                            <div className="settings-form-data">
                                <label className="settings-label">Interface</label>
                                {this.state.dvms_interfaces?
                                    <select className="settings-input" onChange={(e) => this.handlePortChange(e)} >
                                        <option defaultValue value= ""></option> 
                                            {this.state.dvms_interfaces.map((data,index)=>(
                                                <option value={data.name} key={data.name.concat(index)}>{data.name}</option>
                                            ))}
                                    </select>
                                    :
                                    <input 
                                        className="settings-input" 
                                        type="text" 
                                        value={this.state.interface || ""} 
                                        readOnly = {this.state.readOnly}
                                    />
                                }
                            </div>
                            {!this.state.docker_id?
                                <div className="settings-form-data">
                                <button className="edit-button align-right" 
                                    onClick = {this.editAndSubmit}
                                >
                                    <img src={this.state.button_name} alt="logo "/>
                                </button> 
                                </div>
                                :
                                null
                            }
                            </div>
                        
                        <div className="isp-form-row">
                            
                            
                        </div>
                        <div className="isp-form-row">
                            <div className="settings-form-data">
                                <label className="settings-label"> VLAN ID</label>
                                <input 
                                    className="settings-input" 
                                    type="text" 
                                    value={this.state.vlan_id || ""} 
                                    readOnly = {this.state.readOnly}
                                    onChange = {(e) =>{this.handleChnage(e,"vlan_id")}}
                                />
                            </div>
                            <div className="settings-radio-form-vendor " >
                                <div className="radio-button-row">
                                    <div className={this.state.static?"radiobutton radiobutton-selected":"radiobutton"} 
                                    onClick={(e)=>{this.handleRadio("static")}} >
                                    </div>
                                    <p className="settings-label">Static</p>
                                </div>
                                <div className="radio-button-row">  
                                    <div className={this.state.dhcp?"radiobutton radiobutton-selected":"radiobutton"} 
                                    onClick={(e)=>{this.handleRadio("dhcp")}} >
                                    </div>
                                    <p className="settings-label">DHCP</p>
                                </div>
                            </div>
                            <div className="settings-radio-form-vendor" >
                                <div className="radio-button-row">
                                    <div className={this.state.tagged?"radiobutton radiobutton-selected":"radiobutton"} 
                                    onClick={(e)=>{this.handleRadio("tagged")}} >
                                    </div>
                                    <p className="settings-label">Tagged</p>
                                </div>
                                <div className="radio-button-row">  
                                    <div className={this.state.untagged?"radiobutton radiobutton-selected":"radiobutton"} 
                                    onClick={(e)=>{this.handleRadio("untagged")}} >
                                    </div>
                                    <p className="settings-label">Untagged</p>
                                </div>
                            </div> 
                        </div>
                        <div className="isp-form-row">
                            <div className="settings-form-data">
                                <label className="settings-label">IP Address</label>
                                <input 
                                    className="settings-input" 
                                    type="text"  
                                    value = {this.state.ip || ""} 
                                    readOnly = {this.state.readOnly} 
                                    onChange = {(e) =>{this.handleChnage(e,"ip")}}
                                /> 
                            </div>
                            <span className="vendor-slash">/</span>
                            <div className="settings-form-data-prefix">
                                <label className="settings-label">Prefix</label>
                                <input 
                                    className="settings-input" 
                                    type="text"  
                                    value = {this.state.prefix || ""} 
                                    readOnly = {this.state.readOnly} 
                                    onChange = {(e) =>{this.handleChnage(e,"prefix")}} 
                                /> 
                            </div>
                            <div className="settings-form-data">
                                <label className="settings-label">Gateway</label>
                                <input 
                                    className="settings-input" 
                                    type="text"  
                                    value = {this.state.gateway || ""} 
                                    readOnly = {this.state.readOnly} 
                                    onChange = {(e) =>{this.handleChnage(e,"gateway")}}
                                />
                            </div>
                        </div>
                        <div className="isp-form-row">
                            <div className="settings-form-data">
                                <label className="settings-label">Primary DNS</label>
                                <input 
                                    className="settings-input" 
                                    type="text" 
                                    value={this.state.primary_dns || ""} 
                                    readOnly = {this.state.readOnly}
                                    onChange = {(e) =>{this.handleChnage(e,"primary_dns")}}
                                />
                            </div>
                            <div className="settings-form-data">
                                <label className="settings-label">Secondary DNS</label>
                                <input 
                                    className="settings-input" 
                                    type="text" value={this.state.secondary_dns || ""} 
                                    readOnly = {this.state.readOnly}
                                    onChange = {(e) =>{this.handleChnage(e,"secondary_dns")}}
                                />
                            </div>
                        </div>    
                        </div>
                        }   
                    </div>
                )
            // }
            // else{
            //     return(
            //         <div id="vendor" className = "vendor-container row">
            //             <span 
            //                 className="settings-label vendor-names" 
            //                 onClick = {(e) => {this.props.information.onExplore(this.state.id)}}
            //             >
            //                 {this.state.vendor_id}
            //             </span>
            //             <img 
            //                 className="vendor-dropdown" 
            //                 src={vendor_dropdown_close} 
            //                 onClick = {(e) => {this.props.information.onExplore(this.state.id)}} 
            //                 alt="Open"
            //             />
            //             <p className="settings-name">Sent for Verification</p> 
            //         </div>
                   
            //     )
            // }
            

        }
        else {
            return (
                <div id="vendor" className = "vendor-container row ">
                    <span 
                        className=" vendor-names" 
                        onClick = {(e) => {this.props.information.onExplore(this.state.id)}}
                    >
                        <div className="vendor-text">
                        <p className ="settings-label"> Vendor ID:</p> <p>{this.state.vendor_id}</p>
                        </div>
                        <div className="vendor-text">
                        <p className ="settings-label">System Type:</p> <p>{this.state.system_type}</p>
                        </div>
                        <img 
                        className="vendor-dropdown" 
                        src={vendor_dropdown_open} 
                        onClick = {(e) => {this.props.information.onExplore(this.state.id)}} 
                        alt="Open"
                    />
                    </span>
        
                </div>
            )
        }    
    }
}
 
export default Vendor;