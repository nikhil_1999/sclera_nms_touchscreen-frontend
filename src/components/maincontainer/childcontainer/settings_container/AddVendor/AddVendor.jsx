import React, { Component } from 'react';
import AuthenticationModal from './../AuthenticationModal/AuthenticationModal_VendorAuth';
import './addVendor.css';
import { mainserver } from '../../../../../config';
import AlertModal from '../AlertModal/Alertmodal';
import Notifications from '../Notifications/Notifications'

class AddVendor extends Component {

    constructor(props){

        super(props);
        this.state = {
            active: false,
            vendor_type_array : props.information.vendor_type_array,
            new_vendor_information :{
                vendor_id : "",
                system_type : ""
            },
            auth_modal: false,
            agent_id: props.information.agent_id,
            showpopup:false,
            shownotify:false
        };
    }

    componentWillReceiveProps(props){

        this.setState({
            active: props.information.active,
            vendor_type_array : props.information.vendor_type_array
        })
    }

    vendorId = (e) =>{

        this.vendor_id = e.currentTarget.value;
        const new_vendor_information = this.state.new_vendor_information;
        new_vendor_information.vendor_id = this.vendor_id;

        this.setState({
            new_vendor_information: new_vendor_information
        })
    }

    vendorType = (e) =>{  

        this.system_type = e.currentTarget.value
        const new_vendor_information = this.state.new_vendor_information;
        new_vendor_information.system_type = this.system_type;

        this.setState({
            new_vendor_information: new_vendor_information
        })
    }

    createVendorTypeList() {

        let items = [];         
        for (let i = 0; i < this.state.vendor_type_array.length; i++) {             
             items.push(<option key={i} value={this.state.vendor_type_array[i]}>{this.state.vendor_type_array[i]}</option>);
        }
        return items;
    }

    onVendorSubmit = (e) =>{

        this.setState({
            auth_modal: true,
        })

    }

    generateAuthenticationProps = () => {

        const information = {
            active : this.state.auth_modal,
            onDone : this.authenticated,
            onCancel : this.cancelAuth,
            vendor_id: this.state.new_vendor_information.vendor_id,
            system_type: this.state.new_vendor_information.system_type,
            api: mainserver.concat("/authenticate/vendor"),
            type: "vendor_activation",
            agent_id: this.state.agent_id,
        }

        return information;
    }

    showPopupModal = () => { 
        setTimeout(
            this.resetNotification(),5000
        )
        return (
            <AlertModal modalcolor={this.state.modalcolor} modaltext={this.state.modaltext} onCancel={this.onCancel}></AlertModal>
        )  
    }
    resetNotification =() => {
        this.setState({
            shownotify:false
        })
    }

    showNotification = () => {

        return(
            <Notifications notificationcolor={this.state.notificationcolor} notificationtext={this.state.notificationtext}></Notifications>
        )
    }

    onCancel = () =>{
        this.setState({
            showpopup: false,
            modaltext: false,
        })
    }

    authenticated = () =>{

        this.setState({
            new_vendor_information :{
                vendor_id : "",
                system_type : ""
            },
            auth_modal: false,
            shownotify: true,
            notificationtext: "Sent for authentication !!!"
        })

    }

    collapsVendorInfo = () => {

        this.setState({
            active: false
        })
        this.props.information.onExplore("__New");     

    }

    cancelAuth = () =>{

        this.setState({
            auth_modal: false,
        })

    }

    render() { 

        if(this.state.showpopup){

            this.showPopupModal()
        }
        else if(this.state.auth_modal){
            
            return(
                <AuthenticationModal information = {this.generateAuthenticationProps()}/> 
            )
        }
        else if(this.state.active){

            return (
                <div className = "isp-form-row">
                    {this.state.shownotify?
                    this.showNotification():
                    null}
                    <div className = "settings-form-data">
                        <label className="settings-label">Vendor ID</label>
                        <input className="settings-input" type="text" name="vendor_name" id="" onChange = {this.vendorId}/>
                    </div>
                    <div className = "settings-form-data">
                        <label className="settings-label">Vendor Type</label>
                        <select className="settings-input settings-dropdown" name="vendor_type"   value = {this.state.new_vendor_information.system_type || ""} onChange = {this.vendorType}>
                            (<option key={"000"} value = ""></option>)
                            {this.state.vendor_type_array.map((system_type,index) =>
                                (<option key={system_type.concat(index)} value = {system_type}>{system_type}</option>)
                             )}
                        </select>
                    </div>
                    <div className="settings-form-buttons">
                        <button className = "settings-button" onClick = {this.onVendorSubmit}>Save</button>
                        <button className = "settings-button" onClick = {(e) => {this.props.information.onExplore("__New")}}>Cancel</button>
                    </div>         
                </div>
            )
        }
        else{

            return ( 
                    <div className = "addVendor-addTage" onClick = {(e) => {this.props.information.onExplore("__New")}}>
                        <span className="addVendor-plus"></span>
                        <span className="settings-button">Add a new vendor</span>
                    </div>       
             )
        }
    }
}

export default AddVendor;