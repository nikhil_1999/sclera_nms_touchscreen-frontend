import React, { Component } from 'react'
import './systemtype.css'
import axios from 'axios';
import { link } from '../../../../../../config';
import { connect } from 'react-redux';
import * as homeActionTypes from '../../../../../../actions/HomeActions';

class SystemTypeList extends Component {

    state = {
        systemtypelist: [],
        selectedSystemType: null
    }

    componentDidMount() {
        axios.get(`${link}/home/systemtypes`)
            .then(res => {
                this.setState({ systemtypelist: JSON.parse(res.data) })
            })
            .catch(err => console.log(err));
    }

    handleClick = (e) => {

        if (e.currentTarget.dataset.system_type === this.props.systemtype) {
            this.props.reset_system_type()
        }
        else {
            this.setState({
                selectedSystemType: e.currentTarget.dataset.system_type
            });
            this.props.set_systemtype(e.currentTarget.dataset.system_type);
            this.props.set_info_tab('account-info');
        }


    }
    convertImageName = (type) => {

        return type.replace(/[\s\/]/g, "_").toLowerCase()
    }

    render() {
        let list = this.state.systemtypelist.map(systemtype => {
            return (
                <div className={this.props.systemtype === systemtype ? 'homecontainer-midgrid homecontainer-grid homecontainer-activegrid' : 'homecontainer-midgrid homecontainer-grid'} key={systemtype} data-system_type={systemtype} onClick={this.handleClick}>
                    <p className="systemtype-name homecontainer-value childcontainer-value">{systemtype}</p>
                    <img className="homecontainer-grid-icon" src={`/img/childcontainer/home/system_type/${this.convertImageName(systemtype)}.png`} title={systemtype} alt="Systemtype" />
                    <div className="homecontainer-status child-status child-offline"></div>
                </div>
            )
        });
        return (
            <div className="systemtypelist-container homecontainer-grid-container">
                {list}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        systemtype: state.home.systemtype
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        set_systemtype: systemtype =>
            dispatch({
                type: homeActionTypes.SELECT_SYSTEMTYPE,
                payload: systemtype
            }),
        set_info_tab: tab =>
            dispatch({
                type: homeActionTypes.SELECTED_INFO_TAB,
                payload: tab
            }),
        reset_system_type: () =>
            dispatch({
                type: homeActionTypes.RESET_SYSTEM_TYPE,

            }),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SystemTypeList);