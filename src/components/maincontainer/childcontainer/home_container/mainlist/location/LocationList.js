import React, { Component } from 'react'
import axios from 'axios';
import { connect } from 'react-redux';
import * as homeActionTypes from '../../../../../../actions/HomeActions';
import { link } from '../../../../../../config';
import './location.css';

class LocationList extends Component {

    state = {
        selectedLocation: null,
        locationlist: []
    }


    componentDidMount() {
        if (this.props.building === null && this.props.floor === null) {
            console.log(`${link}/home/locations`)
            axios.get(`${link}/home/locations`)
                .then(res => this.setState({ locationlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (this.props.building !== null && this.props.floor === null) {
            console.log(`${link}/home/building/${this.props.building}/locations`)
            axios.get(`${link}/home/building/${this.props.building}/locations`)
                .then(res => {
                    console.log(res.data)
                    this.setState({ locationlist: JSON.parse(res.data) })
                })
                .catch(err => console.log(err));
        }
        else if (this.props.building === null && this.props.floor !== null) {
            console.log(`${link}/home/floor/${this.props.floor}/locations`)
            axios.get(`${link}/home/floor/${this.props.floor}/locations`)
                .then(res => this.setState({ locationlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else {
            console.log(`${link}/home/building/${this.props.building}/floor/${this.props.floor}/locations`)
            axios.get(`${link}/home/building/${this.props.building}/floor/${this.props.floor}/locations`)
                .then(res => this.setState({ locationlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            if (nextProps.building === null && nextProps.floor === null) {
                console.log(`${link}/home/locations`)
                axios.get(`${link}/home/locations`)
                    .then(res =>
                        this.setState({ locationlist: JSON.parse(res.data) })
                    )
                    .catch(err => console.log(err));
            }
            else if (nextProps.building !== null && nextProps.floor === null) {
                console.log(`${link}/home/building/${nextProps.building}/locations`)
                axios.get(`${link}/home/building/${nextProps.building}/locations`)
                    .then(res => {
                        console.log(res.data)
                        this.setState({ locationlist: JSON.parse(res.data) })
                    })
                    .catch(err => console.log(err));
            }
            else if (nextProps.building === null && nextProps.floor !== null) {
                console.log(`${link}/home/floor/${nextProps.floor}/locations`)
                axios.get(`${link}/home/floor/${nextProps.floor}/locations`)
                    .then(res => this.setState({ locationlist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            }
            else {
                console.log(`${link}/home/building/${nextProps.building}/floor/${nextProps.floor}/locations`)
                axios.get(`${link}/home/building/${nextProps.building}/floor/${nextProps.floor}/locations`)
                    .then(res => this.setState({ locationlist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            }
        }
    }
    handleClick = (location) => {
        console.log('handleClick', location)
        this.setState({
            selectedLocation: location
        });
        this.props.reset_device()
        this.props.set_location_action('click')
        this.props.set_location(location);
        this.props.set_info_tab('device-list')
    }

    render() {
        let list = this.state.locationlist.map(location => {
            return (
                <div className={this.props.location === location._id ? 'homecontainer-midgrid  homecontainer-grid homecontainer-activegrid' : 'homecontainer-midgrid  homecontainer-grid'} key={location._id} data-location={location._id} onClick={() => this.handleClick(location._id)}>
                    <p className="homecontainer-value schildcontainer-value">{location._id}</p>
                    <img src="/img/childcontainer/home/location/location.png" className="homecontainer-grid-icon" title={location} alt="Location" />
                    <div className={!location.status.includes(0) ? "homecontainer-status childcontainer-status childcontainer-online" : "homecontainer-status childcontainer-status childcontainer-offline"}></div>
                </div>
            )
        })
        return (
            <div className="locationlist-container homecontainer-grid-container">
                {list}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        building: state.home.building,
        floor: state.home.floor,
        location: state.home.location
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        set_location: location =>
            dispatch({
                type: homeActionTypes.SELECT_LOCATION,
                payload: location
            }),
        set_info_tab: tab =>
            dispatch({
                type: homeActionTypes.SELECTED_INFO_TAB,
                payload: tab
            }),
        set_location_action: tab =>
            dispatch({
                type: homeActionTypes.LOCATION_ACTION,
                payload: tab
            }),
        reset_device: () =>
            dispatch({
                type: homeActionTypes.RESET_SELECTED_DEVICE,

            })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationList);