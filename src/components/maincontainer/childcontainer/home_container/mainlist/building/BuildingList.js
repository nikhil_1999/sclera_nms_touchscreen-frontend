import React, { Component } from 'react'
import axios from 'axios';
import { link } from '../../../../../../config';
import { connect } from 'react-redux';
import * as homeActionTypes from '../../../../../../actions/HomeActions';
import './buildinglist.css';

class BuildingList extends Component {

    state = {
        clickedBuilding: null,
        buildingList: []
    }

    componentDidMount() {
        axios.get(`${link}/home/buildings`)

            .then(res => {

                this.setState({ buildingList: JSON.parse(res.data) })
            })
            .catch(err => console.log(err));
    }

    handleClick = (building) => {

        this.setState({
            selectedBuilding: building
        });
        this.props.reset_device();
        this.props.set_building_action('click')
        this.props.set_building(building);
        this.props.set_info_tab('device-list')
    }

    handleSelect = (e) => {
        e.stopPropagation();
        this.props.set_building_action('select')
        this.props.set_building(e.currentTarget.dataset.building);
        this.props.set_tab('floor');
    }


    render() {
        let list = this.state.buildingList.map(building => {
            return (
                <div className={this.props.building === building._id ? 'homecontainer-grid homecontainer-activegrid' : 'homecontainer-grid'} onClick={() => this.handleClick(building._id)} key={building._id}>
                    <div className="homecontainer-midgrid">
                        <p className="homecontainer-value childcontainer-value">{building._id}</p>
                        <img src={`/img/childcontainer/home/building/building.png`} className="homecontainer-grid-icon" title={building._id} alt="Building" />
                        <div className={!building.status.includes(0) ? "homecontainer-status childcontainer-status childcontainer-online" : "homecontainer-status childcontainer-status childcontainer-offline"}></div>
                    </div>
                    <div className="homecontainer-selectgrid" data-building={building._id} onClick={this.handleSelect}>
                        <img src="/img/icons/dropdown.png" className="homecontainer-grid-icon homecontainer-select" alt="Select" />
                    </div>
                </div>
            )
        });
        return (
            <div className="buildinglist-container homecontainer-grid-container">
                {list}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        building: state.home.building
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        set_building: building =>
            dispatch({
                type: homeActionTypes.SELECT_BUILDING,
                payload: building
            }),
        set_tab: tab =>
            dispatch({
                type: homeActionTypes.SELECTED_LIST_TAB,
                payload: tab
            }),
        set_info_tab: tab =>
            dispatch({
                type: homeActionTypes.SELECTED_INFO_TAB,
                payload: tab
            }),
        set_building_action: tab =>
            dispatch({
                type: homeActionTypes.BUILDING_ACTION,
                payload: tab
            }),
        reset_device: () =>
            dispatch({
                type: homeActionTypes.RESET_SELECTED_DEVICE,

            })

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BuildingList);