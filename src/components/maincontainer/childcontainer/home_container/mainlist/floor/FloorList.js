import React, { Component } from 'react'
import axios from 'axios';
import { connect } from 'react-redux';
import * as homeActionTypes from '../../../../../../actions/HomeActions';
import './floor.css';
import { link } from '../../../../../../config';

class FloorList extends Component {

    state = {
        selectedFloor: null,
        floorList: []
    }

    componentDidMount() {
        if (this.props.building === null) {
            axios.get(`${link}/home/floors`)
                .then(res => this.setState({ floorList: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else {
            console.log(`${link}/home/building/${this.props.building}/floors`)
            axios.get(`${link}/home/building/${this.props.building}/floors`)
                .then(res => {
                    console.log(res)
                    this.setState({ floorList: JSON.parse(res.data) }
                    )

                })
                .catch(err => console.log(err));
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            if (nextProps.building === null) {
                axios.get(`${link}/home/floors`)
                    .then(res => this.setState({ floorList: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else {
                axios.get(`${link}/building/${nextProps.building}/floors`)
                    .then(res => this.setState({ floorList: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            }
        }
    }



    handleClick = (floor) => {
        this.setState({
            selectedFloor: floor
        });
        this.props.reset_device()
        this.props.set_floor_action('click')
        this.props.set_floor(floor);
        this.props.set_info_tab('device-list')
    }
    handleSelect = (e) => {
        e.stopPropagation();
        this.props.set_floor_action('select')
        this.props.set_floor(e.currentTarget.dataset.floor);
        this.props.set_tab('location');
    }

    render() {
        let list = this.state.floorList.map(floor => {
            return (
                <div className={this.props.floor === floor._id ? 'homecontainer-grid homecontainer-activegrid' : 'homecontainer-grid'} key={floor._id} onClick={() => this.handleClick(floor._id)}>
                    <div className="homecontainer-midgrid">
                        <p className="homecontainer-value childcontainer-value">{floor._id}</p>
                        <img src="/img/childcontainer/home/floor/floor.png" className="homecontainer-grid-icon" title={floor._id} alt="Floor" />
                        <div className={!floor.status.includes(0) ? "homecontainer-status childcontainer-status childcontainer-online" : "homecontainer-status childcontainer-status childcontainer-offline"}></div>
                    </div>
                    <div className="homecontainer-selectgrid" data-floor={floor._id}  onClick={this.handleSelect}>
                        <img src="/img/icons/dropdown.png" className="homecontainer-grid-icon homecontainer-select"  title={floor._id} alt="Floor" />
                    </div>
                </div>
            )
        })
        return (
            <div className="floorlist-container homecontainer-grid-container">
                {list}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        building: state.home.building,
        floor: state.home.floor,

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        set_floor: floor =>
            dispatch({
                type: homeActionTypes.SELECT_FLOOR,
                payload: floor
            }),
        set_tab: tab =>
            dispatch({
                type: homeActionTypes.SELECTED_LIST_TAB,
                payload: tab
            }),
        set_info_tab: tab =>
            dispatch({
                type: homeActionTypes.SELECTED_INFO_TAB,
                payload: tab
            }),
        set_floor_action: tab =>
            dispatch({
                type: homeActionTypes.FLOOR_ACTION,
                payload: tab
            }),
        reset_device: () =>
            dispatch({
                type: homeActionTypes.RESET_SELECTED_DEVICE,

            })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FloorList);