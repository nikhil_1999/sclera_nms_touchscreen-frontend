import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as homeActionTypes from '../../../../../actions/HomeActions';
import SystemTypeList from './systemtype/SystemTypeList';
import BuildingList from './building/BuildingList';
import FloorList from './floor/FloorList';
import LocationList from './location/LocationList';
import './mainlist.css'



class MainList extends Component {

    handleChangeTabs = (index) => {
        switch (index) {

            case 'systemtype':

                this.props.set_list_tab('systemtype')
                this.props.set_info_tab('account-info')
                this.props.reset_building()
                this.props.reset_floor()
                this.props.reset_location()
                this.props.reset_building_action()
                this.props.reset_floor_action()
                this.props.reset_location_action()
                this.props.reset_selected_device()


                return (<SystemTypeList />)


            case 'building':

                this.props.set_list_tab('building')
                this.props.set_info_tab('device-list')
                this.props.reset_systemtype()
                this.props.reset_building()
                this.props.reset_floor()
                this.props.reset_location()
                this.props.reset_selected_device()

                this.props.reset_floor_action()
                this.props.reset_location_action()

                return (<BuildingList />)


            case 'floor':

                this.props.set_list_tab('floor')
                this.props.set_info_tab('device-list')
                this.props.reset_systemtype()
                this.props.reset_floor()
                this.props.reset_location()
                this.props.reset_selected_device()

                if (this.props.building_action === 'click') {
                    this.props.reset_building()

                }
                return (<FloorList />)


            case 'location':

                this.props.set_list_tab('location')
                this.props.set_info_tab('device-list')
                this.props.reset_systemtype()
                this.props.reset_location()
                this.props.reset_selected_device()

                if (this.props.building_action === 'click') {
                    this.props.reset_building()

                }
                if (this.props.floor_action === 'click') {
                    this.props.reset_floor()

                }

                return (<LocationList />)


            default: return null
        }


    }
    componentWillReceiveProps(nextProps) {
        console.log('nextProps', nextProps)
    }

    displayMainList = () => {

        switch (this.props.selected_list_tab) {

            case 'systemtype':

                return (<SystemTypeList />)

            case 'building':

                return (<BuildingList />)


            case 'floor':
                return (<FloorList />)


            case 'location':
                return (<LocationList />)


            default: return null
        }


    }


    render() {
        return (
            <div className="mainlist-container childcontainer-left">
                <div className="homecontainer-block">
                    <div className="homecontainer-btn-container">
                        <button className={this.props.selected_list_tab === 'systemtype' ? "childcontainer-btn homecontainer-activebtn" : "childcontainer-btn"} onClick={() => this.handleChangeTabs('systemtype')}>System Type</button>
                        <button className={this.props.selected_list_tab === 'building' ? "childcontainer-btn homecontainer-activebtn" : "childcontainer-btn"} onClick={() => this.handleChangeTabs('building')}>{(this.props.building && (this.props.selected_list_tab === 'floor' || this.props.selected_list_tab === 'location')) ? this.props.building : 'Building'}</button>
                        <button className={this.props.selected_list_tab === 'floor' ? "childcontainer-btn homecontainer-activebtn" : "childcontainer-btn"} onClick={() => this.handleChangeTabs('floor')}>{(this.props.floor && this.props.selected_list_tab === 'location') ? this.props.floor : 'Floor'}</button>
                        <button className={this.props.selected_list_tab === 'location' ? "childcontainer-btn homecontainer-activebtn" : "childcontainer-btn"} onClick={() => this.handleChangeTabs('location')}>{this.props.location ? this.props.location : 'Location'}</button>
                    </div>
                    {this.displayMainList()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        selected_list_tab: state.home.selected_list_tab,
        systemtype: state.home.systemtype,
        building: state.home.building,
        floor: state.home.floor,
        location: state.home.location,
        building_action: state.home.building_action,
        floor_action: state.home.floor_action,
        location_action: state.home.location_action,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        set_list_tab: tab =>
            dispatch({
                type: homeActionTypes.SELECTED_LIST_TAB,
                payload: tab
            }),
        set_info_tab: tab =>
            dispatch({
                type: homeActionTypes.SELECTED_INFO_TAB,
                payload: tab
            }),
        set_systemtype: systemtype =>
            dispatch({
                type: homeActionTypes.SELECT_SYSTEMTYPE,
                payload: systemtype
            }),
        reset_systemtype: () =>
            dispatch({
                type: homeActionTypes.RESET_SYSTEM_TYPE
            }),

        set_building: building =>
            dispatch({
                type: homeActionTypes.SELECT_BUILDING,
                payload: building
            }),
        reset_building: () =>
            dispatch({
                type: homeActionTypes.RESET_BUILDING
            }),
        set_floor: floor =>
            dispatch({
                type: homeActionTypes.SELECT_FLOOR,
                payload: floor
            }),
        reset_floor: () =>
            dispatch({
                type: homeActionTypes.RESET_FLOOR
            }),
        set_location: location =>
            dispatch({
                type: homeActionTypes.SELECT_LOCATION,
                payload: location
            }),
        reset_location: () =>
            dispatch({
                type: homeActionTypes.RESET_LOCATION,

            }),
        reset_building_action: () =>
            dispatch({
                type: homeActionTypes.RESET_BUILDING_ACTION
            }),
        reset_floor_action: () =>
            dispatch({
                type: homeActionTypes.RESET_FLOOR_ACTION
            }),
        reset_location_action: () =>
            dispatch({
                type: homeActionTypes.RESET_LOCATION_ACTION
            }),
        reset_selected_device: () =>
            dispatch({
                type: homeActionTypes.RESET_SELECTED_DEVICE
            }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainList);