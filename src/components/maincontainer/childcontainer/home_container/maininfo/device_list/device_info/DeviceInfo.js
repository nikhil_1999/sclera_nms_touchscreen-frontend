import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as homeActionTypes from '../../../../../../../actions/HomeActions';
import axios from 'axios';
import { link } from '../../../../../../../config';
import './deviceinfo.css';

class DeviceInfo extends Component {
    state = {
        "display_name": null,
        "first_seen_on": null,
        "history": null,
        "hw_address": null,
        "ip_addresses": null,
        "last_seen_on": null,
        "location": null,
        "model": null,
        "network_layer": null,
        "notes": null,
        "type": null,
        "user_data_model": null,
        "user_data_name": null,
        "user_data_type": null,
        "user_data_vendor": null,
        "vendor": null,
        "agent_id": null,
        "global_dealer_account_number": null,
        "local_dealer_account_number": null,
        "parent_device_id": null,
        "important": null,
        "monitor": null,
        "status": null,
        "building": null,
        "floor": null,
        "system_type": null,

        local_vendor: [],
        other_vendor: [],
        selected_tab: 'device-info',

        show_local_vendor: false,
        show_other_vendor: false,
        selected_phone_option: null,
        interval: null,
        epoch: null

    }

    epochs = [
        ['year(s)', 31536000],
        ['month(s)', 2592000],
        ['day(s)', 86400],
        ['hour(s)', 3600],
        ['minute(s)', 60],
        ['second(s)', 1]
    ];

    getDuration = (timeAgoInSeconds) => {
        for (let [name, seconds] of this.epochs) {
            const interval = Math.floor((timeAgoInSeconds + 1) / seconds);

            if (interval >= 1) {
                return {
                    interval: interval,
                    epoch: name
                };
            }
        }
    };


    calculateLastSeenTime = (timestamp) => {
        let last_seen_timestamp = parseInt(timestamp, 10);
        const timeAgoInSeconds = Math.floor((new Date() - new Date(last_seen_timestamp)) / 1000);
        const { interval, epoch } = this.getDuration(timeAgoInSeconds);
        console.log('Last Seen On', interval, epoch)
        this.setState({
            interval: interval,
            epoch: epoch
        })
    }

    componentDidMount = () => {
        console.log('componentDidMount')
        if (this.state.option === null) {
            this.setState({ selected_phone_option: 'Extension' })
        }
        else {
            this.setState({ selected_phone_option: 'Option' })
        }

        if(this.props.selected_device!==null){
            if (this.props.selected_device.last_seen_on !== null) {
                let timestamp = parseInt(this.props.selected_device.last_seen_on*1000);
                this.calculateLastSeenTime(timestamp);
            }else{
                this.calculateLastSeenTime(Date.now());
            }
        }


        this.setState({

            display_name: this.props.selected_device.display_name,
            first_seen_on: this.props.selected_device.first_seen_on,
            history: this.props.selected_device.history,
            hw_address: this.props.selected_device.hw_address,
            ip_addresses: this.props.selected_device.ip_addresses,
            last_seen_on: this.props.selected_device.last_seen_on,
            location: this.props.selected_device.location,
            model: this.props.selected_device.model,
            network_layer: this.props.selected_device.network_layer,
            notes: this.props.selected_device.notes,
            type: this.props.selected_device.type,
            user_data_model: this.props.selected_device.user_data_model,
            user_data_name: this.props.selected_device.user_data_name,
            user_data_type: this.props.selected_device.user_data_type,
            user_data_vendor: this.props.selected_device.user_data_vendor,
            vendor: this.props.selected_device.vendor,
            agent_id: this.props.selected_device.agent_id,
            global_dealer_account_number: this.props.selected_device.global_dealer_account_number,
            local_dealer_account_number: this.props.selected_device.local_dealer_account_number,
            parent_device_id: this.props.selected_device.parent_device_id,
            important: this.props.selected_device.important,
            monitor: this.props.selected_device.monitor,
            status: this.props.selected_device.status,
            building: this.props.selected_device.building,
            floor: this.props.selected_device.floor,
            system_type: this.props.selected_device.system_type,
            local_vendor:this.props.selected_device.local_dealer_account_number



        })
        console.log(`${link}/devices/othervendor/${this.props.selected_device.id}`)

        axios.get(`${link}/devices/othervendor/${this.props.selected_device.id}`)
            .then(res => {
                console.log(`${link}/devices/othervendor/${this.props.selected_device.id}`)

                console.log('othervendor', res.data)
                this.setState({
                    other_vendor: res.data
                })
            })
            .catch(err => console.log(err));

        // axios.get(`${link}/devices/vendor/local_dealer_account_number/${this.props.selected_device.id}`)

        //     .then(res => {
        //         console.log(`${link}/devices/vendor/local_dealer_account_number/${this.props.selected_device.id}`)

        //         console.log('localvendor', res.data[0])
        //         this.setState({
        //             local_vendor: res.data[0]
        //         })
        //     })
        //     .catch(err => console.log(err));
    }
    componentWillReceiveProps(nextProps) {
       
        console.log('nextProps', nextProps)
        if (this.state.option === null) {
            this.setState({ selected_phone_option: 'Extension' })
        }
        else {
            this.setState({ selected_phone_option: 'Option' })
        }
        if(nextProps.selected_device !== null){
            if (nextProps.selected_device.last_seen_on !== null) {
                this.calculateLastSeenTime(nextProps.selected_device.last_seen_on * 1000);
            }else{
                this.calculateLastSeenTime(Date.now());
            }
        }
        this.setState({
            display_name: nextProps.selected_device.display_name,
            first_seen_on: nextProps.selected_device.first_seen_on,
            history: nextProps.selected_device.history,
            hw_address: nextProps.selected_device.hw_address,
            ip_addresses: nextProps.selected_device.ip_addresses,
            last_seen_on: nextProps.selected_device.last_seen_on,
            location: nextProps.selected_device.location,
            model: nextProps.selected_device.model,
            network_layer: nextProps.selected_device.network_layer,
            notes: nextProps.selected_device.notes,
            type: nextProps.selected_device.type,
            user_data_model: nextProps.selected_device.user_data_model,
            user_data_name: nextProps.selected_device.user_data_name,
            user_data_type: nextProps.selected_device.user_data_type,
            user_data_vendor: nextProps.selected_device.user_data_vendor,
            vendor: nextProps.selected_device.vendor,
            agent_id: nextProps.selected_device.agent_id,
            global_dealer_account_number: nextProps.selected_device.global_dealer_account_number,
            local_dealer_account_number: nextProps.selected_device.local_dealer_account_number,
            parent_device_id: nextProps.selected_device.parent_device_id,
            important: nextProps.selected_device.important,
            monitor: nextProps.selected_device.monitor,
            status: nextProps.selected_device.status,
            building: nextProps.selected_device.building,
            floor: nextProps.selected_device.floor,
            system_type: nextProps.selected_device.system_type,
            selected_tab: 'device-info',
            local_vendor:nextProps.selected_device.local_dealer_account_number

        })
        axios.get(`${link}/devices/othervendor/${this.props.selected_device.id}`)
            .then(res => {
                console.log('othervendor', res.data)
                this.setState({
                    other_vendor: res.data
                })
            })
            .catch(err => console.log(err));

        // axios.get(`${link}/devices/vendor/local_dealer_account_number/${this.props.selected_device.id}`)
        //     .then(res => {
        //         console.log('localvendor', res.data)
        //         this.setState({
        //             local_vendor: res.data[0]
        //         })
        //     })
        //     .catch(err => console.log(err));

    }

    handleClickBack = () => {
        this.props.reset_device()
    }


    handleShowLocalVendor = (val) => {
        this.setState({
            show_local_vendor: !this.state.show_local_vendor,
            show_other_vendor: false
        })
    }
    handleShowOtherVendor = (val) => {
        this.setState({
            show_local_vendor: false,
            show_other_vendor: !this.state.show_other_vendor,
        })
    }
    addressString = (vendor) => {
       
        let addressString = ''
        if (vendor.address != null)
            addressString = addressString + vendor.address + ' '
        if (vendor.zip != null)
            addressString = addressString + vendor.zip + ' '
        if (vendor.street != null)
            addressString = addressString + vendor.street + ' '
        if (vendor.city != null)
            addressString = addressString + vendor.city + ' '
        if (vendor.state != null)
            addressString = addressString + vendor + ' '
        if (vendor.country != null)
            addressString = addressString + vendor.country + ' '
        return addressString

    }

    renderTabs = () => {

        if (this.state.selected_tab === 'device-info') {
            return (<>
                <div className="maininfo-card-info devicelist-infobody">
                    {this.state.user_data_name !== null ?
                        <>
                            <label className="childcontainer-label">Name</label>
                            <input className="childcontainer-input" type="text" name="user_data_name" value={this.state.user_data_name ? this.state.user_data_name : ''} readOnly />
                        </> :
                        <>
                            <label className="childcontainer-label">Name</label>
                            <input className="childcontainer-input" type="text" name="display_name" value={this.state.display_name ? this.state.display_name : ''} readOnly />
                        </>}
                    <div className="maininfo-block">

                        <div className="maininfo-col2">
                            {this.state.user_data_vendor !== null ?
                                <>
                                    <label className="childcontainer-label">Vendor</label>
                                    <input className="childcontainer-input" type="text" name="user_data_vendor" value={this.state.user_data_vendor ? this.state.user_data_vendor : ''} readOnly />
                                </> :
                                <>
                                    <label className="childcontainer-label">Vendor</label>
                                    <input className="childcontainer-input" type="text" name="vendor" value={this.state.vendor ? this.state.vendor : ''} readOnly />
                                </>}
                        </div>
                        <div className="maininfo-col2">
                            {this.state.user_data_model !== null ?
                                <>
                                    <label className="childcontainer-label">Model</label>
                                    <input className="childcontainer-input" type="text" name="user_data_model" value={this.state.user_data_model ? this.state.user_data_model : ''} readOnly />
                                </> :
                                <>
                                    <label className="childcontainer-label">Model</label>
                                    <input className="childcontainer-input" type="text" name="model" value={this.state.model ? this.state.model : ''} readOnly />
                                </>}
                        </div>
                    </div>
                    {this.state.user_data_type !== null ?
                        <>
                            <label className="childcontainer-label">Type</label>
                            <input className="childcontainer-input" type="text" name="user_data_type" value={this.state.user_data_type ? this.state.user_data_type : ''} readOnly />
                        </> :
                        <>
                            <label className="childcontainer-label">Type</label>
                            <input className="childcontainer-input" type="text" name="type" value={this.state.type ? this.state.type : 'Generic'} readOnly />
                        </>}
                    <div className="maininfo-block">
                        <div className="maininfo-col3">
                            <label className="childcontainer-label maininfo-row">Building</label>
                            <input className="childcontainer-input maininfo-row" type="text" name="building" value={this.state.building ? this.state.building : ''} readOnly />
                        </div>
                        <div className="maininfo-col3">
                            <label className="childcontainer-label maininfo-row">Floor</label>
                            <input className="childcontainer-input maininfo-row" type="text" name="floor" value={this.state.floor ? this.state.floor : ''} readOnly />
                        </div>
                        <div className="maininfo-col3">
                            <label className="childcontainer-label maininfo-row">Location</label>
                            <input className="childcontainer-input maininfo-row" type="text" name="location" value={this.state.location ? this.state.location : ''} readOnly />
                        </div>
                    </div>
                    <label className="childcontainer-label">IP Address</label>
                    <input className="childcontainer-input" type="text" name="ip_addresses" value={this.state.ip_addresses ? this.state.ip_addresses : ''} readOnly />
                    <label className="childcontainer-label">MAC Address</label>
                    <input className="childcontainer-input" type="text" name="hw_address" value={this.state.hw_address ? this.state.hw_address : ''} readOnly />

                    <label className="childcontainer-label">Status</label>
                    <input className="childcontainer-input" type="text" name="status" value={this.state.status === 1 ? 'Online' : 'Offline'} readOnly />


                    <label className="childcontainer-label">Last Seen On</label>
                    <input className="childcontainer-input" type="text" name="last_seen_on" value={`${this.state.interval} ${this.state.epoch} ago`} readOnly />
                </div>
                <div className="childcontainer-btn-container maininfo-btn-container">
                    <button className="childcontainer-btn maininfo-btn" onClick={this.handleClickBack}>Back</button>
                </div>
            </>)
        }
        else if (this.state.selected_tab === 'device-vendor') {
            if (this.state.local_vendor === null && this.state.other_vendor.length === 0) {
                return (
                    <div className="maininfo-card-info devicelist-infobody">
                        <p className='childcontainer-no-data-text'>There are no vendors for this device.</p>
                    </div>
                )
            }
            else {
                return (
                    <>
                        <div className="maininfo-card-info devicelist-infobody">
                            <div className="deviceinfo-vendordata">
                                {this.state.local_vendor !== null ?
                                    <button className="childcontainer-btn deviceinfo-vendor" onClick={this.handleShowLocalVendor}>Local<img src={this.state.show_local_vendor ? "/img/icons/dropdown.png" : "/img/icons/dropdown.png"} className="deviceinfo-dropdown" alt="Click" /></button> : null}
                                {this.state.show_local_vendor ?
                                    <div className="deviceinfo-vendor">
                                        <label className="childcontainer-label">Account Number</label>
                                        <input className="childcontainer-input" type="text" name="account_number" value={this.state.local_vendor.account_number ? this.state.local_vendor.account_number : ''} readOnly />
                                        <label className="childcontainer-label">Dealer Name</label>
                                        <input className="childcontainer-input" type="text" name="dealer_name" value={this.state.local_vendor.dealer_name ? this.state.local_vendor.dealer_name : ''} readOnly />
                                        <label className="childcontainer-label">Email</label>
                                        <input className="childcontainer-input" type="text" name="email" value={this.state.local_vendor.email ? this.state.local_vendor.email : ''} readOnly />
                                        <div className="maininfo-block">
                                            <div className="maininfo-col2">
                                                <label className="childcontainer-label">Phone</label>
                                                <input className="childcontainer-input" type="text" name="phone" value={this.state.local_vendor.phone ? this.state.local_vendor.phone : ''} readOnly />
                                            </div>
                                            <div className="maininfo-col2">
                                                <label className="childcontainer-label">{this.state.local_vendor.extension !== null ? 'Extension' : 'Option'}</label>
                                                <input className="childcontainer-input" name={this.state.local_vendor.extension !== null ? 'Extension' : ('Option')} value={this.state.local_vendor.extension !== null ? this.state.local_vendor.extension : this.state.local_vendor.option} id="account-extension-input" readOnly />
                                            </div>
                                        </div>
                                        <label className="childcontainer-label">Type</label>
                                        <input className="childcontainer-input" type="text" name="type" value={this.state.local_vendor.type ? this.state.local_vendor.type : ''} readOnly />
                                        <label className="childcontainer-label">Address</label>
                                        <input className="childcontainer-input" type="text" name="address" value={this.addressString(this.state.local_vendor)} readOnly />

                                    </div> : null}
                            </div>

                            <div className="deviceinfo-vendordata othervendor">
                                {this.state.other_vendor.length !== 0 ? <button className="childcontainer-btn deviceinfo-vendor othervendor-button" onClick={this.handleShowOtherVendor}>Other<img src={this.state.show_other_vendor ? "/img/icons/dropdown.png" : "/img/icons/dropdown.png"} className="deviceinfo-dropdown" alt="Click" /></button> : null}
                                {this.state.show_other_vendor ? this.state.other_vendor.map(vendor => {
                                    return (
                                        <div className="deviceinfo-vendor othervendor-body" key={vendor.account_number}>
                                            <label className="childcontainer-label">Account Number</label>
                                            <input className="childcontainer-input" type="text" name="account_number" value={vendor.account_number ? vendor.account_number : ''} readOnly />
                                            <label className="childcontainer-label">Dealer Name</label>
                                            <input className="childcontainer-input" type="text" name="dealer_name" value={vendor.dealer_name ? vendor.dealer_name : ''} readOnly />
                                            <label className="childcontainer-label">Email</label>
                                            <input className="childcontainer-input" type="text" name="email" value={vendor.email ? vendor.email : ''} readOnly />
                                            <div className="maininfo-block">
                                                <div className="maininfo-col2">
                                                    <label className="childcontainer-label">Phone</label>
                                                    <input className="childcontainer-input" type="text" name="phone" value={vendor.phone ? vendor.phone : ''} readOnly />
                                                </div>
                                                <div className="maininfo-col2">
                                                    <label className="childcontainer-label">{vendor.extension !== null ? 'Extension' : 'Option'}</label>
                                                    <input className="childcontainer-input" name={vendor.extension !== null ? 'Extension' : ('Option')} value={vendor.extension !== null ? vendor.extension : vendor.option} id="account-extension-input" readOnly />
                                                </div>
                                            </div>
                                            <label className="childcontainer-label">Type</label>
                                            <input className="childcontainer-input" type="text" name="type" value={vendor.type ? vendor.type : ''} readOnly />
                                            <label className="childcontainer-label">Address</label>
                                            <input className="childcontainer-input" type="text" name="address_id" value={this.addressString(vendor)} readOnly />

                                        </div>
                                    )
                                }) : null}
                            </div>
                        </div>
                        <div className="childcontainer-btn-container maininfo-btn-container">
                            <button className="childcontainer-btn maininfo-btn" onClick={this.handleClickBack}>Back</button>
                        </div>
                    </>

                )
            }
        }
        else {
            return (<></>)
        }
    }

    handleClick = (e) => {
        this.setState({
            selected_tab: e.target.id
        });
    }
    render() {
      
        return (<>
            <div className="devicelist-container">
                <div className="devicelist-header childcontainer-btn-container">
                    <p onClick={this.handleClick} id="device-info" className={this.state.selected_tab === 'device-info' ? 'devicelist-heading devicelist-activeheading' : 'devicelist-heading'}>Device Info
                    </p>
                    <p onClick={this.handleClick} id="device-vendor" className={this.state.selected_tab === 'device-vendor' ? 'devicelist-heading devicelist-activeheading' : 'devicelist-heading'}>Device Vendor
                    </p>
                </div>
                <div className="devicelist-body ">
                    {this.renderTabs()}
                </div>
            </div>
        </>)


    }
}



const mapStateToProps = (state) => {
    return {
        selected_device: state.home.selected_device,

    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        reset_device: () =>
            dispatch({
                type: homeActionTypes.RESET_SELECTED_DEVICE,

            })
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(DeviceInfo);