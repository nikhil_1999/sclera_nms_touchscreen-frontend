import React, { Component } from 'react'
import AllDeviceList from './all_device_list/AllDeviceList';
import OnlineDeviceList from './online_device_list/OnlineDeviceList';
import OfflineDeviceList from './offline_device_list/OfflineDeviceList';
import DeviceInfo from './device_info/DeviceInfo';
import { connect } from 'react-redux';
import './deviceListContainer.css'

class DeviceListContainer extends Component {

    state = {
        selected_tab: 'all'
    }

    handleClick = (e) => {
        this.setState({
            selected_tab: e.target.id
        });
    }

    renderList = () => {
        if (this.state.selected_tab === 'all') {
            return (<AllDeviceList />)
        } else if (this.state.selected_tab === 'online') {
            return (<OnlineDeviceList />)
        } else {
            return (<OfflineDeviceList />)
        }
    }

    render() {
        console.log('DeviceListContainer', this.props)
        return (
            <>
                {
                    this.props.selected_device !== null ? (
                        <DeviceInfo />
                    ) : (
                            <div className="devicelist-container">
                                <div className="devicelist-header childcontainer-btn-container">
                                    <p onClick={this.handleClick} id="all" className={this.state.selected_tab === 'all' ? "devicelist-heading devicelist-activeheading" : "devicelist-heading"}>All</p>
                                    <p onClick={this.handleClick} id="online" className={this.state.selected_tab === 'online' ? "devicelist-heading devicelist-activeheading" : "devicelist-heading"}>Online
                                    </p>
                                    <p onClick={this.handleClick} id="offline" className={this.state.selected_tab === 'offline' ? "devicelist-heading devicelist-activeheading" : "devicelist-heading"}>Offline
                                    </p>
                                </div>
                                <div className="devicelist-body">
                                    <div className=" maininfo-card-info devicelist-infobody">
                                        {this.renderList()}
                                    </div>
                                </div>
                            </div>
                        )
                }
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selected_device: state.home.selected_device
    }
}

export default connect(mapStateToProps, null)(DeviceListContainer);