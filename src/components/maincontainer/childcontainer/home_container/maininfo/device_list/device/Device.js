import React, { Component } from 'react'
import {connect} from 'react-redux';
import * as homeActionTypes from '../../../../../../../actions/HomeActions';

class Device extends Component {

    state = {
        id:null,
        display_name:null,
        history:null,
        hw_address:null,
        ip_addresses:null,
        location:null,
        model:null,
        network_layer:null,
        notes:null,
        type:null,
        vendor:null,
        parent_device_id:null,
        important:false,
        monitor:false,
        status:false,
        building:null,
        floor:null
    }

    componentDidMount(){
        let display_name,model,vendor,type,important,monitor,status;
        if(this.props.data.user_data_name === null || this.props.data.user_data_name === "")display_name = this.props.data.display_name; else display_name = this.props.data.user_data_name;
        if(this.props.data.user_data_model === null || this.props.data.user_data_model === "") model = this.props.data.model; else model = this.props.data.user_data_model;
        if(this.props.data.user_data_vendor === null || this.props.data.user_data_vendor === "") vendor = this.props.data.vendor; else vendor = this.props.user_data_vendor;
        if(this.props.data.important === 1) important = true; else important = false;
        if(this.props.data.monitor === 1) monitor = true; else monitor = false;
        if(this.props.data.status === 1) status = true; else status = false;
        this.setState({
            id:this.props.data.id,
            display_name:display_name,
            history:this.props.data.history,
            hw_address:this.props.data.hw_address,
            ip_addresses:this.props.data.ip_addresses,
            location:this.props.data.location,
            model:model,
            network_layer:this.props.data.network_layer,
            notes:this.props.data.notes,
            type:type,
            vendor:vendor,
            parent_device_id:this.props.data.parent_device_id,
            important:important,
            monitor:monitor,
            status:status,
            building:this.props.data.building,
            floor:this.props.data.floor
        })
    }

    componentWillReceiveProps(nextProps){
        if(this.props!==nextProps){
            let display_name,model,vendor,type,important,monitor,status;
            if(nextProps.data.user_data_name === null || nextProps.data.user_data_name === "")display_name = nextProps.data.display_name; else display_name = nextProps.data.user_data_name;
            if(nextProps.data.user_data_model === null || nextProps.data.user_data_model === "") model = nextProps.data.model; else model = nextProps.data.user_data_model;
            if(nextProps.data.user_data_vendor === null || nextProps.data.user_data_vendor === "") vendor = nextProps.data.vendor; else vendor = nextProps.user_data_vendor;
            if(nextProps.data.important === 1) important = true; else important = false;
            if(nextProps.data.monitor === 1) monitor = true; else monitor = false;
            if(nextProps.data.status === 1) status = true; else status = false;
            this.setState({
                id:nextProps.data.id,
                display_name:display_name,
                history:nextProps.data.history,
                hw_address:nextProps.data.hw_address,
                ip_addresses:nextProps.data.ip_addresses,
                location:nextProps.data.location,
                model:model,
                network_layer:nextProps.data.network_layer,
                notes:nextProps.data.notes,
                type:type,
                vendor:vendor,
                parent_device_id:nextProps.data.parent_device_id,
                important:important,
                monitor:monitor,
                status:status,
                building:nextProps.data.building,
                floor:nextProps.data.floor
            })
            }
    }

    handleClick = (e)=>{
        this.props.set_device(this.state);
    }

    render() {
        return (
            <div className="device-container" onClick={this.handleClick}>
                <p className="device-name">{this.state.display_name}</p>
                <p className="device-location">Room 21</p>
                <div className="device-status">{this.state.status}</div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        set_device:device=>
        dispatch({
            type:homeActionTypes.SELECTED_DEVICE,
            payload:device
        })
    }
}

export default connect(null,mapDispatchToProps)(Device);