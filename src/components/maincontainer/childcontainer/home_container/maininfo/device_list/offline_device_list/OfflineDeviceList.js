import React, { Component } from 'react'
import axios from 'axios';
import * as homeActionTypes from '../../../../../../../actions/HomeActions';
import { link } from '../../../../../../../config'
import { connect } from 'react-redux';

class OfflineDeviceList extends Component {

    state = {
        devicelist: []
    }

    componentDidMount() {
        if (this.props.systemtype !== null && this.props.building === null && this.props.floor === null && this.props.location === null) {
            axios.get(`${link}/devices/systemtype/${this.props.systemtype}/offline`)
                .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                .catch(err => {
                    console.log(err)
                });
        } else if (this.props.systemtype === null && this.props.building !== null && this.props.floor === null && this.props.location === null) {
            axios.get(`${link}/devices/building/${this.props.building}/offline`)
                .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building === null && this.props.floor !== null && this.props.location === null) {
            axios.get(`${link}/devices/floor/${this.props.floor}/offline`)
                .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building === null && this.props.floor === null && this.props.location !== null) {
            axios.get(`${link}/devices/location/${this.props.location}/offline`)
                .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building !== null && this.props.floor !== null && this.props.location === null) {
            axios.get(`${link}/devices/building/${this.props.building}/floor/${this.props.floor}/offline`)
                .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building !== null && this.props.floor === null && this.props.location !== null) {
            axios.get(`${link}/devices/building/${this.props.building}/location/${this.props.location}/offline`)
                .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building === null && this.props.floor !== null && this.props.location !== null) {
            axios.get(`${link}/devices/floor/${this.props.floor}/location/${this.props.location}/offline`)
                .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (this.props.systemtype === null && this.props.building !== null && this.props.floor !== null && this.props.location !== null) {
            console.log(`${link}/devices/building/${this.props.building}/floor/${this.props.floor}/location/${this.props.location}`)
            axios.get(`${link}/devices/building/${this.props.building}/floor/${this.props.floor}/location/${this.props.location}/offline`)
                .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else {
            return null;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            if (nextProps.systemtype !== null && nextProps.building === null && nextProps.floor === null && nextProps.location === null) {
                axios.get(`${link}/devices/systemtype/offline`)
                    .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building !== null && nextProps.floor === null && nextProps.location === null) {
                axios.get(`${link}/devices/building/${nextProps.building}/offline`)
                    .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building === null && nextProps.floor !== null && nextProps.location === null) {
                axios.get(`${link}/devices/floor/${nextProps.floor}/offline`)
                    .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building === null && nextProps.floor === null && nextProps.location !== null) {
                axios.get(`${link}/devices/location/${nextProps.location}/offline`)
                    .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building !== null && nextProps.floor !== null && nextProps.location === null) {
                axios.get(`${link}/devices/building/${nextProps.building}/floor/${nextProps.floor}/offline`)
                    .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building !== null && nextProps.floor === null && nextProps.location !== null) {
                axios.get(`${link}/devices/building/${nextProps.building}/location/${nextProps.location}/offline`)
                    .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building === null && nextProps.floor !== null && nextProps.location !== null) {
                axios.get(`${link}/devices/floor/${nextProps.floor}/location/${nextProps.location}/offline`)
                    .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            }
            else if (nextProps.systemtype === null && nextProps.building !== null && nextProps.floor !== null && nextProps.location !== null) {
                console.log(`${link}/devices/building/${nextProps.building}/floor/${nextProps.floor}/location/${nextProps.location}`)
                axios.get(`${link}/devices/building/${nextProps.building}/floor/${nextProps.floor}/location/${nextProps.location}/offline`)
                    .then(res => this.setState({ devicelist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else {
                return null;
            }
        }
    }

    handleClick = (device) => {
        this.props.set_device(device);
    }

    render() {

        let list = this.state.devicelist.map(device => {
            return (
                <div className="device-container devicelist-card" key={device.id} onClick={() => this.handleClick(device)}>
                    <p className="devicelist-name childcontainer-value">{device.user_data_name !== null ? device.user_data_name : (device.display_name !== null ? device.display_name : device.hw_address)}</p>
                    <p className="devicelist-location childcontainer-value">{device.location}</p>
                    <div className="devicelist-status childcontainer-status childcontainer-offline"></div>
                </div>
            )
        })
        return (
            <>
                {list}
            </>
        )
    }


}

const mapStateToProps = (state) => {
    return {
        systemtype: state.home.systemtype,
        building: state.home.building,
        floor: state.home.floor,
        location: state.home.location,
        selected_device: state.home.selected_device
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        set_device: device =>
            dispatch({
                type: homeActionTypes.SELECTED_DEVICE,
                payload: device
            })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OfflineDeviceList);