import React, { Component } from 'react'
import './notification.css';
import axios from 'axios';
import * as homeActionTypes from '../../../../../../actions/HomeActions';
import { link,AlarmTypes } from '../../../../../../config'
import { connect } from 'react-redux';
import DeviceInfo from '../../maininfo/device_list/device_info/DeviceInfo';

const epochs = [
    ['year(s)', 31536000],
    ['month(s)', 2592000],
    ['day(s)', 86400],
    ['hour(s)', 3600],
    ['minute(s)', 60],
    ['second(s)', 1]
];

class Notification extends Component {
    state = {
        notificationlist: [],
        interval:null,
        epoch:null
    }

    componentDidMount() {
        if (this.props.systemtype !== null && this.props.building === null && this.props.floor === null && this.props.location === null) {
            console.log(`${link}/notification/systemtype/${this.props.systemtype}`)
            axios.get(`${link}/notification/systemtype/${this.props.systemtype}`)
                .then(res => {
                    console.log(res.data);
                    this.setState({ notificationlist: JSON.parse(res.data) })
                })
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building !== null && this.props.floor === null && this.props.location === null) {
            console.log(`${link}/notification/building/${this.props.building}`)
            axios.get(`${link}/notification/building/${this.props.building}`)
                .then(res => {
                    console.log(res.data);
                    this.setState({ notificationlist: JSON.parse(res.data) })
                })
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building === null && this.props.floor !== null && this.props.location === null) {
            console.log(`${link}/notification/floor/${this.props.floor}`)
            axios.get(`${link}/notification/floor/${this.props.floor}`)
                .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building === null && this.props.floor === null && this.props.location !== null) {
            console.log(`${link}/notification/location/${this.props.location}`)
            axios.get(`${link}/notification/location/${this.props.location}`)
                .then(res => {
                    this.setState({ notificationlist: JSON.parse(res.data) })
                })
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building !== null && this.props.floor !== null && this.props.location === null) {
            console.log(`${link}/notification/building/${this.props.building}/floor/${this.props.floor}`)
            axios.get(`${link}/notification/building/${this.props.building}/floor/${this.props.floor}`)
                .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building !== null && this.props.floor === null && this.props.location !== null) {
            console.log(`${link}/notification/building/${this.props.building}/location/${this.props.location}`)
            axios.get(`${link}/notification/building/${this.props.building}/location/${this.props.location}`)
                .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else if (this.props.systemtype === null && this.props.building === null && this.props.floor !== null && this.props.location !== null) {
            console.log(`${link}/notification/floor/${this.props.floor}/location/${this.props.location}`)
            axios.get(`${link}/notification/floor/${this.props.floor}/location/${this.props.location}`)
                .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (this.props.systemtype === null && this.props.building !== null && this.props.floor !== null && this.props.location !== null) {
            console.log(`${link}/notification/building/${this.props.building}/floor/${this.props.floor}/location/${this.props.location}`)
            axios.get(`${link}/notification/building/${this.props.building}/floor/${this.props.floor}/location/${this.props.location}`)
                .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        } else {
            return null;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            if (nextProps.systemtype !== null && nextProps.building === null && nextProps.floor === null && nextProps.location === null) {
                axios.get(`${link}/notification/systemtype/${nextProps.systemtype}`)
                    .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building !== null && nextProps.floor === null && nextProps.location === null) {
                axios.get(`${link}/notification/building/${nextProps.building}`)
                    .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building === null && nextProps.floor !== null && nextProps.location === null) {
                axios.get(`${link}/notification/floor/${nextProps.floor}`)
                    .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building === null && nextProps.floor === null && nextProps.location !== null) {
                axios.get(`${link}/notification/location/${nextProps.location}`)
                    .then(res => {
                        this.setState({ notificationlist: JSON.parse(res.data) })
                    })
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building !== null && nextProps.floor !== null && nextProps.location === null) {
                axios.get(`${link}/notification/building/${nextProps.building}/floor/${nextProps.floor}`)
                    .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building !== null && nextProps.floor === null && nextProps.location !== null) {
                axios.get(`${link}/notification/building/${nextProps.building}/location/${nextProps.location}`)
                    .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else if (nextProps.systemtype === null && nextProps.building === null && nextProps.floor !== null && nextProps.location !== null) {
                axios.get(`${link}/notification/floor/${nextProps.floor}/location/${nextProps.location}`)
                    .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            }
            else if (nextProps.systemtype === null && nextProps.building !== null && nextProps.floor !== null && nextProps.location !== null) {
                console.log(`${link}/notification/building/${nextProps.building}/floor/${nextProps.floor}/location/${nextProps.location}`)
                axios.get(`${link}/notification/building/${nextProps.building}/floor/${nextProps.floor}/location/${nextProps.location}`)
                    .then(res => this.setState({ notificationlist: JSON.parse(res.data) }))
                    .catch(err => console.log(err));
            } else {
                return null;
            }
        }
    }

    getDuration = (timeAgoInSeconds) => {
        for (let [name, seconds] of epochs) {
            const interval = Math.floor((timeAgoInSeconds + 1) / seconds);

            if (interval >= 1) {
                return {
                    interval: interval,
                    epoch: name
                };
            }
        }
    };


    calculateLastSeenTime = (timestamp) => {
        let last_seen_timestamp = parseInt(timestamp, 10);
        const timeAgoInSeconds = Math.floor((new Date() - new Date(last_seen_timestamp)) / 1000);
        const { interval, epoch } = this.getDuration(timeAgoInSeconds);
        console.log('Last Seen On', interval, epoch)
        return `${interval} ${epoch} ago`
    }

    showDeviceInfo = () => {
        return (
            <DeviceInfo />
        )
    }

    handleClick = (device) => {
        this.props.set_device(device);
    }

    render() {

        let list = this.state.notificationlist.map(device => {
            return (
                <div className="device-container devicelist-card device-notification-tabs" key={device._id} onClick={() => this.handleClick(device)}>
                    <div className="device-notification-details">
                        <p className="childcontainer-value devicelist-name device-notification-text">{device.user_data_name !== null ? device.user_data_name : (device.display_name !== null ? device.display_name : device.hw_address)}</p>
                        <p className="childcontainer-value devicelist-location device-notification-text device-notification-highlight">{`${device.location ? device.location : null}`}</p>
                        {/* <span className="childcontainer-value device-notification-text">{device.user_data_name !== null ? device.user_data_name : (device.display_name !== null ? device.display_name : device.hw_address)}</span>
                        <span className="childcontainer-value device-notification-text device-notification-highlight">{` in ${device.location ? device.location : null}`}</span> */}
                        {/* <span className="childcontainer-value device-notification-text">{device.alarm ? AlarmTypes[`${device.alarm}`] : null}</span> */}
                    </div>
                    <div className="device-notification-status">
                    <p className="childcontainer-value device-notification-statustext">{device.last_seen_on !== null?(this.calculateLastSeenTime(device.last_seen_on*1000)):(this.calculateLastSeenTime(Date.now()))}</p>
                        <div className={device.status === 0 ? "devicelist-status childcontainer-status childcontainer-offline" : "devicelist-status childcontainer-status childcontainer-online"}></div>
                    </div>
                </div>

            )
        })
        return (
            <>
                {this.props.selected_device !== null ? this.showDeviceInfo() :
                    <div className="maininfo-card-info">
                        {list}
                    </div>
                }
            </>
        )


    }
}

const mapStateToProps = (state) => {
    return {
        systemtype: state.home.systemtype,
        building: state.home.building,
        floor: state.home.floor,
        location: state.home.location,
        selected_device: state.home.selected_device
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        set_device: device =>
            dispatch({
                type: homeActionTypes.SELECTED_DEVICE,
                payload: device
            })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Notification);

