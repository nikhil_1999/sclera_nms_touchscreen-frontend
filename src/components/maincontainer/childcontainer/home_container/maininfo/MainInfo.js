import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as homeActionType from '../../../../../actions/HomeActions';
import { virtualize } from 'react-swipeable-views-utils';
import './maininfo.css';
import DeviceListContainer from './device_list/DeviceListContainer';
import AccountInfo from './account_info/AccountInfo';
import SensorList from './sensor_list/SensorList';
import Notification from './notification/Notification';


class MainInfo extends Component {

    state = {
        selected_tab: null,
    }

    handleChangeTabs = (tab) => {
        this.props.reset_device()

        if (this.props.selected_list_tab == 'systemtype' && this.props.systemtype !== null) {
            switch (tab) {

                case 'account-info':
                    this.props.set_info_tab('account-info')
                    break;

                case 'device-list':
                    this.props.set_info_tab('device-list')
                    break;

                case 'sensor-list':
                    this.props.set_info_tab('sensor-list')
                    break;

                case 'notification':
                    this.props.set_info_tab('notification')
                    break;

                default: return null;
            }



        } else {
            if (this.props.building !== null || this.props.floor !== null || this.props.location !== null) {

                switch (tab) {

                    case 'device-list':
                        this.props.set_info_tab('device-list')
                        break;

                    case 'sensor-list':
                        this.props.set_info_tab('sensor-list')
                        break;

                    case 'notification':
                        this.props.set_info_tab('notification')
                        break;

                    default: return null;

                }
            }
        }
    }

    displayMainInfo = () => {

        switch (this.props.selected_info_tab) {

            case 'account-info':
                return (
                    <div className="maininfo-card childcontainer-card">
                        <div className="childcontainer-card-details">
                            <AccountInfo />
                        </div>
                    </div>
                );

            case 'device-list':

                return (
                    <div className="maininfo-card childcontainer-card">
                        <div className="childcontainer-card-details">
                            <DeviceListContainer />
                        </div>
                    </div>
                );

            case 'sensor-list':


                return (
                    <div className="maininfo-card childcontainer-card">
                        <div className="childcontainer-card-details">
                            <SensorList />
                        </div>
                    </div>)


            case 'notification':
                return (
                    <div className="maininfo-card childcontainer-card">
                        <div className="childcontainer-card-details">
                            <Notification />
                        </div>
                    </div>
                );

            default:
                return null;
        }

    }
    displayMainInfoTabs = () => {
        if (this.props.selected_list_tab == 'systemtype' && this.props.systemtype !== null) {
            return (<>
                <div className="homecontainer-sidebtn-container">
                    <button className={this.props.selected_info_tab === 'account-info' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('account-info')}>Acc Info</button>
                    <button className={this.props.selected_info_tab === 'device-list' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('device-list')}>Device List</button>
                    <button className={this.props.selected_info_tab === 'sensor-list' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('sensor-list')}>Sensor List</button>
                    <button className={this.props.selected_info_tab === 'notification' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('notification')}>Notification</button>
                </div>
                {this.displayMainInfo(this.props.selected_info_tab)}
            </>)

        }
        else if (this.props.selected_list_tab == 'building' && this.props.building !== null) {
            return (<>
                <div className="homecontainer-sidebtn-container">
                    <button className={this.props.selected_info_tab === 'device-list' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('device-list')}>Device List</button>
                    <button className={this.props.selected_info_tab === 'sensor-list' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('sensor-list')}>Sensor List</button>
                    <button className={this.props.selected_info_tab === 'notification' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('notification')}>Notification</button>
                </div>
                {this.displayMainInfo(this.props.selected_info_tab)}
            </>)

        }
        else if (this.props.selected_list_tab == 'floor' && this.props.floor !== null) {
            return (<>
                <div className="homecontainer-sidebtn-container">
                    <button className={this.props.selected_info_tab === 'device-list' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('device-list')}>Device List</button>
                    <button className={this.props.selected_info_tab === 'sensor-list' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('sensor-list')}>Sensor List</button>
                    <button className={this.props.selected_info_tab === 'notification' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('notification')}>Notification</button>
                </div>
                {this.displayMainInfo(this.props.selected_info_tab)}
            </>)

        }
        else if (this.props.selected_list_tab == 'location' && this.props.location !== null) {

            return (<>
                <div className="homecontainer-sidebtn-container">
                    <button className={this.props.selected_info_tab === 'device-list' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('device-list')}>Device List</button>
                    <button className={this.props.selected_info_tab === 'sensor-list' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('sensor-list')}>Sensor List</button>
                    <button className={this.props.selected_info_tab === 'notification' ? 'childcontainer-sidebtn homecontainer-activesidebtn' : 'childcontainer-sidebtn'} onClick={() => this.handleChangeTabs('notification')}>Notification</button>
                </div>
                {this.displayMainInfo(this.props.selected_info_tab)}
            </>)

        }
        else {

        }
    }

    render() {
        console.log('MainInfo', this.props.location)
        return (
            <div className="maininfo-container childcontainer-right">
                <div className="homecontainer-block">
                    {this.displayMainInfoTabs()}
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        selected_list_tab: state.home.selected_list_tab,
        selected_info_tab: state.home.selected_info_tab,
        systemtype: state.home.systemtype,
        building: state.home.building,
        floor: state.home.floor,
        location: state.home.location
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        set_info_tab: tab =>
            dispatch({
                type: homeActionType.SELECTED_INFO_TAB,
                payload: tab
            }),
        reset_device: () =>
            dispatch({
                type: homeActionType.RESET_SELECTED_DEVICE,

            })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainInfo);