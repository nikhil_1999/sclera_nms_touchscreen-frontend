import React, { Component } from 'react';


class SensorInfo extends Component {
    state = {
        sensor_name: null,
        sensor_des: null,
        sensor_uplimit: null,
        sensor_lowlimit: null,
        sensor_value: null,
        sensor_status: null,
        device_id: null,
        sensor_location: null,
        sensor_unit: null
    }

    componentDidMount = () => {
        let sensor_name, sensor_des;
        sensor_name = this.props.sensor.user_data_sensor_name === null ? (sensor_name = this.props.sensor.sensor_name) : (sensor_name = this.props.sensor.user_data_sensor_name);
        sensor_des = this.props.sensor.user_data_sensor_des === null ? (sensor_des = this.props.sensor.sensor_des) : (sensor_des = this.props.sensor.user_data_sensor_des);
        this.setState({
            sensor_name: sensor_name,
            sensor_des: sensor_des,
            sensor_uplimit: this.props.sensor.sensor_uplimit,
            sensor_lowlimit: this.props.sensor.sensor_lowlimit,
            sensor_value: this.props.sensor.sensor_value,
            sensor_status: this.props.sensor.sensor_status,
            device_id: this.props.sensor.device_id,
            sensor_location: this.props.sensor.sensor_location,
            sensor_unit: this.props.sensor.sensor_unit
        })
    }
    componentWillReceiveProps(nextProps) {

        this.setState({
            sensor_name: nextProps.sensor_name,
            sensor_des: nextProps.des,
            sensor_uplimit: nextProps.sensor_uplimit,
            sensor_lowlimit: nextProps.sensor_lowlimit,
            sensor_value: nextProps.sensor_value,
            sensor_status: nextProps.sensor_status,
            device_id: nextProps.device_id,
            sensor_location: nextProps.sensor.sensor_location,
            sensor_unit: nextProps.sensor.sensor_unit
        })

    }

    render() {
        return (
            <>
                <div className="maininfo-card-info">
                    <label className="childcontainer-label">Sensor Name</label>
                    <input className="childcontainer-input" type="text" name="sensor_name" value={this.state.sensor_name ? this.state.sensor_name : ''} readOnly />
                    <label className="childcontainer-label">Sensor Description</label>
                    <input className="childcontainer-input" type="text" name="sensor_des" value={this.state.sensor_des ? this.state.sensor_des : ''} readOnly />
                    <label className="childcontainer-label">Value</label>
                    <input className="childcontainer-input" type="text" name="sensor_value" value={this.state.sensor_value + ' ' + this.state.sensor_unit} readOnly />
                    {/* <label className="childcontainer-label">Sensor Uplimit</label>
                    <input className="childcontainer-input" type="text" name="sensor_uplimit" value={this.state.sensor_uplimit ? this.state.sensor_uplimit : ''} readOnly />
                    <label className="childcontainer-label">Sensor LowLimit</label>
                    <input className="childcontainer-input" type="text" name="sensor_lowlimit" value={this.state.sensor_lowlimit ? this.state.sensor_lowlimit : ''} readOnly /> */}
                    <label className="childcontainer-label">Location</label>
                    <input className="childcontainer-input" type="text" name="sensor_location" value={this.state.sensor_location ? this.state.sensor_location : ''} readOnly />
                    <label className="childcontainer-label">Parent Device</label>
                    <input className="childcontainer-input" type="text" name="device_id" value={this.state.device_id ? this.state.device_id : ''} readOnly />
                    <label className="childcontainer-label">Sensor Status</label>
                    <input className="childcontainer-input" type="text" name="sensor_status" value={this.state.sensor_status ? this.state.sensor_status : ''} readOnly />
                </div>
                <div className="maininfo-btn-container">
                    <button className="childcontainer-btn maininfo-btn" onClick={this.props.handleClickBack}> Back</button>
                </div>
            </>
        );
    }
}

export default SensorInfo;