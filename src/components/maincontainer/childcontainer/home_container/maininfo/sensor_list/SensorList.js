import React, { Component } from 'react'
import './sensorlist.css';
import axios from 'axios';
import { link } from '../../../../../../config';
import { connect } from 'react-redux';
import SensorInfo from './sensor_info/SensorInfo'

class SensorList extends Component {
    state = {
        sensorlist: [],
        showsensorinfo: false,
        selected_sensor: null
    }

    componentDidMount() {
        //sensors of a systemtype
        if (this.props.building === null && this.props.floor === null && this.props.location === null) {
            axios.get(`${link}/sensors/${this.props.systemtype}`)
                .then(res => {
                    this.setState({ sensorlist: JSON.parse(res.data) })
                })
                .catch(err => console.log(err));
        }
        //sensors for devices in a building
        else if (this.props.building !== null && this.props.floor === null && this.props.location === null) {

            axios.get(`${link}/sensors/building/${this.props.building}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        //sensors for devices in a floor
        else if (this.props.building === null && this.props.floor !== null && this.props.location === null) {
            axios.get(`${link}/sensors/floor/${this.props.floor}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        //sensor for devices in a location
        else if (this.props.building === null && this.props.floor === null && this.props.location !== null) {
            axios.get(`${link}/sensors/location/${this.props.location}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        //building and floor
        else if (this.props.building !== null && this.props.floor !== null && this.props.location === null) {
            axios.get(`${link}/sensors/building/${this.props.building}/floor/${this.props.floor}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        //building and location
        else if (this.props.building !== null && this.props.floor === null && this.props.location !== null) {
            axios.get(`${link}/sensors/building/${this.props.building}/location/${this.props.location}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        //floor and location 
        else if (this.props.building === null && this.props.floor !== null && this.props.location !== null) {
            axios.get(`${link}/sensors/floor/${this.props.floor}/location/${this.props.location}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        //building and floor and location 
        else if (this.props.building !== null && this.props.floor !== null && this.props.location !== null) {
            axios.get(`${link}/sensors/building/${this.props.building}/floor/${this.props.floor}/location/${this.props.location}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else {
            return null;
        }
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.building === null && nextProps.floor === null && nextProps.location === null) {
            axios.get(`${link}/sensors/bacnet`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (nextProps.building !== null && nextProps.floor === null && nextProps.location === null) {

            axios.get(`${link}/sensors/building/${this.props.building}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (nextProps.building === null && nextProps.floor !== null && nextProps.location === null) {
            axios.get(`${link}/sensors/floor/${nextProps.floor}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (nextProps.building === null && nextProps.floor === null && nextProps.location !== null) {
            axios.get(`${link}/sensors/location/${nextProps.location}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (nextProps.building !== null && nextProps.floor !== null && nextProps.location === null) {
            axios.get(`${link}/sensors/building/${nextProps.building}/floor/${nextProps.floor}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (nextProps.building !== null && nextProps.floor === null && nextProps.location !== null) {
            axios.get(`${link}/sensors/building/${nextProps.building}/location/${nextProps.location}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (nextProps.building === null && nextProps.floor !== null && nextProps.location !== null) {
            axios.get(`${link}/sensors/floor/${nextProps.floor}/location/${nextProps.location}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else if (nextProps.building !== null && nextProps.floor !== null && nextProps.location !== null) {
            axios.get(`${link}/sensors/building/${nextProps.building}/floor/${nextProps.floor}/location/${nextProps.location}`)
                .then(res => this.setState({ sensorlist: JSON.parse(res.data) }))
                .catch(err => console.log(err));
        }
        else {
            return null;
        }
    }


    handleClickSensor = (sensor) => {

        this.setState({
            showsensorinfo: true,
            selected_sensor: sensor
        })

    }
    handleClickBack = () => {
        this.setState({
            showsensorinfo: false
        })
    }

    render() {
        console.log(this.state.sensorlist);
        let list = this.state.sensorlist.map(sensor => {
            return (
                <div key={sensor._id} className="device-container devicelist-card" onClick={() => this.handleClickSensor(sensor)}>
                    <p className="devicelist-name childcontainer-value" >{sensor.sensor_name}</p>
                    <p className="devicelist-location childcontainer-value" >{sensor.sensor_value} {sensor.sensor_unit}</p>
                </div>
            )
        });
        return (
            <>
                {this.state.showsensorinfo ? 
                    <SensorInfo sensor={this.state.selected_sensor} handleClickBack={this.handleClickBack} />
                 : 
                 <div className="maininfo-card-info">
                     {list}
                 </div>}
                 
            </>
        )

    }
}

const mapStateToProps = (state) => {
    return {
        systemtype: state.home.systemtype,
        building: state.home.building,
        floor: state.home.floor,
        location: state.home.location,
        selected_device: state.home.selected_device
    }
}
export default connect(mapStateToProps, null)(SensorList);


















