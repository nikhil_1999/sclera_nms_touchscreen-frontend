import React, { Component } from 'react'
import './accountinfo.css';
import axios from 'axios';
import { link } from '../../../../../../config';
import { connect } from 'react-redux';
class AccountInfo extends Component {

    state = {

        username: null,
        extension: null,
        option: null,
        name: null,
        email: null,
        phone: null,
        company_name: null,
        website: null,
        address: null,
        street: null,
        state: null,
        zip: null,
        country: null,
        selected_phone_option: null

    }
    componentDidMount = () => {
        if (this.state.option === null) {
            this.setState({ selected_phone_option: 'Extension' })
        }
        else {
            this.setState({ selected_phone_option: 'Option' })
        }
        axios.get(`${link}/mastervendor/systemtype/${this.props.systemtype}/mastervendor`)
            .then(res => {
                let data = { ...JSON.parse(res.data) }
                this.setState({
                    username: data.username,
                    extension: data.extension,
                    option: data.option,
                    name: data.name,
                    email: data.email,
                    phone: data.phone,
                    company_name: data.company_name,
                    website: data.website,
                    address: data.address,
                    street: res.data.street,
                    city: res.data.city,
                    state: res.data.state,
                    zip: res.data.zip,
                    country: res.data.country

                })
            })
            .catch(err => console.log(err));
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.option === null) {
            this.setState({ selected_phone_option: 'Extension' })
        }
        else {
            this.setState({ selected_phone_option: 'Option' })
        }
        axios.get(`${link}/mastervendor/systemtype/${nextProps.systemtype}/mastervendor`)
            .then(res => {

                let data = { ...JSON.parse(res.data) }
                this.setState({
                    username: data.username,
                    extension: data.extension,
                    option: data.option,
                    name: data.name,
                    email: data.email,
                    phone: data.phone,
                    company_name: data.company_name,
                    website: data.website,
                    address: res.data.address,
                    street: res.data.street,
                    city: res.data.city,
                    state: res.data.state,
                    zip: res.data.zip,
                    country: res.data.country

                })
            })
            .catch(err => console.log(err));

    }

    addressString = () => {
        let addressString = ''
        if (this.state.address !== null)
            addressString = addressString + this.state.address + ' '
        if (this.state.zip != null)
            addressString = addressString + this.state.zip + ' '
        if (this.state.street != null)
            addressString = addressString + this.state.street + ' '
        if (this.state.city != null)
            addressString = addressString + this.state.city + ' '
        if (this.state.state != null)
            addressString = addressString + this.state + ' '
        if (this.state.country != null)
            addressString = addressString + this.state.country + ' '
        return addressString

    }


    render() {
        return (
            <div className="maininfo-card-info">
                <label className="childcontainer-label">Username</label>
                <input className="childcontainer-input" type="text" name="username" value={this.state.username ? this.state.username : ''} readOnly />
                <label className="childcontainer-label">Name</label>
                <input className="childcontainer-input" type="text" name="name" value={this.state.name ? this.state.name : ''} readOnly />
                <div className="maininfo-block">
                    <div className="maininfo-col2">
                        <label className="childcontainer-label">Phone</label>
                        <input className="childcontainer-input" type="text" name="phone" value={this.state.phone ? this.state.phone : ''} readOnly />
                    </div>
                    <div className="maininfo-col2">
                        <label className="childcontainer-label">{this.state.selected_phone_option}</label>
                        <input className="childcontainer-input" name={this.state.selected_phone_option === 'Extension' ? ('Extension') : ('option')} value={this.state.selected_phone_option === 'Extension' ? (this.state.extension) : (this.state.option)} id="account-extension-input" readOnly />
                    </div>
                </div>
                <label className="childcontainer-label">Email</label>
                <input className="childcontainer-input" type="text" name="email" value={this.state.email ? this.state.email : ''} readOnly />
                <label className="childcontainer-label">Company Name</label>
                <input className="childcontainer-input" type="text" name="company_name" value={this.state.company_name ? this.state.company_name : ''} readOnly />
                <label className="childcontainer-label">Website</label>
                <input className="childcontainer-input" type="text" name="website" value={this.state.website ? this.state.website : ''} readOnly />
                <label className="childcontainer-label">Address</label>
                <input className="childcontainer-input" type="text" name="address" value={this.addressString()} readOnly />
            </div>

        )
    }
}
const mapStateToProps = (state) => {
    return {
        systemtype: state.home.systemtype,

    }
}

export default connect(mapStateToProps, null)(AccountInfo);
