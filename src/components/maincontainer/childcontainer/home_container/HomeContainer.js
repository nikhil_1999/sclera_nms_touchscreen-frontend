import React, { Component } from 'react'
import MainList from './mainlist/MainList'
import MainInfo from './maininfo/MainInfo'
import WeatherWidget from './weatherwidget/WeatherWidget';
import { connect } from 'react-redux';
import './homecontainer.css';

class HomeContainer extends Component {

    renderInfo = () => {
        if (this.props.systemtype !== null || this.props.building !== null || this.props.floor !== null || this.props.location !== null) {
            return (<MainInfo />)
        } else {
            return (<WeatherWidget />)
        }
    }

    render() {
        return (
            <div className="homecontainer childcontainer-inner">
                <MainList />
                {this.renderInfo()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        systemtype: state.home.systemtype,
        building: state.home.building,
        floor: state.home.floor,
        location: state.home.location
    }
}

export default connect(mapStateToProps, null)(HomeContainer);