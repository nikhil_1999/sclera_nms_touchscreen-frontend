import React, { Component } from 'react'
import Navbar from './navbar/Navbar';
import ChildContainer from './childcontainer/ChildContainer';
import './maincontainer.css';
import Notification from './childcontainer/notification_container/Notification';
import { connect } from 'react-redux';
import Modal from '../modal/Modal';

class MainContainer extends Component {
    render() {
        return (
            <div className="maincontainer">
                <Navbar />
                <ChildContainer />
                {this.props.show_notification ? <Notification /> : null}
                <p className="maincontainer-footer">Sclera VDMS</p>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        selected_choice: state.navbar.selected_choice,
        show_notification: state.navbar.show_notification,

    }
}
export default connect(mapStateToProps, null)(MainContainer);