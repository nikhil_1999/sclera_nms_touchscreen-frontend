import axios from 'axios';

export const link = 'http://localhost:2000/api';
export const mainserver = 'http://dbapi.sclera.com:7777';

export const dashboard_state_enum = {
    1: 'snooze',
    2: 'reported to vendor',
    3: 'fixed by vendor'
}

export const routerapilinks = "http://25.37.246.76:5000";
export const scleradmslink = 'http://10.2.10.136:5001';
export const axiosRequest = (url, data, method, headers) => {
    if (url.length === 0) url = link;
    if (method.length === 0) method = 'GET';
    return axios({
        method: method,
        url: url,
        headers: headers,
        data: data
    })
}

export const AlarmTypes = {
    "1": "IP Change",
    "2": "New Device",
    "3": "IP conflict & IP change",
    "4": "IP conflict and New device",
    "5": "IP change and mac change",
    "6": "New device and Mac change",
    "7": "Went Offline and Came Online",
    "8": "Came Online",
    "9": "Went offline"
}