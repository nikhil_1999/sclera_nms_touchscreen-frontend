import * as dashboardActionTypes from '../actions/DashboardActions';

const initState = {
    dashboard_list: null,
    selected_dashboard: null
}

const dashboardReducer = (state = initState, action) => {
    switch (action.type) {
        case dashboardActionTypes.LIST_DASHBOARD:
            return {
                ...state,
                dashboard_list: action.payload
            }
        case dashboardActionTypes.SELECTED_DASHBOARD:
            return {
                ...state,
                selected_dashboard: action.payload
            }
        case dashboardActionTypes.RESET_SELECTED_DASHBOARD:
            return {
                ...state,
                selected_dashboard: action.payload
            }


        default: return state;
    }
}

export default dashboardReducer;