import * as notificationActionTypes from '../actions/NotificationActions';

const initState = {
    notification_list: null
}

const notificationReducer = (state = initState, action) => {
    switch (action.type) {
        case notificationActionTypes.LIST_NOTIFICATIONS:
            return {
                ...state,
                notification_list: action.payload
            }
        default: return state;
    }
}

export default notificationReducer;