import * as homeActionTypes from '../actions/HomeActions';
import { useCallback } from 'react';

const initState = {
    systemtype: null,
    building: null,
    floor: null,
    location: null,
    selected_list_tab: 'systemtype',
    selected_info_tab: 'account-info',
    selected_device: null,
    building_action: null,
    floor_action: null,
    location_action: null

}

const homeReducer = (state = initState, action) => {
    switch (action.type) {
        case homeActionTypes.SELECT_SYSTEMTYPE:
            return {
                ...state,
                systemtype: action.payload
            }

        case homeActionTypes.SELECT_BUILDING:
            return {
                ...state,
                building: action.payload
            }

        case homeActionTypes.SELECT_FLOOR:
            return {
                ...state,
                floor: action.payload
            }

        case homeActionTypes.SELECT_LOCATION:
            return {
                ...state,
                location: action.payload
            }

        case homeActionTypes.SELECTED_LIST_TAB:
            return {
                ...state,
                selected_list_tab: action.payload
            }

        case homeActionTypes.SELECTED_INFO_TAB:
            return {
                ...state,
                selected_info_tab: action.payload
            }

        case homeActionTypes.SELECTED_DEVICE:
            return {
                ...state,
                selected_device: action.payload
            }

        case homeActionTypes.SELECTED_SENSOR:
            return {
                ...state,
                selected_sensor: action.payload
            }
        case homeActionTypes.RESET_SYSTEM_TYPE:
            return {
                ...state,
                systemtype: null
            }
        case homeActionTypes.RESET_BUILDING:
            return {
                ...state,
                building: null
            }
        case homeActionTypes.RESET_FLOOR:
            return {
                ...state,
                floor: null
            }
        case homeActionTypes.RESET_LOCATION:
            return {
                ...state,
                location: null
            }
        case homeActionTypes.RESET_SELECTED_DEVICE:
            return {
                ...state,
                selected_device: null
            }
        case homeActionTypes.BUILDING_ACTION:
            return {
                ...state,
                building_action: action.payload
            }
        case homeActionTypes.FLOOR_ACTION:
            return {
                ...state,
                floor_action: action.payload
            }
        case homeActionTypes.LOCATION_ACTION:
            return {
                ...state,
                location_action: action.payload
            }
        case homeActionTypes.RESET_BUILDING_ACTION:
            return {
                ...state,
                building_action: null
            }
        case homeActionTypes.RESET_FLOOR_ACTION:
            return {
                ...state,
                floor_action: null
            }
        case homeActionTypes.RESET_LOCATION_ACTION:
            return {
                ...state,
                location_action: null
            }
        default: return state;
    }
}

export default homeReducer;