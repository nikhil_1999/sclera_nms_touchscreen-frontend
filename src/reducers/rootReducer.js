import navbarReducer from './navbarReducer';
import homeReducer from './homeReducer';
import contactReducer from './contactReducer';
import dashboardReducer from './dashboardReducer';
import notificationReducer from './notificationReducer';


import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    navbar: navbarReducer,
    home: homeReducer,
    contact: contactReducer,
    dashboard: dashboardReducer,
    notification: notificationReducer
});

export default rootReducer;