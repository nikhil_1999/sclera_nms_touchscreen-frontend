import * as NavbarActionTypes from '../actions/NavbarActions';

const initState = {
    selected_choice: 'dashboard',
    show_notification: false
}

const navbarReducer = (state = initState, action) => {
    switch (action.type) {
        case NavbarActionTypes.SELECTED_CHOICE:
            return {
                ...state,
                selected_choice: action.payload
            };

        case NavbarActionTypes.NOT_SELECTED:
            return state;
        case NavbarActionTypes.SHOW_NOTIFICATION:
            return {
                ...state,
                show_notification: action.payload
            };

        default:
            return state;
    }
};

export default navbarReducer;