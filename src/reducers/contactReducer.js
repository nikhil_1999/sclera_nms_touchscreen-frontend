import * as contactActionTypes from '../actions/ContactActions';

const initState = {
    selected_contact:null
}

const contactReducer = (state=initState,action) => {
    switch(action.type){
        case contactActionTypes.SELECTED_CONTACT:
            return {
                ...state,
                selected_contact:action.payload
            }
        
        case contactActionTypes.RESET_SELECTED_CONTACT:
            return {
                ...state,
                selected_contact:null
            }
        
        default:return state;
    }
}

export default contactReducer;